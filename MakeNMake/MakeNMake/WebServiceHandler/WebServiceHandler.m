//
//  WebServiceHandler.m
//  MakeNMake
//
//  Created by Archit Saxena on 26/03/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "WebServiceHandler.h"

@implementation WebServiceHandler

+ (WebServiceHandler *)sharedDocument {
    
    static WebServiceHandler *sharedDocument = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedDocument = [[WebServiceHandler alloc] init];
    });
    return sharedDocument;
}

@end
