//
//  Utility.h
//  
//
//  Created by Nriendra on 

//

#import <Foundation/Foundation.h>

@interface Utility : NSObject
+(Utility*)sharedInstance;

+ (BOOL)isValidUrl: (NSString *)url;
+ (BOOL)isValidString:(NSString *)string;
-(NSString*)convertDate:(NSDate*)date toDateFormat:(NSString*)toFormat;
-(NSDateFormatter*)getDateFormatterWithDateFormat:(NSString*)dateFormat;
+ (BOOL) NSStringIsValidEmail:(NSString *)checkString;
+ (BOOL) NSStringIsValidName:(NSString *)checkString;
+ (NSString*)getValidString:(NSString *)string;
+ (NSString *)getUrlStringWithHttpVerb:(NSString *)url;
+ (void)showAlertMessage:(NSString *)message withTitle:(NSString *)title;
+ (void)showNetWorkAlert;
+(NSString*)getCurrentDateString;
+ (NSDate *)getUTCFormateDate:(double)timeInSeconds;
extern id ObjectOrBlank(id object);

+ (NSString *)appVersion;
+ (NSString *)build;

+ (NSString *)extractComponentsInString:(NSDate *)date;

+ (NSString *)stringWithoutNilOrNull :(NSString *)checkString;

+ (NSString*)base64forData:(NSData*) theData;

+(NSString *)imageToNSString:(UIImage *)image;

+(void)setRoundedImage:(id)roundedView toDiameter:(float)newSize;

//Resize image from a given image
+ (UIImage*)imageWithImage:(UIImage*)image
              scaledToSize:(CGSize)newSize;

+ (UIImage *)resizeImage:(UIImage*)image newSize:(CGSize)newSize;

+(void) startActivityIndicatorInView:(UIView*)aView withMessage:(NSString*)aMessage;
+(void) stopActivityIndicatorInView:(UIView*)aView;
-(void)archiveFileWithFileName:(NSString*)fileName andData:(NSObject*)data;
-(BOOL)unArchiveFileWithFileName:(NSString*)fileName;
-(void)logoutUserWithFileName:(NSString *)fileName;
-(NSMutableArray *)getFaqQuestionForService:(NSString *)serviceName;
-(NSMutableArray *)getFaqAnswerForService:(NSString *)serviceName;
+ (UIWebView *)youtubePlayerViewWithFrame:(CGRect)frame andURLString:(NSString *)URLString;

@end
