//
//  Utility.m
//  MyFlingFinder
//
//  Created by Dipak on 12/25/14.
//  Copyright (c) 2014 Neuron. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "Utility.h"
#import "Constant.h"

//#import "MBProgressHUD.h"

@implementation Utility

#pragma mark - Validation
static Utility* oaUtility = nil;
+(Utility*)sharedInstance
{
    if (oaUtility == nil) {
        oaUtility = [[Utility alloc]init];
        
    }
    return oaUtility;
}
+ (BOOL)isValidUrl:(NSString *)url {
    
    /*NSString *urlRegEx =
     
     @"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
     
     NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
     
     return [urlTest evaluateWithObject:url];*/
    
    NSURL *candidateURL = [NSURL URLWithString:url];
    
    // WARNING > "test" is an URL according to RFCs, being just a path
    
    // so you still should check scheme and all other NSURL attributes you need
    if (candidateURL && candidateURL.scheme && candidateURL.host) {
        
        // candidate is a well-formed url with:
        
        //  - a scheme (like http://)
        
        //  - a host (like stackoverflow.com)
        
        return YES;
    }
    return NO;
}

//+(UIViewController *)getCurrentRootViewController
//{
//    UIViewController *result = nil;
//    
//    // Find the top window (that is not an alert view or other window)
//    UIWindow *topWindow = [SHARED_APPLICATION keyWindow];
//    if (topWindow.windowLevel != UIWindowLevelNormal)
//    {
//        NSArray *windows = [SHARED_APPLICATION windows];
//        for(topWindow in windows)
//        {
//            if (topWindow.windowLevel == UIWindowLevelNormal)
//                break;
//        }
//    }
//    
//    if ([topWindow subviews] && [topWindow subviews].count)
//    {
//        UIView *rootView = [[topWindow subviews] objectAtIndex:0];
//        id nextResponder = [rootView nextResponder];
//        
//        if ([nextResponder isKindOfClass:[UIViewController class]])
//        {
//            result = nextResponder;
//        }
//        else if ([topWindow respondsToSelector:@selector(rootViewController)] && topWindow.rootViewController != nil)
//        {
//            result = topWindow.rootViewController;
//        }
//        else
//        {
//            NSAssert(NO, @"Error");
//        }
//    }
//    
//    return result;
//}
//
//
//#pragma mark - Activity Indicator
//+(void) startActivityIndicatorInView:(UIView*)aView withMessage:(NSString*)aMessage
//{
//    MBProgressHUD *_hud = [MBProgressHUD showHUDAddedTo:aView animated:YES];
//    _hud.dimBackground  = YES;
//    _hud.labelText      = aMessage;
//    _hud.userInteractionEnabled = NO;
//    
//}
//
//+(void) stopActivityIndicatorInView:(UIView*)aView
//{
//    [MBProgressHUD hideHUDForView:aView animated:YES];
//}

+ (NSString *)getUrlStringWithHttpVerb:(NSString *)url {
    
    [self getValidString:url];
    if (!([url hasPrefix:@"http://"] || [url hasPrefix:@"https://"])) {
        
        url = [NSString stringWithFormat:@"http://%@",url];
    }
    return url;
}

+ (BOOL)isValidString:(NSString *)string

{
    if ([string isKindOfClass:[NSNull class]] || string.length == 0 || [string isEqualToString:@"null"] || [string isEqualToString:@"<null>"] || [string isEqualToString:@"(null)"])
        return NO;
    return YES;
}


+ (BOOL)NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}


+ (BOOL)NSStringIsValidName:(NSString *)checkString
{
    NSString *nameRegex = @"[A-Za-z]+";
    NSPredicate *nameTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", nameRegex];
    return [nameTest evaluateWithObject:checkString];
}

+ (NSString*)getValidString:(NSString *)string
{
    if ([Utility isValidString:string])
    {
        string = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    } else
    {
        string = @"";
    }
    return string;
}

#pragma mark - Show Alert

// Show an alert message
+ (void)showAlertMessage:(NSString *)message withTitle:(NSString *)title
{
    [[[UIAlertView alloc] initWithTitle:title
                                message:message
                               delegate:nil
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil] show];
}

// Show an Network alert message

+ (void)showNetWorkAlert {
    
    UIAlertView *alert =[[UIAlertView alloc] initWithTitle:@"No Network Connection" message:@"Please check your connection and try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    
    [alert show];
    
}

+ (NSDate *)getUTCFormateDate:(double)timeInSeconds
{
    NSTimeInterval mytime = timeInSeconds;
    //////NSLog(@"mytime:%f",mytime);
    NSDate *localDate = [NSDate dateWithTimeIntervalSince1970:mytime];
    //////NSLog(@"localDate:%@",localDate);
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:localDate];
    //return dateString;
    return [dateFormatter dateFromString:dateString];
}

//Method for passing null values when intented values are not available.
extern id ObjectOrBlank(id object)
{
    //return object ?: [NSNull null];
    return object ?: @"";

}

//Convert NSDate into NSString (With different components)
+ (NSString *)extractComponentsInString:(NSDate *)date
{
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    // Extract date components into components
    NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:date];
    NSInteger year = [components year];
    NSInteger month = [components month];
    NSInteger day = [components day];
    
    NSString *dateInString = [NSString stringWithFormat:@"%ld-%ld-%ld",(long)year,(long)month,(long)day];
    return dateInString;
}

//Convert NSDate into NSString (With different components)
+ (NSString *)extractTimeComponentsInString:(NSDate *)date
{
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    // Extract date components into components
    NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:date];
    NSInteger year = [components year];
    NSInteger month = [components month];
    NSInteger day = [components day];
    
    NSString *dateInString = [NSString stringWithFormat:@"%ld-%ld-%ld",(long)year,(long)month,(long)day];
    return dateInString;
}



+ (NSString *) appVersion
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
}

+ (NSString *) build
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
}

//Test methods
+ (NSString *)stringWithoutNilOrNull :(NSString *)checkString
{
       if (![checkString isKindOfClass:[NSNull class]])
        {
            return checkString;
        }
    else
    {
      return checkString = @"";
    }
}

#pragma mark - Base64 conversion for image
+ (NSString*)base64forData:(NSData*) theData {
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}


#pragma mark - Convert UIImage to Base64
+ (NSString *)imageToNSString:(UIImage *)image
{
    NSData *data = UIImagePNGRepresentation(image);
    return [data base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
}

#pragma mark - Uiimageview with Rounded corner
+ (void)setRoundedImage:(id)roundedView toDiameter:(float)newSize
{
    if ([roundedView isKindOfClass:[UIImageView class]])
    {
        UIImageView *imgview = (UIImageView *)roundedView;
        
       // CGPoint saveCenter = imgview.center;
        CGRect newFrame = CGRectMake(imgview.frame.origin.x, imgview.frame.origin.y, newSize, newSize);
        imgview.frame = newFrame;
        imgview.layer.cornerRadius = newSize / 2.0;
        imgview.layer.borderWidth = 0.1;
        imgview.layer.borderColor = [[UIColor grayColor] CGColor];
        //imgview.center = saveCenter;
    }
    else if ([roundedView isKindOfClass:[UIView class]])
    {
        UIView *view = (UIView *)roundedView;
        
        CGPoint saveCenter = view.center;
        CGRect newFrame = CGRectMake(view.frame.origin.x, view.frame.origin.y, newSize, newSize);
        view.frame = newFrame;
        view.layer.cornerRadius = newSize / 2.0;
        view.layer.borderWidth = 2.0;
        view.layer.borderColor = [[UIColor redColor] CGColor];
        view.center = saveCenter;
    }
    else if ([roundedView isKindOfClass:[UIButton class]])
    {
        UIButton *btnView = (UIButton *)roundedView;
        
        CGPoint saveCenter = btnView.center;
        CGRect newFrame = CGRectMake(btnView.frame.origin.x, btnView.frame.origin.y, newSize, newSize);
        btnView.frame = newFrame;
        btnView.layer.cornerRadius = newSize / 2.0;
        btnView.layer.borderWidth = 2.0;
        //btnView.layer.borderColor = [App_Theme_Color CGColor];
        btnView.center = saveCenter;
    }
}

+ (UIImage*)imageWithImage:(UIImage*)image
              scaledToSize:(CGSize)newSize;
{
    NSData *imageData = UIImagePNGRepresentation(image);
    //////NSLog(@"Image size in MB:%lu",imageData.length/1024);
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    imageData = UIImagePNGRepresentation(newImage);
    //////NSLog(@"After Compression: Image size in MB:%lu",imageData.length/1024);
    return newImage;
}
-(NSString*)convertDate:(NSDate*)date toDateFormat:(NSString*)toFormat{
    if (date) {
        if ([[self getDateFormatterWithDateFormat:toFormat] stringFromDate:date]) {
            return [[self getDateFormatterWithDateFormat:toFormat] stringFromDate:date];
        }
        else{
            NSLog(@"ERROR : CANNOT CONVERT TO REQUIRED FORMAT");
            return nil;
        }
    }
    else
    {
        NSLog(@"ERROR: NIL date");
        return nil;
    }
}

-(NSDateFormatter*)getDateFormatterWithDateFormat:(NSString*)dateFormat{
    NSDateFormatter* dateFormatter =[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:dateFormat];
    return dateFormatter;
}

+ (UIImage *)resizeImage:(UIImage*)image newSize:(CGSize)newSize
{
    CGRect newRect = CGRectIntegral(CGRectMake(0, 0, newSize.width, newSize.height));
    CGImageRef imageRef = image.CGImage;
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Set the quality level to use when rescaling
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    CGAffineTransform flipVertical = CGAffineTransformMake(1, 0, 0, -1, 0, newSize.height);
    
    CGContextConcatCTM(context, flipVertical);
    // Draw into the context; this scales the image
    CGContextDrawImage(context, newRect, imageRef);
    
    // Get the resized image from the context and a UIImage
    CGImageRef newImageRef = CGBitmapContextCreateImage(context);
    UIImage *newImage = [UIImage imageWithCGImage:newImageRef];
    
    CGImageRelease(newImageRef);
    
    UIGraphicsEndImageContext();
    
    return newImage;
}
-(void)archiveFileWithFileName:(NSString*)fileName andData:(NSObject*)data{
    NSString* documentDirectoryPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString* filePath = [NSString stringWithFormat:@"%@/%@",documentDirectoryPath,fileName];
    NSLog(@"archived file path : %@",filePath);
    @try {
        BOOL isSucceed = [NSKeyedArchiver archiveRootObject:data toFile:filePath];
        NSLog(@"%@ file is %@ archived.",fileName,isSucceed?@"successfully":@"not");
    }
    @catch (NSException *exception) {
        NSLog(@"Exception occurred while archiving : %@",exception.reason);
    }
    @finally {
        NSLog(@"Archive process was performed");
    }
    
}
-(BOOL)unArchiveFileWithFileName:(NSString*)fileName{
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    
    NSString* documentDirectoryPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString* filePath = [NSString stringWithFormat:@"%@/%@",documentDirectoryPath,fileName];
    NSLog(@"archived file path : %@",filePath);
    
    BOOL returnObject = false;
    if( [fileMgr fileExistsAtPath:filePath] )
    {
        @try {
            returnObject = true;
        }
        @catch (NSException *exception) {
            NSLog(@"Exception Occurred : %@",exception.reason);
            returnObject = false;
        }
        @finally {
            NSLog(@"UnArchive process is done");
        }
    }
    else{
        NSLog(@"Unarchive file is not found");
    }
    return returnObject;
    
}
-(void)logoutUserWithFileName:(NSString *)fileName{
    NSFileManager *fileMgr = [NSFileManager defaultManager];

    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"UserId"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userName"];

    NSString* documentDirectoryPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString* filePath = [NSString stringWithFormat:@"%@/%@",documentDirectoryPath,fileName];
    NSLog(@"archived file path : %@",filePath);
    
    if( [fileMgr fileExistsAtPath:filePath] )
    {
        [fileMgr removeItemAtPath:filePath error:nil];
    }else{
        //User Already Logout
    }
}
-(NSMutableArray *)getFaqQuestionForService:(NSString *)serviceName{
    
    NSMutableArray *questionArray = [[NSMutableArray alloc]init];

    
    if ([serviceName caseInsensitiveCompare:SERVICE_AC] == NSOrderedSame) {
        [questionArray addObject:ACQuest1];
        [questionArray addObject:ACQuest2];
        
    }else if ([serviceName caseInsensitiveCompare:SERVICE_Electrical] == NSOrderedSame) {
        [questionArray addObject:ElectricalServiceQuest1];
        [questionArray addObject:ElectricalServiceQuest2];

    }else if ([serviceName caseInsensitiveCompare:SERVICE_HomeCleaning] == NSOrderedSame) {
        [questionArray addObject:HomeCleaningQuest1];
        [questionArray addObject:HomeCleaningQuest2];
        [questionArray addObject:HomeCleaningQuest3];
        [questionArray addObject:HomeCleaningQuest4];
        [questionArray addObject:HomeCleaningQuest5];

    }else if ([serviceName caseInsensitiveCompare:SERVICE_Upholstery] == NSOrderedSame) {
        [questionArray addObject:UpholsteryCleaningQuest1];
        [questionArray addObject:UpholsteryCleaningQuest2];
        [questionArray addObject:UpholsteryCleaningQuest3];
        [questionArray addObject:UpholsteryCleaningQuest4];
        [questionArray addObject:UpholsteryCleaningQuest5];

    }else if ([serviceName caseInsensitiveCompare:SERVICE_WaterTank] == NSOrderedSame) {
        [questionArray addObject:WaterTankQuest1];
        [questionArray addObject:WaterTankQuest2];
        [questionArray addObject:WaterTankQuest3];

    }else if ([serviceName caseInsensitiveCompare:SERVICE_Appliance] == NSOrderedSame) {
        [questionArray addObject:ApplianceQuest1];
        [questionArray addObject:ApplianceQuest2];
        [questionArray addObject:ApplianceQuest3];

    }else if ([serviceName caseInsensitiveCompare:SERVICE_RO] == NSOrderedSame) {
        [questionArray addObject:ROQuest1];
        [questionArray addObject:ROQuest2];
        [questionArray addObject:ROQuest3];

    }else if ([serviceName caseInsensitiveCompare:SERVICE_CCTV] == NSOrderedSame) {
        [questionArray addObject:CCTVQuest1];
        [questionArray addObject:CCTVQuest2];

    }else if ([serviceName caseInsensitiveCompare:SERVICE_Paint] == NSOrderedSame) {
        [questionArray addObject:PaintQuest1];
        [questionArray addObject:PaintQuest2];
        [questionArray addObject:PaintQuest3];
        [questionArray addObject:PaintQuest4];
        [questionArray addObject:PaintQuest5];

    }else if ([serviceName caseInsensitiveCompare:SERVICE_CarSpa] == NSOrderedSame) {
        [questionArray addObject:CarSpaQuest1];
        [questionArray addObject:CarSpaQuest2];
        [questionArray addObject:CarSpaQuest3];
        [questionArray addObject:CarSpaQuest4];
        [questionArray addObject:CarSpaQuest5];
        [questionArray addObject:CarSpaQuest6];

    }else if ([serviceName caseInsensitiveCompare:SERVICE_Plumbing] == NSOrderedSame) {
        [questionArray addObject:PlumbingQuest1];

    }else if ([serviceName caseInsensitiveCompare:SERVICE_Woodwork] == NSOrderedSame) {
        [questionArray addObject:WoodWorkQuest1];
        [questionArray addObject:WoodWorkQuest2];
        [questionArray addObject:WoodWorkQuest3];
        [questionArray addObject:WoodWorkQuest4];
        [questionArray addObject:WoodWorkQuest5];

    }else if ([serviceName caseInsensitiveCompare:SERVICE_Carpentry] == NSOrderedSame) {
        [questionArray addObject:CarpentryQuest1];
        [questionArray addObject:CarpentryQuest2];
        
    }else if ([serviceName caseInsensitiveCompare:SERVICE_Gardening] == NSOrderedSame) {
        [questionArray addObject:GardeningQuest1];
        [questionArray addObject:GardeningQuest2];
        [questionArray addObject:GardeningQuest3];
        
    }else if ([serviceName caseInsensitiveCompare:SERVICE_PestControl] == NSOrderedSame) {
        [questionArray addObject:PestControlQuest1];
        [questionArray addObject:PestControlQuest2];
        [questionArray addObject:PestControlQuest3];
        [questionArray addObject:PestControlQuest4];


    }else if ([serviceName caseInsensitiveCompare:SERVICE_Fumigation] == NSOrderedSame) {
        [questionArray addObject:FumigationQuest1];

    }
    else if ([serviceName caseInsensitiveCompare:@"UnlimitedPlanVC"] == NSOrderedSame) {
        [questionArray addObject:UnlimitedPlanQuest1];
        [questionArray addObject:UnlimitedPlanQuest2];
        [questionArray addObject:UnlimitedPlanQuest3];
        [questionArray addObject:UnlimitedPlanQuest4];
        [questionArray addObject:UnlimitedPlanQuest5];
        [questionArray addObject:UnlimitedPlanQuest6];
        [questionArray addObject:UnlimitedPlanQuest7];
        [questionArray addObject:UnlimitedPlanQuest8];
        [questionArray addObject:UnlimitedPlanQuest9];
        [questionArray addObject:UnlimitedPlanQuest10];
        [questionArray addObject:UnlimitedPlanQuest11];
        [questionArray addObject:UnlimitedPlanQuest12];

    }
    else if ([serviceName caseInsensitiveCompare:@"FixedPlanVC"] == NSOrderedSame) {
        [questionArray addObject:FixedPlanQuest1];
        [questionArray addObject:FixedPlanQuest2];
        [questionArray addObject:FixedPlanQuest3];
        [questionArray addObject:FixedPlanQuest4];
        [questionArray addObject:FixedPlanQuest5];
        [questionArray addObject:FixedPlanQuest6];
        [questionArray addObject:FixedPlanQuest7];

    }
    else if ([serviceName caseInsensitiveCompare:@"AddOnPlanVC"] == NSOrderedSame) {
        [questionArray addObject:AddOnQuest1];
        [questionArray addObject:AddOnQuest2];

    }
    else{
        //sendEmpty Array
    }
    
    return questionArray;
}
-(NSMutableArray *)getFaqAnswerForService:(NSString *)serviceName{
    
    NSMutableArray *answerArray = [[NSMutableArray alloc]init];
    
    if ([serviceName caseInsensitiveCompare:SERVICE_AC]== NSOrderedSame) {
        [answerArray addObject:ACAnswer1];
        [answerArray addObject:ACAnswer2];
        
    }else if ([serviceName caseInsensitiveCompare:SERVICE_Electrical]== NSOrderedSame) {
        [answerArray addObject:HomeCleaningAnswer1];
        [answerArray addObject:HomeCleaningAnswer2];
        
    }else if ([serviceName caseInsensitiveCompare:SERVICE_HomeCleaning]== NSOrderedSame) {
        [answerArray addObject:HomeCleaningAnswer1];
        [answerArray addObject:HomeCleaningAnswer2];
        [answerArray addObject:HomeCleaningAnswer3];
        [answerArray addObject:HomeCleaningAnswer4];
        [answerArray addObject:HomeCleaningAnswer5];
        
    }else if ([serviceName caseInsensitiveCompare:SERVICE_Upholstery]== NSOrderedSame) {
        [answerArray addObject:UpholsteryCleaningAnswer1];
        [answerArray addObject:UpholsteryCleaningAnswer2];
        [answerArray addObject:UpholsteryCleaningAnswer3];
        [answerArray addObject:UpholsteryCleaningAnswer4];
        [answerArray addObject:UpholsteryCleaningAnswer5];
        
    }else if ([serviceName caseInsensitiveCompare:SERVICE_WaterTank]== NSOrderedSame) {
        [answerArray addObject:WaterTankAns1];
        [answerArray addObject:WaterTankAns2];
        [answerArray addObject:WaterTankAns3];
        
    }else if ([serviceName caseInsensitiveCompare:SERVICE_Appliance]== NSOrderedSame) {
        [answerArray addObject:ApplianceAns1];
        [answerArray addObject:ApplianceAns2];
        [answerArray addObject:ApplianceAns3];
        
    }else if ([serviceName caseInsensitiveCompare:SERVICE_RO]== NSOrderedSame) {
        [answerArray addObject:ROAns1];
        [answerArray addObject:ROAns2];
        [answerArray addObject:ROAns3];
        
    }else if ([serviceName caseInsensitiveCompare:SERVICE_CCTV]== NSOrderedSame) {
        [answerArray addObject:CCTVAns1];
        [answerArray addObject:CCTVAns2];
        
    }else if ([serviceName caseInsensitiveCompare:SERVICE_Paint]== NSOrderedSame) {
        [answerArray addObject:PaintAns1];
        [answerArray addObject:PaintAns2];
        [answerArray addObject:PaintAns3];
        [answerArray addObject:PaintAns4];
        [answerArray addObject:PaintAns5];
        
    }else if ([serviceName caseInsensitiveCompare:SERVICE_CarSpa]== NSOrderedSame) {
        [answerArray addObject:CarSpaAnswer1];
        [answerArray addObject:CarSpaAnswer2];
        [answerArray addObject:CarSpaAnswer3];
        [answerArray addObject:CarSpaAnswer4];
        [answerArray addObject:CarSpaAnswer5];
        [answerArray addObject:CarSpaAnswer6];
        
    }else if ([serviceName caseInsensitiveCompare:SERVICE_Plumbing]== NSOrderedSame) {
        [answerArray addObject:PlumbingAnswer1];
        
    }else if ([serviceName caseInsensitiveCompare:SERVICE_Woodwork]== NSOrderedSame) {
        [answerArray addObject:WoodWorkAnswer1];
        [answerArray addObject:WoodWorkAnswer2];
        [answerArray addObject:WoodWorkAns3];
        [answerArray addObject:WoodWorkAns4];
        [answerArray addObject:WoodWorkAns5];
        
    }else if ([serviceName caseInsensitiveCompare:SERVICE_Carpentry]== NSOrderedSame) {
        [answerArray addObject:CarpentryAns1];
        [answerArray addObject:CarpentryAns2];
        
    }else if ([serviceName caseInsensitiveCompare:SERVICE_Gardening]== NSOrderedSame) {
        [answerArray addObject:GardeningAns1];
        [answerArray addObject:GardeningAns2];
        [answerArray addObject:GardeningAns3];
        
    }else if ([serviceName caseInsensitiveCompare:SERVICE_PestControl] == NSOrderedSame) {
        [answerArray addObject:PestControlAns1];
        [answerArray addObject:PestControlAns2];
        [answerArray addObject:PestControlAns3];
        [answerArray addObject:PestControlAns4];
    
        
    }else if ([serviceName caseInsensitiveCompare:SERVICE_Fumigation]== NSOrderedSame) {
        [answerArray addObject:FumigationAns1];
        
    }else if ([serviceName caseInsensitiveCompare:@"UnlimitedPlanVC"]== NSOrderedSame) {
        [answerArray addObject:UnlimitedPlanAns1];
        [answerArray addObject:UnlimitedPlanAns2];
        [answerArray addObject:UnlimitedPlanAns3];
        [answerArray addObject:UnlimitedPlanAns4];
        [answerArray addObject:UnlimitedPlanAns5];
        [answerArray addObject:UnlimitedPlanAns6];
        [answerArray addObject:UnlimitedPlanAns7];
        [answerArray addObject:UnlimitedPlanAns8];
        [answerArray addObject:UnlimitedPlanAns9];
        [answerArray addObject:UnlimitedPlanAns10];
        [answerArray addObject:UnlimitedPlanAns11];
        [answerArray addObject:UnlimitedPlanAns12];

        
    }else if ([serviceName caseInsensitiveCompare:@"FixedPlanVC"]==NSOrderedSame) {
        
        [answerArray addObject:FixedPlanAns1];
        [answerArray addObject:FixedPlanAns2];
        [answerArray addObject:FixedPlanAns3];
        [answerArray addObject:FixedPlanAns4];
        [answerArray addObject:FixedPlanAns5];
        [answerArray addObject:FixedPlanAns6];
        [answerArray addObject:FixedPlanAns7];
        
    }
    /*else if ([serviceName isEqualToString:SERVICE_InteriorDesign]) {
        [answerArray addObject:InteriorDesignAns1];
    }*/
    else{
        //sendEmpty Array
    }

    
    return answerArray;
}
+ (UIWebView *)youtubePlayerViewWithFrame:(CGRect)frame andURLString:(NSString *)URLString {
    
    NSString *urlId = [URLString stringByReplacingOccurrencesOfString:@"https://youtu.be/" withString:@""];
    
    URLString = [NSString stringWithFormat:@"http://www.youtube.com/embed/%@",urlId];
    
    
    UIWebView *videoView = [[UIWebView alloc] initWithFrame:frame];
    videoView.backgroundColor = [UIColor clearColor];
    videoView.opaque = NO;
    
    NSString *videoHTML = [NSString stringWithFormat:@"\
                           <html>\
                           <head>\
                           <style type=\"text/css\">\
                           iframe {position:absolute; top:0; margin-top:0px;}\
                           body {background-color:#000; margin:0;}\
                           </style>\
                           </head>\
                           <body>\
                           <iframe width=\"%0.0f\" height=\"%0.0f\" src=\"%@\" frameborder=\"0\" allowfullscreen></iframe>\
                           </body>\
                           </html>", frame.size.width, frame.size.height, [self youtubeEmbededURLStringFromURLString:URLString]];
    
    [videoView loadHTMLString:videoHTML baseURL:nil];
    
    return videoView;
}

+ (NSString *)youtubeEmbededURLStringFromURLString:(NSString *)URLString {
    NSMutableString *mutableURLString = [URLString mutableCopy];
    [mutableURLString replaceOccurrencesOfString:@"watch?v=" withString:@"embed/" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [mutableURLString length])];
    NSString *youtubeEmbededURLString = mutableURLString;
    return youtubeEmbededURLString;

}
@end
