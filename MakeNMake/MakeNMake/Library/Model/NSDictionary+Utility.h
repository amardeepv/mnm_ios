//
//  NSDictionary+Utility.h
//  MyFlingFinder
//
//  Created by Dipak on 23/12/14.
//  Copyright (c) 2014 Neurons Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Utility)

- (id)objectForKeyNotNull:(id)key;
- (BOOL)hasValueForKey:(NSString *)key;
@end
