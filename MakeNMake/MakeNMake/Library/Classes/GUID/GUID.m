//
//  GUID.m
//  ProductSurvey
//
//  Created by Biggs|Gilmore on 3/12/13.
//
//

#import "GUID.h"


@implementation GUID

+ (NSString*) generateNew{
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    return  [(NSString *)CFBridgingRelease(string) lowercaseString];
}

@end
