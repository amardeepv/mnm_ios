//
//  GUID.h
//  ProductSurvey
//
//  Created by Biggs|Gilmore on 3/12/13.
//
//

#import <Foundation/Foundation.h>

@interface GUID : NSObject

+ (NSString*) generateNew;

@end
