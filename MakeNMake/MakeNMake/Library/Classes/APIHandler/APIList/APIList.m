//
//  APIList.m
//

#import "APIList.h"
#import "Constant.h"

@implementation APIList

static APIList *apiListObj = nil;

// Get the shared instance and create it if necessary.
+ (APIList *)sharedAPIList {
    if (apiListObj == nil) {
        apiListObj = [[super allocWithZone:NULL] init];
    }
    return apiListObj;
}

- (id)init {
    if (self = [super init]) {
        // your custom initialization
    }
    return self;
}

- (void)API_GetAllDataFromWS: (NSString *)strParam ShowLoader: (BOOL)loader showOverlay: (BOOL)overlay completion: (FetchAllRecord)completionBlock;  {
    
    [self callApiGetWithRequest:strParam showLoader:loader showOverlay:overlay completion:^(BOOL success, id responceData, NSMutableArray *arrShare) {
        completionBlock(success, responceData, arrShare);
    }];
}

- (void)API_sendLoginDetails: (NSMutableDictionary *)dict ShowLoader: (BOOL)loader showOverlay: (BOOL)overlay completion: (FetchAllRecord)completionBlock {
    NSString *url = [NSString stringWithFormat:@"%@",LOGIN];
    
    [self callApiPostWithRequest:url Dictionary:dict showLoader:loader showOverlay:overlay completion:^(BOOL success, id responceData, NSMutableArray *arrShare) {
        completionBlock(success, responceData, arrShare);
    }];
}


- (void)API_LocationServiceGetAllDataFromWS: (NSString *)strParam ShowLoader: (BOOL)loader showOverlay: (BOOL)overlay completion: (FetchAllRecord)completionBlock;  {
    
    [self callLocationServiceApiGetWithRequest:strParam showLoader:loader showOverlay:overlay completion:^(BOOL success, id responceData, NSMutableArray *arrShare) {
        completionBlock(success, responceData, arrShare);
    }];

}


@end
