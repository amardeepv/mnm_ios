//
//  NetworkCheck.m
//  WeatherTask
//
//  Created by hvt_admin on 8/11/16.
//  Copyright © 2016 Droisys. All rights reserved.
//



#import "NetworkCheck.h"
#import "Reachability.h"

@implementation NetworkCheck

+ (BOOL)isWifiOn {
    Reachability* wifiReach = [Reachability reachabilityForLocalWiFi];
    
    NetworkStatus netStatus = [wifiReach currentReachabilityStatus];
    return (netStatus==ReachableViaWiFi);
}
+ (BOOL)isInternetOff {
    Reachability* internetReach = [Reachability reachabilityForInternetConnection];
    NetworkStatus netStatus = [internetReach currentReachabilityStatus];
    return netStatus==NotReachable;
}
@end