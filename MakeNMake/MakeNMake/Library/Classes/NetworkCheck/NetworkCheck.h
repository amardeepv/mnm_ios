//
//  NetworkCheck.h
//  WeatherTask
//
//  Created by hvt_admin on 8/11/16.
//  Copyright © 2016 Droisys. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkCheck : NSObject
+ (BOOL)isWifiOn;
+ (BOOL)isInternetOff;
@end