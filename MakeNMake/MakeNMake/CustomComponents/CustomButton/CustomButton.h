//
//  CustomButton.h
//  MakeNMake
//
//  Created by Archit Saxena on 16/04/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomButton : UIButton
@property int tagIndex;
@property(nonatomic,strong) NSIndexPath *indexPath;
@end
