//
//  faqTableViewCell.m
//  MakeNMake
//
//  Created by Archit Saxena on 26/03/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "faqTableViewCell.h"

@implementation faqTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code

}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
  if (selected) {
        [_icon setImage:[UIImage imageNamed:@"UpArrowIcon"]];//UpArrowIcon
    }else{
        [_icon setImage:[UIImage imageNamed:@"downArrowIcon"]];

    }
    // Configure the view for the selected state
}

@end
