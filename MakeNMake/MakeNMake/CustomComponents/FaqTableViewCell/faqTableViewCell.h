//
//  faqTableViewCell.h
//  MakeNMake
//
//  Created by Archit Saxena on 26/03/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface faqTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *icon;
- (void)setSelected:(BOOL)selected animated:(BOOL)animated ;
@property (weak, nonatomic) IBOutlet UITextView *answerLabel;

@end
