//
//  DateCollectionViewCell.m
//  MakeNMake
//
//  Created by Archit Saxena on 01/04/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "DateCollectionViewCell.h"

@implementation DateCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)setSelected:(BOOL)selected{
    [super setSelected:selected];
   if (selected) {
        [self setBackgroundColor:[UIColor colorWithRed:129.0f/255 green:6.0f/255 blue:127.0f/255 alpha:1]];
        [self.monthLabel setTextColor:[UIColor whiteColor]];
        [self.dateNumberLabel setTextColor:[UIColor whiteColor]];
    }else{
        [self setBackgroundColor:[UIColor whiteColor]];
        [self.monthLabel setTextColor:[UIColor lightGrayColor]];
        [self.dateNumberLabel setTextColor:[UIColor lightGrayColor]];
    }

}

@end
