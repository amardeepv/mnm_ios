//
//  TenureCollectionCell.h
//  MakeNMake
//
//  Created by Archit Saxena on 12/05/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TenureCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *dateNumberLabel;
@property (assign, nonatomic) BOOL isSelectedOnce;

@end
