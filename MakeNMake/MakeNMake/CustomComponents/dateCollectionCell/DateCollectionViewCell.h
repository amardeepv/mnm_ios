//
//  DateCollectionViewCell.h
//  MakeNMake
//
//  Created by Archit Saxena on 01/04/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DateCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *dateNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *monthLabel;
@property (assign, nonatomic) BOOL isSelectedOnce;

@end
