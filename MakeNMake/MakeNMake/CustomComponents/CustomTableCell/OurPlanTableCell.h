//
//  OurPlanTableCell.h
//  MakeNMake
//
//  Created by Chirag Patel on 3/13/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OurPlanTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *unlimitedPlanButton;
@property (weak, nonatomic) IBOutlet UIButton *fixedPlanButton;
@property (weak, nonatomic) IBOutlet UIButton *addOnPlanButton;

@end
