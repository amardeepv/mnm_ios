//
//  FilterCollectionCell.h
//  MakeNMake
//
//  Created by Chirag Patel on 3/20/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblFilter;

@end
