//
//  TicketServiceNameTableViewCell.m
//  MakeNMake
//
//  Created by Archit Saxena on 22/04/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "TicketServiceNameTableViewCell.h"

@implementation TicketServiceNameTableViewCell
{
    NSArray *arrImages;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [  _cellBackView.layer setCornerRadius:8];
    [  _cellBackView.layer setMasksToBounds:true];
    
   
    
}
-(void)configureCellWithServiceName:(NSString *)serviceName andServiceDesc:(NSString *)serviceDesc andServiceIcon:(NSString*)serviceIconName{
    
    arrImages = [NSArray arrayWithObjects:@"Electrical",@"Plumbing",@"Carpentry",@"PestControl",@"acrepair",@"watertank",@"Security",@"gardening",@"cctv",@"ro",@"Homecleaning",@"Appliances",@"paintservice",@"carcleaning",@"Upholstery",@"Interior", nil];
    
    [_serviceNameLabel setText:serviceName];
    [_serviceDescLabel setText:serviceDesc];
    
    for (NSString *Str in arrImages) {
        
        if ([serviceIconName caseInsensitiveCompare:Str]== NSOrderedSame) {
           
            [_serviceImageView setImage:[UIImage imageNamed:Str]];

        }
    }
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

@end
