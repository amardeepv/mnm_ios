//
//  PageMenuCustomCell.m
//  MakeNMake
//
//  Created by archit.saxena on 23/03/17.
//  Copyright © 2017 Nripendra  singh. All rights reserved.
//

#import "PageMenuCustomCell.h"

@implementation PageMenuCustomCell
    {
        int count;
    }
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code

}

- (IBAction)didTapPageControl:(id)sender {
    CGRect frame = CGRectMake(_pageControl.frame.size.width*[_pageControl currentPage], 0, _pageControl.frame.size.width, _pageScrollView.frame.size.height);
    

    [_pageScrollView scrollRectToVisible:frame animated:YES];
}
-(void)setArrayForHomePageMenuForViewController:(UIViewController *)view{
    _pageMenuArray = [[NSMutableArray alloc]initWithObjects:@"homePageMenu1",@"homePageMenu2",@"homePageMenu3",@"homePageMenu4",@"homePageMenu5", nil];
        [view setAutomaticallyAdjustsScrollViewInsets:NO];

    [self setupScrollViewForViewController:view];

    _pageScrollView.delegate = self;
    [_pageControl setNumberOfPages:_pageMenuArray.count?_pageMenuArray.count:2];
    
    count = 0;
    [NSTimer scheduledTimerWithTimeInterval:2
                                     target:self
                                   selector:@selector(loadNextController)
                                   userInfo:nil
                                    repeats:YES];
}
-(void)loadNextController{

    [_pageScrollView setContentOffset:CGPointMake([[UIScreen mainScreen] bounds].size.width*count, 0) animated:YES];
    if (count<_pageMenuArray.count-1) {
        count++;
    }else{
        count = 0;
    }
}
    
#pragma mark - Configure ScrollView
- (void)setupScrollViewForViewController:(UIViewController*)view{
    for (int i=0; i<_pageMenuArray.count; i++) {
        UIImage *image = [UIImage imageNamed:[_pageMenuArray objectAtIndex:i]];
        [self needsUpdateConstraints];
        UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake(i*[[UIScreen mainScreen] bounds].size.width, 0, [[UIScreen mainScreen] bounds].size.width, _pageScrollView.frame.size.height)];
        //        if([UIScreen mainScreen].bounds.size.height == [IPHONE_4S_HEIGHT floatValue]){
        //            imgV.contentMode=UIViewContentModeScaleAspectFit;
        //        }
        //        else{
        imgV.contentMode=UIViewContentModeScaleToFill;
        //        }
        [imgV setImage:image];
        [_pageScrollView addSubview:imgV];

    }
    [_pageScrollView setContentSize:CGSizeMake([[UIScreen mainScreen] bounds].size.width*_pageMenuArray.count, _pageScrollView.frame.size.height)];

}
- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    //     Update the page when more than 50% of the previous/next page is visible
        CGFloat pageWidth = _pageControl.frame.size.width;
    //CGFloat pageWidth = [UIScreen mainScreen].bounds.size.width;
    int page = floor((_pageScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    _pageControl.currentPage = page;
}
-(void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    
    //    if (targetContentOffset->y == 0 && scrollView.contentOffset.y < - someOffset) { //use someOffset if you dont want to reload immedeately on scroll to top
    //        //dragged past top
    //    }
    //
    if (targetContentOffset->x == scrollView.contentSize.width-[[UIScreen mainScreen] bounds].size.width /*&&isthirdImageVisited == YES*/) {
//        OALoginVC* viewController = [[OALoginVC alloc] initWithNibName:@"OALoginVC" bundle:nil];
//        [self.navigationController pushViewController:viewController animated:YES];
    }
    if (targetContentOffset->x == scrollView.contentSize.width-[[UIScreen mainScreen] bounds].size.width) {
//        isthirdImageVisited = YES;
    }
    if (targetContentOffset->x < scrollView.contentSize.width-[[UIScreen mainScreen] bounds].size.width /* && isthirdImageVisited == YES*/) {
//        isthirdImageVisited = NO;
    }
    
}

#pragma mark -
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
