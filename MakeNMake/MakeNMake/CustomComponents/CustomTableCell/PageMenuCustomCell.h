//
//  PageMenuCustomCell.h
//  MakeNMake
//
//  Created by archit.saxena on 23/03/17.
//  Copyright © 2017 Nripendra  singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageMenuCustomCell : UITableViewCell<UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *pageScrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property(nonatomic,strong) NSMutableArray *pageMenuArray;

-(void)setArrayForHomePageMenuForViewController:(UIViewController *)view;

@end
