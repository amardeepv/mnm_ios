//
//  HSecondCustomCell.h
//  MakeNMake
//
//  Created by Nripendra on 13/03/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HSecondCustomCell : UITableViewCell// <UIWebViewDelegate>

@property (nonatomic,retain) NSString *strWebContent;
@property (weak, nonatomic) IBOutlet UILabel *descString;
-(void)configureView;
@property (weak, nonatomic) IBOutlet UILabel *serviceNamelabel;
@property (weak, nonatomic) IBOutlet UIImageView *BannerImage;

@end
