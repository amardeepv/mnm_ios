//
//  ServiceMenuTableCell.m
//  MakeNMake
//
//  Created by Chirag Patel on 3/13/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "ServiceMenuTableCell.h"
#import "ServiceCollectionCell.h"
#import "Constant.h"
@implementation ServiceMenuTableCell {
    
    NSArray *arrImages;
    NSArray *arrTitle;
    NSArray *arrList ;
    BOOL isReloadTable;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"showFilter" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selectedValueFromFilter:) name:@"showFilter" object:nil];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    CGFloat  width = ([UIScreen mainScreen].bounds.size.width)/4.0;
    width = width - 10;
    return CGSizeMake(width, 100);
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return  arrTitle.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ServiceCollectionCell *Cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ServiceCollectionCell" forIndexPath:indexPath];
    if (!Cell){
        NSArray * arrNib = [[NSBundle mainBundle ]loadNibNamed:@"ServiceCollectionCell" owner:self options:nil];
        Cell = (ServiceCollectionCell *)arrNib[0];
    }
    
    Cell.imgServices.image = [UIImage imageNamed:[arrImages objectAtIndex:indexPath.row]];
    
    Cell.lblService.text = [arrTitle objectAtIndex:indexPath.row];
    Cell.selected = YES;

    if (g_AppDelegate.arrHomeService.count>0){
    for (NSDictionary *diction in g_AppDelegate.arrHomeService) {
        
        
            if ([diction[@"serviceGroupName"] caseInsensitiveCompare:[arrTitle objectAtIndex:indexPath.row]] != NSOrderedSame) {
                Cell.layer.opacity = 0.3;
            
            }else{
                Cell.layer.opacity = 1.0;
                break;
            }
    }
    }else{
        Cell.layer.opacity = 0.3;
    }

    return Cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"%@",[arrTitle objectAtIndex:indexPath.row]);
    //Logic For get Data
    NSString *strArrTitle = [arrTitle objectAtIndex:indexPath.row];
    
    NSDictionary *dict = [NSDictionary dictionaryWithObject:strArrTitle forKey:@"FilterKey"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CollectionReloadSelect" object:dict];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    
    isReloadTable = TRUE;
    arrImages = [NSArray arrayWithObjects:@"Electrical",@"Plumbing",@"Carpentry",@"PestControl",@"acrepair",@"watertank",@"Security",@"gardening",@"cctv",@"ro",@"Homecleaning",@"Appliances",@"paintservice",@"carcleaning",@"Upholstery",@"Interior", nil];
    
    arrTitle = [NSArray arrayWithObjects:@"Electrical",@"Plumbing",@"Carpentry",@"Pest Control",@"AC",@"Watertank Cleaning",@"Security Services ",@"Gardening",@"CCTV",@"RO",@"Home Cleaning",@"Home Appliances",@"Paint Services",@"Car Spa",@"Upholstery Cleaning",@"Interior Design", nil];
   
    if (isReloadTable == TRUE) {
       // NSLog(@"%@",APPDELEGATE.arrHomeService);
        [_CollectionList registerNib:[UINib nibWithNibName:@"ServiceCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"ServiceCollectionCell"];
        _CollectionList.delegate = self;
        _CollectionList.dataSource = self;
        [_CollectionList reloadData];

    }

}

@end
