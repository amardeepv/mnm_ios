//
//  HVideoCustomCell.h
//  MakeNMake
//
//  Created by Nripendra on 13/03/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface HVideoCustomCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *videoLabel;
@property(strong, nonatomic) NSString *urlString;

-(void)videoSetupfrom:(NSString*)urlString;

@end
