//
//  ServiceMenuTableCell.h
//  MakeNMake
//
//  Created by Chirag Patel on 3/13/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceMenuTableCell : UITableViewCell <UICollectionViewDataSource,UICollectionViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *listView;
@property (weak, nonatomic) IBOutlet UICollectionView *CollectionList;
@property (weak, nonatomic) IBOutlet UITableView *tblList;
@property (retain, nonatomic) NSMutableArray *arrGetResult;
@end
