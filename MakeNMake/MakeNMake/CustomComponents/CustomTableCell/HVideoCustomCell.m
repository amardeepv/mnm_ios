//
//  HVideoCustomCell.m
//  MakeNMake
//
//  Created by Nripendra on 13/03/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "HVideoCustomCell.h"
#import <MediaPlayer/MediaPlayer.h>
#import <XCDYouTubeKit/XCDYouTubeKit.h>
//#import <AVPlayer/AVPlayer.h>
#import "Utility.h"

@implementation HVideoCustomCell
{
    MPMoviePlayerViewController *theMoviPlayer;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)videoSetupfrom:(NSString*)urlString{
    if (urlString.length>0) {
        _videoLabel.hidden = true;
        UIWebView *videoView = [Utility youtubePlayerViewWithFrame:CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width, self.frame.size.height) andURLString:urlString];
        videoView.tag = 2;
        [self addSubview:videoView];
        self.userInteractionEnabled = YES;
    }else{
        _videoLabel.hidden = false;
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
