//
//  TicketServiceNameTableViewCell.h
//  MakeNMake
//
//  Created by Archit Saxena on 22/04/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TicketServiceNameTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *serviceImageView;
@property (weak, nonatomic) IBOutlet UILabel *serviceNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *serviceDescLabel;

@property (weak, nonatomic) IBOutlet UIButton *infoButton;

-(void)configureCellWithServiceName:(NSString *)serviceName andServiceDesc:(NSString *)serviceDesc andServiceIcon:(NSString*)serviceIconName;
@property (weak, nonatomic) IBOutlet UIView *cellBackView;


@end
