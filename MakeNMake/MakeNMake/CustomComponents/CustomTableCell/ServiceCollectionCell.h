//
//  ServiceCollectionCell.h
//  MakeNMake
//
//  Created by Chirag Patel on 3/18/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgServices;
@property (weak, nonatomic) IBOutlet UILabel *lblService;

@end
