//
//  showAddressTableCell.m
//  MakeNMake
//
//  Created by Archit Saxena on 02/04/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "showAddressTableCell.h"

@implementation showAddressTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state

    if (selected) {
        [_checkBoxIcon setImage:[UIImage imageNamed:@"checkBoxEnabledIcon"]];
    }else{
        [_checkBoxIcon setImage:[UIImage imageNamed:@"checkBoxIcon"]];
        
    }
}

@end
