//
//  AddAddressCustomCell.m
//  MakeNMake
//
//  Created by Archit Saxena on 04/05/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "AddAddressCustomCell.h"

@implementation AddAddressCustomCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)configureCellWithDict:(NSMutableDictionary *)dict{
    

   

    [_addressLine1TxtFld setText:[dict valueForKey:@"title"]];
    [_cityTxtFld setText:[dict valueForKey:@"city"]];
    [_stateTxtFld setText:[dict valueForKey:@"state"]];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
