//
//  AddAddressCustomCell.h
//  MakeNMake
//
//  Created by Archit Saxena on 04/05/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddAddressCustomCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *addressHeaderLabel;
@property (weak, nonatomic) IBOutlet UITextField *addressLine1TxtFld;
@property (weak, nonatomic) IBOutlet UITextField *cityTxtFld;
@property (weak, nonatomic) IBOutlet UITextField *stateTxtFld;

-(void)configureCellWithDict:(NSMutableDictionary *)dict;

@end
