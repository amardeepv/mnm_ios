//
//  newAddressTableCell.h
//  MakeNMake
//
//  Created by Archit Saxena on 02/04/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface newAddressTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label;

@end
