//
//  OrderPlanCustumCell.m
//  MakeNMake
//
//  Created by Archit Saxena on 28/04/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "OrderPlanCustumCell.h"

@implementation OrderPlanCustumCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)configureCellWithDataDictionary:(NSDictionary*)info{

    [_orderIdLabel setText:[NSString stringWithFormat:@"- %@",[info valueForKey:@"OrderID"]]];
    [_planLabel setText:[NSString stringWithFormat:@"- %@",[info valueForKey:@"PlanName"]]];
    [_dateLabel setText:[NSString stringWithFormat:@"- %@",[info valueForKey:@"Created"]]];
    [_expiryLabel setText:[NSString stringWithFormat:@"- %@",[info valueForKey:@"Expirydate"]]];
    [_rateLabel setText:[NSString stringWithFormat:@"- ₹ %@",[info valueForKey:@"TotalCost"]]];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
