//
//  OrderCustomTableCell.h
//  MakeNMake
//
//  Created by Archit Saxena on 25/04/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomButton.h"

@protocol ReloadTableDelegate <NSObject>

@optional
-(void)reloadSectionAtIndexPath:(NSIndexPath *)indexPath isSelected:(BOOL)isSelected;

@end


@interface OrderCustomTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *ticketIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *serviceNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *datelabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet CustomButton *detailsButton;
@property (weak, nonatomic) IBOutlet UIImageView *IconImageView;
@property (weak, nonatomic) IBOutlet UILabel *engineerName;
@property (weak, nonatomic) IBOutlet CustomButton *descriptionButton;
@property (weak, nonatomic) IBOutlet UIButton *historyButton;
@property (weak, nonatomic) IBOutlet UIView *detailCustomView;
@property (weak, nonatomic) IBOutlet UIButton *feedbackButton;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descLabelHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *detailCustomViewHeightConstraint;

-(void)configureCellWithDataDictionary:(NSDictionary*)info;
@property (weak, nonatomic) IBOutlet UILabel *ticketStatus;

@property (strong , nonatomic) id<ReloadTableDelegate> delegate;



@end
