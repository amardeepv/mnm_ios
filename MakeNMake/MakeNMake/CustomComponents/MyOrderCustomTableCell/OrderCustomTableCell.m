//
//  OrderCustomTableCell.m
//  MakeNMake
//
//  Created by Archit Saxena on 25/04/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "OrderCustomTableCell.h"
#import "Constant.h"
#import "CustomButton.h"
#import "UIImageView+AFNetworking.h"
#import "Utility.h"

@implementation OrderCustomTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [_IconImageView.layer setCornerRadius:_IconImageView.frame.size.height/2];
    [_IconImageView.layer setMasksToBounds:true];
    
    [_detailsButton.layer setCornerRadius:5.0f];
    [_detailsButton.layer setMasksToBounds:true];
}

-(void)configureCellWithDataDictionary:(NSDictionary*)info{
    
    NSString *selectedDate = [[[info valueForKey:@"Expirationdate"] componentsSeparatedByString:@"T"] firstObject];
    
    selectedDate = [NSString stringWithFormat:@"%@-%@-%@",[[selectedDate componentsSeparatedByString:@"-"] lastObject],[[selectedDate componentsSeparatedByString:@"-"] objectAtIndex:1],[[selectedDate componentsSeparatedByString:@"-"] firstObject]];
    
    [_ticketIdLabel setText:[NSString stringWithFormat:@"%@",[info valueForKey:@"TicketID"]]];
    [_orderIdLabel setText:[NSString stringWithFormat:@"%@",[info valueForKey:@"OrderID"]]];
    [_priceLabel setText:[NSString stringWithFormat:@"%@",[info valueForKey:@"Amount"]]];
    [_serviceNameLabel setText:[NSString stringWithFormat:@"%@->%@",[info valueForKey:@"ServiceParentName"],[info valueForKey:@"ServiceName"]]];
    [_datelabel setText:[NSString stringWithFormat:@"%@",selectedDate]];
    [_statusLabel setText:[NSString stringWithFormat:@"%@",[info valueForKey:@"paymentStatus"]]];

    [_descLabel setText:[NSString stringWithFormat:@"%@",[info valueForKey:@"ServiceDescription"]]];
    [_ticketStatus setText:[NSString stringWithFormat:@"%@",[info valueForKey:@"ticketStatus"]]];
    [_IconImageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BACKENDBASEURL,[info valueForKey:@"engineerImagePath"]]] placeholderImage:[UIImage imageNamed:@"user-image"]];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)descriptionButton_Tapped:(CustomButton*)sender {
    
    if (sender.selected) {
        [sender setSelected:false];
        _descLabelHeightConstraint.constant = 55;
        _detailCustomViewHeightConstraint.constant = 156;
        
    }else{
        
        [sender setSelected:true];
         _descLabelHeightConstraint.constant = 0;
        _detailCustomViewHeightConstraint.constant = 101;

    }
    
    if ([_delegate respondsToSelector:@selector(reloadSectionAtIndexPath:isSelected:)]) {
        [_delegate reloadSectionAtIndexPath:sender.indexPath isSelected:sender.selected];
    }

    
}

- (IBAction)feedbackButton:(CustomButton*)sender {
 
}

@end
