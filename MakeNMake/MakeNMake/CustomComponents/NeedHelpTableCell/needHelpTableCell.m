//
//  needHelpTableCell.m
//  MakeNMake
//
//  Created by Archit Saxena on 02/05/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "needHelpTableCell.h"

@implementation needHelpTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)configureCellWithLabelText:(NSString *)text andImageName:(NSString *)imageName{
    
    [_titleLabel setText:text];
    [_IconImage setImage:[UIImage imageNamed:imageName]];
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
