//
//  needHelpTableCell.h
//  MakeNMake
//
//  Created by Archit Saxena on 02/05/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface needHelpTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *IconImage;


-(void)configureCellWithLabelText:(NSString *)text andImageName:(NSString *)imageName;


@end
