//
//  TransactionTableCell.h
//  MakeNMake
//
//  Created by Archit Saxena on 06/05/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransactionTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *transactionDescLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;

-(void)configureCellWithictionary:(NSMutableDictionary *)dict;

@end
