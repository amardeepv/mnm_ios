//
//  TransactionTableCell.m
//  MakeNMake
//
//  Created by Archit Saxena on 06/05/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "TransactionTableCell.h"

@implementation TransactionTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)configureCellWithictionary:(NSMutableDictionary *)dict{
    
    NSString *selectedDate = [[[dict valueForKey:@"Date"] componentsSeparatedByString:@"T"] firstObject];
    
    selectedDate = [NSString stringWithFormat:@"%@-%@-%@",[[selectedDate componentsSeparatedByString:@"-"] lastObject],[[selectedDate componentsSeparatedByString:@"-"] objectAtIndex:1],[[selectedDate componentsSeparatedByString:@"-"] firstObject]];
    
    [_dateLabel setText:selectedDate];
    [_transactionDescLabel setText:[dict valueForKey:@"Description"]];
    
    if ([[dict valueForKey:@"Action"] isEqualToString:@"Credit"]) {
        [_amountLabel setText:[NSString stringWithFormat:@"+%@",[dict valueForKey:@"Amount"]]];
        [_amountLabel setTextColor:[UIColor colorWithRed:10.0f/255.0f green:235.0f/255.0f blue:26.0f/255.0f alpha:1.0f]];
        
    }else{
        [_amountLabel setText:[NSString stringWithFormat:@"-%@",[dict valueForKey:@"Amount"]]];
        [_amountLabel setTextColor:[UIColor redColor]];

    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
