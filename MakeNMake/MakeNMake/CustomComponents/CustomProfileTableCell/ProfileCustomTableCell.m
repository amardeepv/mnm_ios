//
//  ProfileCustomTableCell.m
//  MakeNMake
//
//  Created by Archit Saxena on 01/05/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "ProfileCustomTableCell.h"

@implementation ProfileCustomTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)configureCellWithTitleLabel:(NSString*)titleText andTextfieldText:(NSString*)text andIconName:(NSString *)iconName{
    
    switch (_textField.tag) {
        case 0:
            [_textField setUserInteractionEnabled:false];
            break;
        case 1:
            [_textField setUserInteractionEnabled:false];
            break;
        case 2:
            [_textField setUserInteractionEnabled:true];    //Alternative Number
            break;
        case 3:
            [_textField setUserInteractionEnabled:true];    //Email id
            break;
        case 4:
            [_textField setUserInteractionEnabled:false];
            break;
        case 5:
            [_textField setUserInteractionEnabled:false];
            break;
            
        default:
            break;
    }
    
    [_fieldTitleLabel setText:titleText];
    [_textField setText:(text.length>0?text:@"")];
    [_icon setImage:[UIImage imageNamed:iconName]];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
