//
//  ProfileAddressTableCell.h
//  MakeNMake
//
//  Created by Archit Saxena on 01/05/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomButton.h"

@interface ProfileAddressTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *addLine1Label;
@property (weak, nonatomic) IBOutlet UILabel *addLine2label;
@property (weak, nonatomic) IBOutlet UILabel *addLine3Label;
@property (weak, nonatomic) IBOutlet UILabel *addHeader;
-(void)configureCellWithDictionary:(NSMutableDictionary*)dict;
@property (weak, nonatomic) IBOutlet CustomButton *editButton;

@end
