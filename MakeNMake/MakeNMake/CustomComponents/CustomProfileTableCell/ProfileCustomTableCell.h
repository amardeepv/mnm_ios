//
//  ProfileCustomTableCell.h
//  MakeNMake
//
//  Created by Archit Saxena on 01/05/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileCustomTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *fieldTitleLabel;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIImageView *icon;

-(void)configureCellWithTitleLabel:(NSString*)titleText andTextfieldText:(NSString*)text andIconName:(NSString *)iconName;

@end
