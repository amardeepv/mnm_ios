//
//  ProfileAddressTableCell.m
//  MakeNMake
//
//  Created by Archit Saxena on 01/05/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "ProfileAddressTableCell.h"

@implementation ProfileAddressTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)configureCellWithDictionary:(NSMutableDictionary*)dict{
  
    if (![[dict valueForKey:@"line1"] isKindOfClass:[NSNull class]]) {
        [_addLine1Label setText:[dict valueForKey:@"line1"]];
    }else{
        [_addLine1Label setText:@"NA"];
    }
    if (![[dict valueForKey:@"line2"] isKindOfClass:[NSNull class]]) {
        [_addLine2label setText:[dict valueForKey:@"line2"]];
    }else{
        [_addLine2label setText:@"NA"];
    }
    if (![[dict valueForKey:@"state"] isKindOfClass:[NSNull class]]) {
        [_addLine3Label setText:[dict valueForKey:@"state"]];
    }else{
        [_addLine3Label setText:@"NA"];
    }

}

@end
