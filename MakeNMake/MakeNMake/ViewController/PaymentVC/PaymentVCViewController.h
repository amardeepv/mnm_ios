//
//  PaymentVCViewController.h
//  MakeNMake
//
//  Created by Archit Saxena on 04/04/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentVCViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *orderInfoViewContainer;
@property (weak, nonatomic) IBOutlet UILabel *orderIDValue;
@property (weak, nonatomic) IBOutlet UILabel *serviceName;
@property (weak, nonatomic) IBOutlet UILabel *totalAmount;
@property (weak, nonatomic) IBOutlet UILabel *currentBalanceLabel;

@property (strong, nonatomic) NSString *orderValue;
@property (strong, nonatomic) NSString *serviceNametxt;
@property (strong, nonatomic) NSString *totalAmounttxt;
@property (strong, nonatomic) NSString *uniqueReferId;

@property (strong, nonatomic) NSString *taxRate;
@property (strong, nonatomic) NSString *serviceTaxAmount;
@property (strong, nonatomic) NSMutableDictionary* dictForPlanService;

@property (strong, nonatomic) NSString *selectedDate;
@property (strong, nonatomic) NSString *selectedTimeSlot;
@property (strong, nonatomic) NSString *selectedAddress;
@property (strong, nonatomic) NSString *addToBasketApiName;
    @property (strong, nonatomic) NSString *feedbackString;

@property (strong, nonatomic) NSArray *selectedServiceArray;

@property (assign) BOOL isFromPlan;

@property (weak, nonatomic) IBOutlet UIView *codContainerView;
@property (weak, nonatomic) IBOutlet UIView *walletContainerView;
@property (weak, nonatomic) IBOutlet UIView *citrusContainerView;
@property (weak, nonatomic) IBOutlet UIView *DebitCardContainerView;
@property (weak, nonatomic) IBOutlet UIView *walletCodContainerView;
@property (weak, nonatomic) IBOutlet UIView *walletNetbankingContainerView;

typedef enum isScreen
{
    PlanScreen,
    ServiceScreen
    
} isScreenState;

@property (assign) isScreenState screenState;

@end
