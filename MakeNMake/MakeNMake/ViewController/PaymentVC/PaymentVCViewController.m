//
//  PaymentVCViewController.m
//  MakeNMake
//
//  Created by Archit Saxena on 04/04/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "PaymentVCViewController.h"
#import "APIList.h"
#import "OrdersViewController.h"
#import "AlertView.h"
#import "Constant.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "LoadingView.h"
#import "NetworkCheck.h"

@interface PaymentVCViewController ()
{
    int selectedPaymentMode;
    NSMutableArray *serviceArray;
}
@end

@implementation PaymentVCViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self GetWalletApiCallMethod];
    [self configureView];
    
    [self addSerivcesToBasket];
    
   }

-(void)configureView{
    _orderIDValue.text = _orderValue;
    _serviceName.text = _serviceNametxt;
    _totalAmount.text = _totalAmounttxt;
    _selectedServiceArray = [[NSMutableArray alloc] initWithArray:[APPDELEGATE getSelecteServices] copyItems:YES];
    
    [self.navigationController setNavigationBarHidden:true];
    
    [_orderInfoViewContainer.layer setCornerRadius:10];
    [_orderInfoViewContainer.layer setMasksToBounds:true];
    
    [_codContainerView.layer setCornerRadius:10];
    [_codContainerView.layer setMasksToBounds:false];
    [_codContainerView.layer setBorderColor:[UIColor grayColor].CGColor];
    [_codContainerView.layer setBorderWidth:0.5f];
    [_codContainerView.layer setShadowOffset:CGSizeMake(0, 0)];
    [_codContainerView.layer setShadowRadius:15.0f];
    
    [_walletContainerView.layer setCornerRadius:10];
    [_walletContainerView.layer setMasksToBounds:true];
    
    [_DebitCardContainerView.layer setCornerRadius:10];
    [_DebitCardContainerView.layer setMasksToBounds:true];
    
    [_walletCodContainerView.layer setCornerRadius:10];
    [_walletCodContainerView.layer setMasksToBounds:true];
    
    [_citrusContainerView.layer setCornerRadius:10];
    [_citrusContainerView.layer setMasksToBounds:true];
    
    [_walletNetbankingContainerView.layer setCornerRadius:10];
    [_walletNetbankingContainerView.layer setMasksToBounds:true];
    
    [_walletNetbankingContainerView setHidden: true];

}
- (IBAction)backButton_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

#pragma mark - UIbuton Action
- (IBAction)walletButtonTapped:(id)sender {
    selectedPaymentMode = 1;
    //check for sufficient Current Balance
    [AlertView showAlert:@"This Payment mode is Coming Soon" hideAfterDelay:3.0f alertType:AJNotificationTypeRed];
}

- (IBAction)citrusButton_Tapped:(id)sender {
    selectedPaymentMode = 2;
    [AlertView showAlert:@"This Payment mode is Coming Soon" hideAfterDelay:3.0f alertType:AJNotificationTypeRed];

}
- (IBAction)debitCardButton_Tapped:(id)sender {
    selectedPaymentMode = 3;
    [AlertView showAlert:@"This Payment mode is Coming Soon" hideAfterDelay:3.0f alertType:AJNotificationTypeRed];


}
- (IBAction)walletCODButton_Tapped:(id)sender {
    selectedPaymentMode = 5;
    [AlertView showAlert:@"This Payment mode is Coming Soon" hideAfterDelay:3.0f alertType:AJNotificationTypeRed];


}
- (IBAction)walletNetbankingButton_Tapped:(id)sender {
    selectedPaymentMode = 7;
    [AlertView showAlert:@"This Payment mode is Coming Soon" hideAfterDelay:3.0f alertType:AJNotificationTypeRed];

}

- (IBAction)cod_Button_Tapped:(id)sender {
    selectedPaymentMode = 4;
    
    OrdersViewController *Obj = [[UIStoryboard storyboardWithName:@"Second" bundle:nil] instantiateViewControllerWithIdentifier:@"OrdersViewController"];

    
    Obj.txtServiceName = _serviceNametxt;
    Obj.selectedPaymentMode = selectedPaymentMode;
    Obj.txtOrderId = _orderValue;
    Obj.serviceTaxAmount = _serviceTaxAmount;
    Obj.taxRate = _taxRate;
    Obj.uniqueReferId = _uniqueReferId;
    Obj.TotalAmount = _totalAmounttxt;
    Obj.selectedServiceArray = _selectedServiceArray;
    
    if (_isFromPlan) {
        Obj.isFromPlan = TRUE;
    }else{
        Obj.isFromPlan = false;
    }
    
    [self.navigationController pushViewController:Obj animated:true];

}
#pragma mark - services
-(void)addSerivcesToBasket{
    
    serviceArray = [[NSMutableArray alloc]init];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    
    if (_screenState==ServiceScreen) {
        _addToBasketApiName = @"AddToBasket";

        NSMutableArray *AmountServices = [[NSMutableArray alloc]init];
        
        for (int i=0; i<[[APPDELEGATE getSelecteServices] count]; i++) {
            
            if ([[[[APPDELEGATE getSelecteServices] objectAtIndex:i] valueForKey:SERVICE_PRICE] intValue] != 0) {
                [AmountServices addObject:[[APPDELEGATE getSelecteServices] objectAtIndex:i]];
            }
            
        }
      
        if (AmountServices.count>0) {
            for (NSMutableDictionary *dict in AmountServices) {
                
                //remove unecessary keys
                [dict removeObjectForKey:SERVICE_NAME];
                
                //adding necessary keys
                [dict setValue:[APPDELEGATE getUserID] forKey:@"userID"];
                [dict setValue:@"MI" forKey:@"plan"];
                [dict setValue:@"1" forKey:@"noOfCalls"];
                [dict setValue:@"1" forKey:@"quantity"];
                [dict setValue:_selectedDate forKey:@"serviceDate"];
                [dict setValue:@"0" forKey:@"discount"];
                [dict setValue:[dict valueForKey:SERVICE_PRICE] forKey:@"totalAmount"];
                [dict setValue:@"1" forKey:@"status"];
                [dict setValue:[NSString stringWithFormat:@"%@",[NSDate date]] forKey:@"created"];
                [dict setValue:@"1" forKey:@"createdBy"];
                [dict setValue:_selectedTimeSlot forKey:@"slotId"];
                [dict setValue:@"0" forKey:@"couponId"];
                [dict setValue:@"0" forKey:@"couponAmount"];
                [dict setValue:_selectedAddress forKey:@"addressId"];
                [dict setValue:_feedbackString forKey:@"serviceDesc"];
                
                
                [serviceArray addObject:dict];
            }
            [dict setObject:serviceArray forKey:@"servicestoAdd"];
        }
        
        
        
    }else{
        _addToBasketApiName = @"AddToPlanBasket";
        [serviceArray addObject:_dictForPlanService];
        [dict setObject:serviceArray forKey:@"planBasketList"];

    }
    [self addToBasketCallMethodWith:dict];
}
- (void)GetWalletApiCallMethod {
    
    NSString *serviceNameWithParam = [NSString stringWithFormat:@"WalletDetailsById?UserId=%@",[APPDELEGATE getUserID]];
    
    [[APIList sharedAPIList] API_GetAllDataFromWS:serviceNameWithParam ShowLoader:NO showOverlay:YES completion:^(BOOL success, id responceData, NSMutableArray *share) {
        if (success) {
            NSDictionary * result =(NSDictionary *) responceData;
            if ([[result objectForKey:@"hasError"] isEqual:@(0)]) {
                
    _currentBalanceLabel.text = [NSString stringWithFormat:@"₹ %@",[[responceData valueForKey:@"data"] valueForKey:@"Amount"]];
            
            }
        }
    }];
}
#pragma mark - Add To Basket
#pragma mark - Call Login webservice Method


- (void)addToBasketCallMethodWith:(NSDictionary *)diction {
    [LoadingView showLoaderWithOverlay:YES];
    if([NetworkCheck isInternetOff]) {
        [AlertView showAlert:@"The Internet connection appears to be offline." alertType:AJNotificationTypeRed];
        [LoadingView dismissLoader];
        
    }else {
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSString *strPostUrl = [NSString stringWithFormat:@"%@%@",BACKENDBASEURL,_addToBasketApiName];
        
        AFJSONRequestSerializer* serialiser = [AFJSONRequestSerializer serializer];
        //    serialiser.timeoutInterval = 15.0;
        manager.responseSerializer=[AFJSONResponseSerializer serializer];
        manager.requestSerializer = serialiser;
        manager.responseSerializer.acceptableStatusCodes = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(200, 403)];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [manager POST:strPostUrl parameters:diction success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [LoadingView dismissLoader];
            
            if([[responseObject valueForKey:@"message"] isEqualToString:@"Service added in basket successfully"]){
    //if services added to basket successfully then call buy service api
            [AlertView showAlert:@"Services Added to basket succsessfully" alertType:AJNotificationTypeGreen];
                _uniqueReferId = [[responseObject valueForKey:@"data"] valueForKey:@"referenceId"];
                
                [[NSUserDefaults standardUserDefaults] setValue:@"False" forKey:@"isOpenTicket"];

                [APPDELEGATE initializeSelectionForServices];
                
                
            }else{
                [AlertView showAlert:@"Some Error occurred" alertType:AJNotificationTypeRed];

            }
    
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [LoadingView dismissLoader];
            [AlertView showAlert:[error localizedDescription] alertType:AJNotificationTypeRed];
            
        }];
    }
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
