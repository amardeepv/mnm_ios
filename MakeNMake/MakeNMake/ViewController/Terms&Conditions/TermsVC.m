//
//  Terms&ConditionVC.m
//  MakeNMake
//
//  Created by Archit Saxena on 04/05/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "TermsVC.h"

@interface TermsVC ()

@end

@implementation TermsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [_iconView.layer setShadowColor:[UIColor blackColor].CGColor];
    [_iconView.layer setShadowRadius:8.0f];
    [_iconView.layer setMasksToBounds:false];
    [_iconView.layer setShadowOpacity: 0.5];
    
    [_icon.layer setShadowColor:[UIColor blackColor].CGColor];
    [_icon.layer setShadowRadius:1.0f];
    [_icon.layer setMasksToBounds:false];
    [_icon.layer setShadowOpacity: 0.7];
    
}

-(void)viewDidLayoutSubviews {
    [_termsTextView setContentOffset:CGPointZero animated:NO];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backButton_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
