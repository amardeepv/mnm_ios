//
//  Terms&ConditionVC.h
//  MakeNMake
//
//  Created by Archit Saxena on 04/05/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TermsVC : UIViewController

@property (weak, nonatomic) IBOutlet UITextView *termsTextView;
@property (weak, nonatomic) IBOutlet UIView *iconView;
@property (weak, nonatomic) IBOutlet UIImageView *icon;

@end
