//
//  SideMenuVC.m
//  MakeNMake
//
//  Created by archit.saxena on 21/03/17.
//  Copyright © 2017 Nripendra  singh. All rights reserved.
//

#import "SideMenuVC.h"
#import "SWRevealViewController.h"
#import "AlertView.h"
#import "EditProfileViewController.h"
#import "WalletViewController.h"
#import "OfferViewController.h"
#import "OrdersViewController.h"
#import "ReferViewController.h"
#import "NotificatonViewController.h"
#import "TiketViewController.h"
#import "HelpViewController.h"
#import "AboutViewController.h"
#import "Utility.h"
#import "Constant.h"
#import "LoginViewController.h"
#import "OpenTicketVC.h"
#define cellIdentifier @"Cell"
#define cellIdentifierProfile @"CellProfile"
#import "ServicePlanSegmentVC.h"
#import "AboutUsVC.h"
#import "ReferAndEarnVC.h"
#import "ProfileSegmentVC.h"
#import "NeedHelpVC.h"
#import "MyWalletVC.h"
#import "TermsVC.h"
#import "LoginSegment.h"
#import "MyProfileVC.h"

@interface SideMenuVC ()
{
    NSMutableArray *arrayForSectionFirst,*arrayForSectionSecond, *arrayForSectionThird;
    NSMutableArray *iconArrayForSectionFirst,*iconArrayForSectionSecond, *iconArrayForSectionThird;
    NSMutableArray *viewArrayForSectionFirst,*viewArrayForSectionSecond, *viewArrayForSectionThird;

}
@end

@implementation SideMenuVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //registering Nib for profile Pic
    UINib* nibProfile = [UINib nibWithNibName:@"ProfilePicTableViewCell" bundle:nil];
    [_sideMenuTableView registerNib:nibProfile forCellReuseIdentifier:cellIdentifierProfile];
    
    //IconCustomTableViewCell.h
    UINib* nibGeneric = [UINib nibWithNibName:@"IconCustomTableViewCell" bundle:nil];
    [_sideMenuTableView registerNib:nibGeneric forCellReuseIdentifier:cellIdentifier];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [_sideMenuTableView reloadData];
    arrayForSectionFirst = [[NSMutableArray alloc]initWithObjects:@"My Profile",@"My Order",@"My Wallet", nil];
    iconArrayForSectionFirst = [[NSMutableArray alloc]initWithObjects:@"user",@"myorders",@"wallets", nil];
    viewArrayForSectionFirst = [[NSMutableArray alloc]initWithObjects:@"EditProfileViewController",@"OrdersViewController",@"WalletViewController", nil];
    
    
    arrayForSectionSecond = [[NSMutableArray alloc]initWithObjects:@"Refer and Earn",@"Purchased Services",@"About us", nil];
    iconArrayForSectionSecond = [[NSMutableArray alloc]initWithObjects:@"members",@"tickets",@"info", nil];
    viewArrayForSectionSecond = [[NSMutableArray alloc]initWithObjects:@"ReferViewController",@"TiketViewController",@"AboutViewController", nil];
    
    if ([[Utility sharedInstance] unArchiveFileWithFileName:OA_LOGIN_FILE]) {

        arrayForSectionThird = [[NSMutableArray alloc]initWithObjects:@"Need Help",@"Terms & Conditions",@"LogOut", nil];
    }else{
        arrayForSectionThird = [[NSMutableArray alloc]initWithObjects:@"Need Help",@"Terms & Conditions",@"Login | Register", nil];
        }
    iconArrayForSectionThird = [[NSMutableArray alloc]initWithObjects:@"help",@"checkMark",@"logout", nil];
    viewArrayForSectionThird = [[NSMutableArray alloc]initWithObjects:@"HelpViewController", nil];
    

}
- (BOOL)prefersStatusBarHidden {
    return YES;
}
#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return 1;
            break;
        case 1:
            return arrayForSectionFirst.count;
            break;
        case 2:
            return arrayForSectionSecond.count;
            break;
        case 3:
            return arrayForSectionThird.count;
            break;
        default:
            return 1;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   //Profile View Cell
    ProfilePicTableViewCell* profileCell = (ProfilePicTableViewCell*)[_sideMenuTableView dequeueReusableCellWithIdentifier:cellIdentifierProfile];
    //Generic View Cell
    IconCustomTableViewCell* genCell = (IconCustomTableViewCell*)[_sideMenuTableView dequeueReusableCellWithIdentifier:cellIdentifier];

    //disabling selection highlight
    [profileCell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [genCell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    switch (indexPath.section) {
        case 0://first section will show Profile Pic cell with UserName
        {
            if ([[Utility sharedInstance] unArchiveFileWithFileName:OA_LOGIN_FILE]) {
                if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"userName"] length]>0) {
                    [profileCell.userName setText:[NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults] valueForKey:@"userName"] uppercaseString]]];

                }else{
                    [profileCell.userName setText:[[NSString stringWithFormat:@"%@",@"Guest"] uppercaseString]];

                }

            }else{
                [profileCell.userName setText:[[NSString stringWithFormat:@"%@",@"Guest"] uppercaseString]];

            }
            
            return profileCell;

        }break;
            
        case 1:
        {
            [genCell.txtLabel setText:[[arrayForSectionFirst objectAtIndex:indexPath.row] uppercaseString]];
            [genCell.iconImageView setImage:[UIImage imageNamed:[iconArrayForSectionFirst objectAtIndex:indexPath.row]]];
            return genCell;
            
        }break;
        case 2:
        {
            [genCell.txtLabel setText:[[arrayForSectionSecond objectAtIndex:indexPath.row] uppercaseString]];
            [genCell.iconImageView setImage:[UIImage imageNamed:[iconArrayForSectionSecond objectAtIndex:indexPath.row]]];
            return genCell;

        }break;
        case 3:
        {
            [genCell.txtLabel setText:[[arrayForSectionThird objectAtIndex:indexPath.row] uppercaseString]];
            [genCell.iconImageView setImage:[UIImage imageNamed:[iconArrayForSectionThird objectAtIndex:indexPath.row]]];
            return genCell;

        }break;
        default: //Handling default case
        {
            [genCell.txtLabel setText:[[arrayForSectionFirst objectAtIndex:indexPath.row] uppercaseString]];
            [genCell.txtLabel setText:[[iconArrayForSectionFirst objectAtIndex:indexPath.row] uppercaseString]];
            return genCell;

        }break;
    }
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [footerView setBackgroundColor:[UIColor grayColor]];
    return footerView;
}
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   
    NSString *strIdentifier = nil;
    
    switch (indexPath.section) {
        case 0:
        {
            //first section will show Profile Pic cell with UserName
            //do nothing
        }break;
            
        case 1:
        {//MyOrdersVC.h
            switch (indexPath.row) {
                case 0:
                {//My Profile
                    
                    ProfileSegmentVC *Obj = [[ProfileSegmentVC alloc] initWithNibName:@"ProfileSegmentVC" bundle:nil];
                    [self.navigationController pushViewController:Obj animated:true];
                    
                }break;
                case 1:
                {//My Order
                    
                    ServicePlanSegmentVC *Obj = [[ServicePlanSegmentVC alloc] initWithNibName:@"ServicePlanSegmentVC" bundle:nil];
                    [self.navigationController pushViewController:Obj animated:true];
                    
                }break;
                case 2:
                {
                    //My Wallet
                    MyWalletVC *Obj = [[MyWalletVC alloc] initWithNibName:@"MyWalletVC" bundle:nil];
                    [self.navigationController pushViewController:Obj animated:true];
                }
                break;
                    // strIdentifier = [viewArrayForSectionSecond objectAtIndex:indexPath.row];
            }
        }break;
        case 2:
        {
            
            switch (indexPath.row) {

                case 0:{
                    //Refer and Earn
                    
                    ReferAndEarnVC *Obj = [[ReferAndEarnVC alloc] initWithNibName:@"ReferAndEarnVC" bundle:nil];
                    [self.navigationController pushViewController:Obj animated:true];
                }break;
                case 1:
                {//Open Ticket
                    if ([[Utility sharedInstance] unArchiveFileWithFileName:OA_LOGIN_FILE]) {
                        
                    MyProfileVC *Obj = [[MyProfileVC alloc]initWithNibName:@"MyProfileVC" bundle:nil];
                    Obj.fromScreen = TicketScreen;
                    [self.navigationController pushViewController:Obj animated:true];
                        
                    }else{
                        LoginSegment  *loginObj = [[LoginSegment alloc]initWithNibName:@"LoginSegment" bundle:nil];
                        loginObj.loginState = loginScreen;
                        [self.navigationController pushViewController:loginObj animated:YES];
                    }
                   // OpenTicketVC *Obj = [[OpenTicketVC alloc] initWithNibName:@"OpenTicketVC" bundle:nil];
                    //[self.navigationController pushViewController:Obj animated:true];
                    
                }break;
                case 2:
                { //About Us
                    AboutUsVC *Obj = [[AboutUsVC alloc] initWithNibName:@"AboutUsVC" bundle:nil];
                    [self.navigationController pushViewController:Obj animated:true];
                    
                }break;

            }
        }break;
        case 3:
        {
            switch (indexPath.row) {
                case 0: {//Need Help
                    
                    NeedHelpVC *Obj = [[NeedHelpVC alloc] initWithNibName:@"NeedHelpVC" bundle:nil];
                    [self.navigationController pushViewController:Obj animated:true];
                }break;
                case 1:{
                    
                    TermsVC *Obj = [[TermsVC alloc] initWithNibName:@"TermsVC" bundle:nil];
                    [self.navigationController pushViewController:Obj animated:true];

                }break;
                case 2:{
                    //Logout Handling
                    if ([[Utility sharedInstance] unArchiveFileWithFileName:OA_LOGIN_FILE]) {
                        [[Utility sharedInstance] logoutUserWithFileName:OA_LOGIN_FILE];
                        [APPDELEGATE setupSlideBar];
                        
                        [AlertView showAlert:@"User Logged Out Successfully" alertType:AJNotificationTypeRed];
                        
                    }else{
                        //do nothing
                        LoginSegment  *loginObj = [[LoginSegment alloc]initWithNibName:@"LoginSegment" bundle:nil];
                        loginObj.loginState = loginScreen;
                        [self.navigationController pushViewController:loginObj animated:YES];
                    }
                }break;
            }
        }break;
        default: //Handling default case
        {
            strIdentifier = [viewArrayForSectionThird objectAtIndex:indexPath.row];

            
        }break;
    }
  
    if (strIdentifier!=nil) {
        UIViewController *controller = [[UIStoryboard storyboardWithName:@"Second" bundle:nil] instantiateViewControllerWithIdentifier:strIdentifier];
        [self.revealViewController setFrontViewController:controller];
        [self.revealViewController revealToggleAnimated:YES];

    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        return 120;
    }else{
        return 40;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section==3) {
        return 0.001;
    }
        return 1.5;
}

#pragma mark -
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
