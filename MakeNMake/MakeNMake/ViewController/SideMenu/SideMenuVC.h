//
//  SideMenuVC.h
//  MakeNMake
//
//  Created by archit.saxena on 21/03/17.
//  Copyright © 2017 Nripendra  singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "profilePicTableViewCell.h"
#import "IconCustomTableViewCell.h"

@interface SideMenuVC : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *sideMenuTableView;

@end
