//
//  ContactUsVC.m
//  MakeNMake
//
//  Created by Archit Saxena on 06/05/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "ContactUsVC.h"

@interface ContactUsVC ()

@end

@implementation ContactUsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self configureUiView];
}

-(void)configureUiView{
    [_iconView.layer setShadowColor:[UIColor blackColor].CGColor];
    [_iconView.layer setShadowRadius:8.0f];
    [_iconView.layer setMasksToBounds:false];
    [_iconView.layer setShadowOpacity: 0.5];
}

- (IBAction)backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
