//
//  AboutUsVC.h
//  MakeNMake
//
//  Created by Archit Saxena on 30/04/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutUsVC : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *aboutUsTextView;
@property (weak, nonatomic) IBOutlet UIView *iconView;
@property (weak, nonatomic) IBOutlet UIImageView *icon;

@end
