//
//  HomeViewController.m
//  MakeNMake
//
//  Created by Chirag Patel on 2/8/17.
//  Copyright © 2017 Nripendra  singh. All rights reserved.
//

#import "HomeViewController.h"
#import "ServiceMenuTableCell.h"
#import "OurPlanTableCell.h"
#import "PageMenuCustomCell.h"
#import "APIList.h"
#import "Constant.h"
#import "FilterCollectionCell.h"
#import "HomeDetailViewController.h"
#import "segmentViewServiceDetailVC.h"
#import "AlertView.h"

//https://maps.googleapis.com/maps/api/place/autocomplete/json?=types=geocode&sensor=false&components=country:IN&key=AIzaSyC2Z2qRrXy_WJLHSyyrfBMTXbYObgwhbys&input=gurgaon
@interface HomeViewController (){
    float  calculationHeight;
    float TempHeight;
    NSArray *filteredArray;
    NSArray *TempFilterArr;
    BOOL isLocationFilter;
    UITapGestureRecognizer *tapGestureRecognizer;
    SWRevealViewController *revealController;
    
}
@end

@implementation HomeViewController


- (BOOL)prefersStatusBarHidden {
    return YES;
}

#pragma arguments
#pragma mark - ViewDidLoad Method

- (void)viewDidLoad {
    [super viewDidLoad];
    _filterView.hidden = true;

    filteredArray = [NSArray array];
    TempFilterArr = [NSArray array];
    self.viewCollection.hidden = YES;
    NSLog(@"%@",APPDELEGATE.arrHomeService);
    [_collectionList registerNib:[UINib nibWithNibName:@"FilterCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"FilterCollectionCell"];
    _collectionList.delegate = self;
    _collectionList.dataSource = self;
    [_collectionList reloadData];
    
  tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom:)];
    tapGestureRecognizer.delegate = self;
    
    [_tblHome addGestureRecognizer:tapGestureRecognizer];
    //[_viewCollection removeGestureRecognizer:tapGestureRecognizer];
   // [_tblHome removeGestureRecognizer:tapGestureRecognizer];
    
    
    [self GetServiceCategoryApiCallMethodwithCityName:@"Gurugram"];
    revealController = self.revealViewController;
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];

    
    [_btnMenu addTarget:revealController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    //self.tblHome.rowHeight = UITableViewAutomaticDimension;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"showFilter" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"CollectionSelect" object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selectedValueFromFilter:) name:@"showFilter" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(CollectionSelectValue:) name:@"CollectionReloadSelect" object:nil];
   
}
#pragma mark- UIGestureRecognizerDelegate methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:_tblHome]) {
        
        // Don't let selections of auto-complete entries fire the
        // gesture recognizer
        return NO;
    }
    else if ([touch.view isDescendantOfView:_viewCollection]) {
        
        // Don't let selections of auto-complete entries fire the
        // gesture recognizer
        return NO;
    }else if ([touch.view isDescendantOfView:self.collectionList]) {
        
        // Don't let selections of auto-complete entries fire the
        // gesture recognizer
        return NO;
    }else if ([touch.view isDescendantOfView: [UIView new]]) {
        
        // Don't let selections of auto-complete entries fire the
        // gesture recognizer
        return NO;
    }else{
        return YES;
    }
}

#pragma arguments
#pragma mark - FilterNotificationGet Method

-(void)CollectionSelectValue:(NSNotification *)notification{
    NSDictionary *dict = notification.object;
    NSString *strFilter = [dict valueForKey:@"FilterKey"] ;
    NSLog(@"FilterKey %@",strFilter);
    [self comonLogicForDidSelect:strFilter];
}


-(void)selectedValueFromFilter:(NSNotification *)notification{
    NSDictionary *dict = notification.object;
    NSString *strFilter = [dict valueForKey:@"FilterKey"] ;
    NSLog(@"FilterKey %@",strFilter);
    
}

- (void) handleTapFrom: (UITapGestureRecognizer *)recognizer
{
    _txtFilter.text = @"Search";
    if (!(APPDELEGATE.addressLabel.length>0)) {
        _txtLocation.text = @"Gurugram";
        
    }else{
        _txtLocation.text = APPDELEGATE.addressLabel;
        
    }    self.viewCollection.hidden = true;
    
    _filterView.hidden = true;
    self.innerView.hidden = false;
    
    [_txtLocation resignFirstResponder];
    [_txtFilter resignFirstResponder];
}

#pragma arguments
#pragma mark - ViewWillAppear Method

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (g_AppDelegate.addressLabel.length > 0) {
    }else {
        [g_AppDelegate GetLocationMethod];
    }
    if (!(APPDELEGATE.addressLabel.length>0)) {
        _txtLocation.text = @"Gurugram";

    }else{
        _txtLocation.text = APPDELEGATE.addressLabel;

    }
    self.innerView.hidden = NO;
   // self.viewCollection.hidden = NO;
    
}
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [_tblHome reloadData];
//    [_tblHome sizeToFit];
    
    calculationHeight = [[UIScreen mainScreen] bounds].size.width/4.0;
    calculationHeight = calculationHeight + 25;
    TempHeight = calculationHeight * 4;
    TempHeight = TempHeight + 650;
    [_homeScrollView setContentSize:CGSizeMake(_tblHome.frame.size.width, TempHeight+40)];
    self.tblLayoutHeight.constant = TempHeight;
}

#pragma arguments
#pragma mark - GetServices Api Call Method


- (IBAction)btnFilterPressed:(id)sender {
    _filterView.hidden = false;
    self.innerView.hidden = true;
    
}

#pragma arguments
#pragma mark - TextFieldDelegate Method
- (void)textFieldDidBeginEditing:(UITextField *)textField{           // became first responder
    if (textField.tag==111) {
        isLocationFilter = true;
        _txtLocation.text = @"";
    }else{
        isLocationFilter = false;
        _txtFilter.text = @"";

    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField reason:(UITextFieldDidEndEditingReason)reason {
//
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField.tag==111) {
        if (!(APPDELEGATE.addressLabel.length>0)) {
            _txtLocation.text = @"Gurugram";
            
        }else{
            _txtLocation.text = APPDELEGATE.addressLabel;
            
        }    }else {
        _txtFilter.text = @"Search";
    }
    self.viewCollection.hidden = true;
    [textField resignFirstResponder];
    return true;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
  
    if (textField.tag==111) {
        if (string.length >0) {
            _lblFilter.hidden = YES;
            self.viewCollection.hidden = NO;
            
        }else{
            if (textField.text.length > 0) {
                self.viewCollection.hidden = NO;
                
                _lblFilter.hidden = YES;
            }else{
                _lblFilter.hidden = NO;
                self.viewCollection.hidden = NO;
                
            }
        }

        _filterCollectionViewTopConstraint.constant = -180;
       
        //location filter
        [self LocationSearchServicesApiCallMethod:[[_txtLocation.text stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"," withString:@""]];
        _viewCollection.hidden = false;
        [_viewCollection bringSubviewToFront:_collectionList];
        
        return YES;

    }else{
        if (string.length >0) {
            _lblFilter.hidden = YES;
            self.viewCollection.hidden = NO;
            
        }else{
            if (textField.text.length > 0) {
                self.viewCollection.hidden = NO;
                
                _lblFilter.hidden = YES;
            }else{
                _lblFilter.hidden = NO;
                self.viewCollection.hidden = NO;
                
            }
        }
        _filterCollectionViewTopConstraint.constant = 0;

    NSPredicate *bPredicate =
    [NSPredicate predicateWithFormat:@"SELF contains[cd] %@",_txtFilter.text];
    NSLog(@"%@",[g_AppDelegate.arrHomeService valueForKey:@"serviceGroupName"]);
    filteredArray = [[g_AppDelegate.arrHomeService valueForKey:@"serviceGroupName"] filteredArrayUsingPredicate:bPredicate];
    NSLog(@" HEARE %@",filteredArray);
    [self.collectionList reloadData];
    return YES;
}
}


#pragma arguments
#pragma mark - Location Services Called Method

- (void)LocationSearchServicesApiCallMethod:(NSString*)strSearchText {
    
    
    NSString *strSearch = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/autocomplete/json?=types=geocode&sensor=false&components=country:IN&key=AIzaSyC2Z2qRrXy_WJLHSyyrfBMTXbYObgwhbys&input=%@",strSearchText];
    
    [[APIList sharedAPIList] API_LocationServiceGetAllDataFromWS:strSearch ShowLoader:NO showOverlay:YES completion:^(BOOL success, id responceData, NSMutableArray *share) {
        if (success) {
            NSDictionary * result =(NSDictionary *) responceData;
            if ([[result objectForKey:@"status"] caseInsensitiveCompare:@"OK"] == NSOrderedSame) {
//                NSLog(@"WS Response %@",result[@"predictiions"]);
                filteredArray = result[@"predictions"];
                        [self.collectionList reloadData];
                [_viewCollection setHidden:false];
                
                
                /*if ([result[@"predictions"] count]>0) {
                    if ([[result[@"predictions"] firstObject] valueForKey:@"description"]) {
                        _txtLocation.text = [[result[@"predictions"] firstObject] valueForKey:@"description"];
                    }

                }*/
            }
        }else{
            [AlertView showAlert:@"Some error occured" alertType:AJNotificationTypeRed];

        }
    }];
}


#pragma arguments
#pragma mark - GetAllServices Method

    - (void)GetServiceCategoryApiCallMethodwithCityName:(NSString *)cityName {
    NSString *locationUrl = [NSString stringWithFormat:@"SearchServiceByCity?CityName=%@",cityName];
 
    [[APIList sharedAPIList] API_GetAllDataFromWS:locationUrl ShowLoader:YES showOverlay:YES completion:^(BOOL success, id responceData, NSMutableArray *share) {
        if (success) {
            NSDictionary * result =(NSDictionary *) responceData;
            if ([[result objectForKey:@"hasError"] isEqual:@(0)]) {
                [self GetArrayFromResponse:[result valueForKey:@"data"]];
            }else{
                NSLog(@"Error Occurred");
                [AlertView showAlert:[result objectForKey:@"message"] alertType:AJNotificationTypeBlue];
            }
            
        }else{
            [AlertView showAlert:@"Some Error Occurred" alertType:AJNotificationTypeBlue];
        }
    }];
}

#pragma arguments
#pragma mark - ParseResponse Method

- (void)GetArrayFromResponse:(NSMutableArray *)arrResponse {
    g_AppDelegate.arrHomeService = [NSMutableArray array];
    NSLog(@"service - %@",[arrResponse valueForKey:@"serviceGroupName"]);
    
    for (NSDictionary *dictionResult in arrResponse) {
        [g_AppDelegate.arrHomeService addObject:dictionResult];
    }
    [self.tblHome reloadData];
    [_tblHome sizeToFit];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma arguments
#pragma mark -UITableview Delegate & Datasource Method

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
       
        PageMenuCustomCell * Cell = [tableView dequeueReusableCellWithIdentifier:@"PageMenuCustomCell"];
        if (!Cell){
            NSArray * arrNib = [[NSBundle mainBundle ]loadNibNamed:@"PageMenuCustomCell" owner:self options:nil];
            Cell = (PageMenuCustomCell *)arrNib[0];
            [Cell setArrayForHomePageMenuForViewController:self];
        }
        Cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return Cell;
    }else if(indexPath.section == 1) {
        ServiceMenuTableCell * Cell = [tableView dequeueReusableCellWithIdentifier:@"ServiceMenuTableCell"];
        if (!Cell){
            NSArray * arrNib = [[NSBundle mainBundle ]loadNibNamed:@"ServiceMenuTableCell" owner:self options:nil];
            Cell = (ServiceMenuTableCell *)arrNib[0];
        }
        [Cell removeGestureRecognizer:tapGestureRecognizer];
        [Cell removeGestureRecognizer:revealController.panGestureRecognizer];
        
        Cell.selectionStyle = UITableViewCellSelectionStyleNone;
        //[Cell setSelected:true animated:true];
        [Cell.arrGetResult addObjectsFromArray:APPDELEGATE.arrHomeService];
        return Cell;

    }else {
        OurPlanTableCell * Cell = [tableView dequeueReusableCellWithIdentifier:@"OurPlanTableCell"];
        if (!Cell){
            NSArray * arrNib = [[NSBundle mainBundle ]loadNibNamed:@"OurPlanTableCell" owner:self options:nil];
            Cell = (OurPlanTableCell *)arrNib[0];
        }
        [Cell.unlimitedPlanButton addTarget:self action:@selector(unlimitedPlanButton_Tapped:) forControlEvents:UIControlEventTouchUpInside];
        [Cell.fixedPlanButton addTarget:self action:@selector(fixedPlanButton_Tapped:) forControlEvents:UIControlEventTouchUpInside];
        [Cell.addOnPlanButton addTarget:self action:@selector(addOnPlanButton_Tapped:) forControlEvents:UIControlEventTouchUpInside];
        
        
        Cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return Cell;
       
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0){ // Collection View section
        return 170;//Home Page Menu Section

    }else if(indexPath.section == 1) {
        return 450;/*calculationHeight * 4;*/

    }else {
        return 500; // Our Plan Section
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

#pragma arguments
#pragma mark - UIcollection Delegate & Datasource Method
// logic for filter table data show.

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    CGFloat  width = self.collectionList.frame.size.width;
    if (filteredArray.count>0) {
        
    [self.collectionList setNeedsDisplay];
    _filteredListHeightConstraint.constant = filteredArray.count*30;
        if (_filteredListHeightConstraint.constant > 150) {
            _filteredListHeightConstraint.constant = 150;
        }else{
            _filteredListHeightConstraint.constant = filteredArray.count*30;
        }
    }else{
        _filteredListHeightConstraint.constant = 0;
    }

    return CGSizeMake(width, 30);
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (filteredArray.count>0) {
        _viewCollection.hidden = false;
    }else{
        _viewCollection.hidden = true;

    }
    return  filteredArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    FilterCollectionCell *Cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FilterCollectionCell" forIndexPath:indexPath];

    if (!Cell){
        NSArray * arrNib = [[NSBundle mainBundle ]loadNibNamed:@"FilterCollectionCell" owner:self options:nil];
        Cell = (FilterCollectionCell *)arrNib[0];
    }
    if (isLocationFilter) {
        Cell.lblFilter.text = [[filteredArray objectAtIndex:indexPath.row] valueForKey:@"description"];

    }else{
        Cell.lblFilter.text = [filteredArray objectAtIndex:indexPath.row];

    }
    return Cell;
}

#pragma arguments
#pragma mark - UICollectiond Delegate  Method

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (isLocationFilter) {
        NSLog(@"%@",[[filteredArray objectAtIndex:indexPath.row] valueForKey:@"terms"]);
        
        _txtFilter.text = [[[filteredArray objectAtIndex:indexPath.row] valueForKey:@"terms"] count]>3?[[[[filteredArray objectAtIndex:indexPath.row] valueForKey:@"terms"]objectAtIndex:2]  valueForKey:@"value"]:[[[[filteredArray objectAtIndex:indexPath.row] valueForKey:@"terms"] firstObject] valueForKey:@"value"];
        
        _txtLocation.text = [[[filteredArray objectAtIndex:indexPath.row] valueForKey:@"terms"] count]>3?[[[[filteredArray objectAtIndex:indexPath.row] valueForKey:@"terms"]objectAtIndex:2]  valueForKey:@"value"]:[[[[filteredArray objectAtIndex:indexPath.row] valueForKey:@"terms"] firstObject] valueForKey:@"value"];
        
        self.viewCollection.hidden = true;
        
        _filterView.hidden = true;
        self.innerView.hidden = false;
        
        [_txtLocation resignFirstResponder];
        [_txtFilter resignFirstResponder];
        
        [self GetServiceCategoryApiCallMethodwithCityName:_txtFilter.text];

    }else{
        [self comonLogicForDidSelect:[filteredArray objectAtIndex:indexPath.row]];
    }
}

#pragma arguments
#pragma mark - DidSelectonService  Method

- (void)comonLogicForDidSelect:(NSString*)strFilter {
    BOOL isPushed = false;
    
    for (NSDictionary *diction in g_AppDelegate.arrHomeService) {
        
        if( [diction[@"serviceGroupName"] caseInsensitiveCompare:strFilter] == NSOrderedSame ) {
            NSMutableArray *arrList = [NSMutableArray array];
            [arrList addObject:diction];
            segmentViewServiceDetailVC *controller = [[segmentViewServiceDetailVC alloc]initWithNibName:@"segmentViewServiceDetailVC" bundle:nil];;
            controller.arrList = arrList;
            controller.NameTitle = [[arrList  valueForKey:@"serviceGroupName"] objectAtIndex:0];
            isPushed = true;
            [self.navigationController pushViewController:controller animated:YES];
            break;
        }
    }
    
    if (!isPushed) {
        for (NSDictionary *diction in g_AppDelegate.arrHomeService) {
            if ([diction[@"serviceGroupName"] caseInsensitiveCompare:strFilter] != NSOrderedSame) {
                [AlertView showAlert:@"Service coming soon in this city" alertType:AJNotificationTypeBlue];
                
                break;
            }
        }
    }
    


    
}
#pragma mark - Plan Button Actions

-(void)unlimitedPlanButton_Tapped:(id)sender{
    segmentViewServiceDetailVC *controller = [[segmentViewServiceDetailVC alloc]initWithNibName:@"segmentViewServiceDetailVC" bundle:nil];;
    controller.isFromPlanScreen = @"UnlimitedPlanVC";
    controller.NameTitle = @"Unlimited Plan";
    [self.navigationController pushViewController:controller animated:YES];
}
-(void)fixedPlanButton_Tapped:(id)sender{
    segmentViewServiceDetailVC *controller = [[segmentViewServiceDetailVC alloc]initWithNibName:@"segmentViewServiceDetailVC" bundle:nil];;
    controller.isFromPlanScreen = @"FixedPlanVC";
    controller.NameTitle = @"Fixed Plan";
    [self.navigationController pushViewController:controller animated:YES];
}
-(void)addOnPlanButton_Tapped:(id)sender{
    segmentViewServiceDetailVC *controller = [[segmentViewServiceDetailVC alloc]initWithNibName:@"segmentViewServiceDetailVC" bundle:nil];;
    controller.isFromPlanScreen = @"AddOnPlanVC";
    controller.NameTitle = @"Add On Plan";
    [self.navigationController pushViewController:controller animated:YES];
}


@end
