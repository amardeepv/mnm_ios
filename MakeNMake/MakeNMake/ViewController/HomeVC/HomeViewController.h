//
//  HomeViewController.h
//  MakeNMake
//
//  Created by Chirag Patel on 2/8/17.
//  Copyright © 2017 Nripendra  singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"

@interface HomeViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIGestureRecognizerDelegate>

@property (weak,nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic ) IBOutlet UITableView *tblHome;
@property (weak, nonatomic) IBOutlet UITextField *txtLocation;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tblLayoutHeight;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionList;
@property (weak, nonatomic) IBOutlet UIView *filterView;
@property (weak, nonatomic) IBOutlet UIView *innerView;
@property (weak, nonatomic) IBOutlet UITextField *txtFilter;
@property (weak, nonatomic) IBOutlet UILabel *lblFilter;
@property (weak, nonatomic) IBOutlet UIView *viewCollection;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *filteredListHeightConstraint;
@property (weak, nonatomic) IBOutlet UIScrollView *homeScrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *filterCollectionViewTopConstraint;

@property (weak, nonatomic) IBOutlet UIButton *btnFilter;
- (IBAction)btnFilterPressed:(id)sender;

@end
