//
//  faqVC.m
//  MakeNMake
//
//  Created by Archit Saxena on 26/03/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "faqVC.h"
#import "faqTableViewCell.h"
#import "Utility.h"

@interface faqVC ()

@end

@implementation faqVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self registerCells];
    [self setUpFaqQuestionArray];

}
- (void)registerCells
{
        [self.faqTableView registerNib:[UINib nibWithNibName:@"faqTableViewCell" bundle:nil]forCellReuseIdentifier:@"faqTableViewCell"];
}

-(void)setUpFaqQuestionArray{
    
    _faqQuestionArray = [[NSMutableArray alloc]init ];

    if ([_serviceName isEqualToString:@"UnlimitedPlanVC"]) {
        _faqQuestionArray = [[Utility sharedInstance]getFaqQuestionForService:@"UnlimitedPlanVC"];
        _faqAnswerArray = [[NSMutableArray alloc]init];
        _faqAnswerArray = [[Utility sharedInstance]getFaqAnswerForService:@"UnlimitedPlanVC"];
        
    }else if ([_serviceName isEqualToString:@"FixedPlanVC"]){
        _faqQuestionArray = [[Utility sharedInstance]getFaqQuestionForService:@"FixedPlanVC"];
        _faqAnswerArray = [[NSMutableArray alloc]init];
        _faqAnswerArray = [[Utility sharedInstance]getFaqAnswerForService:@"FixedPlanVC"];
        
    }else if ([_serviceName isEqualToString:@"AddOnPlanVC"]){
        _faqQuestionArray = [[Utility sharedInstance]getFaqQuestionForService:@"AddOnPlanVC"];

    }else{
    _faqQuestionArray = [[Utility sharedInstance]getFaqQuestionForService:_serviceName];
    
    _faqAnswerArray = [[NSMutableArray alloc]init];
    _faqAnswerArray = [[Utility sharedInstance]getFaqAnswerForService:_serviceName];
    }
}

-(UITableViewCell *)cellForFAQCell:(NSIndexPath *)indexPath : (UITableView *)tableView{
    faqTableViewCell * Cell = [tableView dequeueReusableCellWithIdentifier:@"faqTableViewCell"];
    [Cell.answerLabel setContentOffset:CGPointZero animated:NO];

    if (!Cell){
        NSArray * arrNib = [[NSBundle mainBundle ]loadNibNamed:@"faqTableViewCell" owner:self options:nil];
        Cell = (faqTableViewCell *)arrNib[0];
    }
    
    if ([_serviceName isEqualToString:@"UnlimitedPlanVC"]) {
        Cell.questionLabel.text = [_faqQuestionArray objectAtIndex:indexPath.section];
        Cell.answerLabel.text = [_faqAnswerArray objectAtIndex:indexPath.section];

    }else if ([_serviceName isEqualToString:@"FixedPlanVC"]){
        Cell.questionLabel.text = [_faqQuestionArray objectAtIndex:indexPath.section];
        Cell.answerLabel.text = [_faqAnswerArray objectAtIndex:indexPath.section];

    }else if ([_serviceName isEqualToString:@"AddOnPlanVC"]){
        [Cell.icon  setHidden:true];
        [Cell.answerLabel  setHidden:true];
        Cell.questionLabel.text = [_faqQuestionArray objectAtIndex:indexPath.section];
        
    }else{
        Cell.answerLabel.text = [_faqAnswerArray objectAtIndex:indexPath.section];
        Cell.questionLabel.text = [_faqQuestionArray objectAtIndex:indexPath.section];
        
    }
   
    Cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return Cell;
}

#pragma Mark - UITableView delegate and datasource.
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
        return _faqQuestionArray.count;

}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        return  [self cellForFAQCell:indexPath :tableView];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
  
    if ([_serviceName isEqualToString:@"UnlimitedPlanVC"]) {
        [self changeCellHeights:indexPath andTableView:tableView];

    }else if ([_serviceName isEqualToString:@"FixedPlanVC"]){
        [self changeCellHeights:indexPath andTableView:tableView];

    }else if ([_serviceName isEqualToString:@"AddOnPlanVC"]){
    }else{
        [self changeCellHeights:indexPath andTableView:tableView];

    }

}
- (void)changeCellHeights:(NSIndexPath *)indexPath andTableView:(UITableView *)tableView
{
    if (self.expandedCellIndexPath) {
        [self.faqTableView reloadSections:[NSIndexSet indexSetWithIndex:self.expandedCellIndexPath.section] withRowAnimation:UITableViewRowAnimationNone];
    }
    self.expandedCellIndexPath = indexPath;
    [self.faqTableView reloadSections:[NSIndexSet indexSetWithIndex:self.expandedCellIndexPath.section] withRowAnimation:UITableViewRowAnimationNone];
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    faqTableViewCell * Cell = [tableView dequeueReusableCellWithIdentifier:@"faqTableViewCell"];
    [Cell.answerLabel setContentOffset:CGPointZero animated:NO];

    if (self.expandedCellIndexPath && indexPath.row == self.expandedCellIndexPath.row && indexPath.section == self.expandedCellIndexPath.section) {
    
        [Cell setSelected:true animated:true];
        return 120;
    
    }else{
        [Cell setSelected:false animated:true];
        
        [Cell.icon setImage:[UIImage imageNamed:@"downArrowIcon"]];
        return 40;
        
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
