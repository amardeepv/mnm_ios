//
//  faqVC.h
//  MakeNMake
//
//  Created by Archit Saxena on 26/03/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface faqVC : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *faqTableView;
@property (retain, nonatomic) NSMutableArray *faqQuestionArray;
@property (retain, nonatomic) NSMutableArray *faqAnswerArray;
@property (retain, nonatomic) NSString *serviceName;
@property (nonatomic, retain) NSIndexPath * expandedCellIndexPath;

@end
