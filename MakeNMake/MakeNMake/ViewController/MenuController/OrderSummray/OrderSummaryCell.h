//
//  OrderSummaryCell.h
//  MakeNMake
//
//  Created by Chirag Patel on 3/14/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderSummaryCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *mainServiceNameLbl;

@end
