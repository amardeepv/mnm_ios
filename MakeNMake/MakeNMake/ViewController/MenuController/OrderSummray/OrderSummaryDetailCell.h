//
//  OrderSummaryDetailCell.h
//  MakeNMake
//
//  Created by Chirag Patel on 3/14/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderSummaryDetailCell : UITableViewCell

@property (nonatomic,weak) IBOutlet UILabel *lblText;
@property (weak, nonatomic) IBOutlet UIButton *editButton;
@property (nonatomic,weak) IBOutlet UILabel *lblPrice;
@end
