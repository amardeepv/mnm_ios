//
//  OrdersViewController.m
//  MakeNMake
//
//  Created by Chirag Patel on 2/17/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "HelpViewController.h"
#import "SWRevealViewController.h"

@interface HelpViewController ()
@property (weak, nonatomic) IBOutlet UIButton *btnSlider;

@end

@implementation HelpViewController

- (BOOL)prefersStatusBarHidden {
    return YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    SWRevealViewController *revealController = self.revealViewController;
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    [_btnSlider addTarget:revealController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
