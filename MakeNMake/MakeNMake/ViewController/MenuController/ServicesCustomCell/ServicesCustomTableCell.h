//
//  ServicesCustomTableCell.h
//  MakeNMake
//
//  Created by Chirag Patel on 3/16/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServicesCustomTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *serviceTitle;
@property (weak, nonatomic) IBOutlet UILabel *serviceDesc;
@property (weak, nonatomic) IBOutlet UIView *counterUIView;
@property (weak, nonatomic) IBOutlet UIImageView *checkBoxImageView;
@property (strong, nonatomic) NSString *serviceType, *subServiceName, *serviceId, *servicePrice, *parentServiceName, *childServiceName;

@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIButton *infoButtonTapped;
@property (weak, nonatomic) IBOutlet UILabel *counterLabel;

@property (weak, nonatomic) IBOutlet UIButton *plusButton;
@property (weak, nonatomic) IBOutlet UIButton *minusButton;
-(void)configCell;

@end
