//
//  ServicesCustomTableCell.m
//  MakeNMake
//
//  Created by Chirag Patel on 3/16/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "ServicesCustomTableCell.h"
#define BASE_RATE @"Base Rate(1st hour)"
#define FIXED_RATE @"Fixed"
#import "AppDelegate.h"
#import "Constant.h"

@implementation ServicesCustomTableCell
{
    int count;
    int originalPrice;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
//how to show rate ?
//in which case we need to show checkbox
//register Api wrking ?

-(void)configCell{
    count = 0;
    originalPrice = [_servicePrice intValue];
    _priceLabel.text = [NSString stringWithFormat:@"₹ %@",_servicePrice];
    
    if (![_serviceType isEqualToString:FIXED_RATE] && ![_serviceType isEqualToString:BASE_RATE]) {
        
        self.counterUIView.hidden = false;
        _checkBoxImageView.hidden = true;
        
    }else{
        
        self.counterUIView.hidden = true;
        _checkBoxImageView.hidden = false;
        
    }
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    if (![_serviceType isEqualToString:FIXED_RATE] && ![_serviceType isEqualToString:BASE_RATE]) {
        
    }else{
        
        
        if (selected) {
            [_checkBoxImageView setImage:[UIImage imageNamed:@"checkBoxEnabledIcon"]];
            [APPDELEGATE addServiceInfoWithServiceDict:[self getServiceDataWithPrice:_servicePrice]];
        }else{
            [_checkBoxImageView setImage:[UIImage imageNamed:@"checkBoxIcon"]];
            [APPDELEGATE removeServiceInfoWithServiceDict:[self getServiceDataWithPrice:_servicePrice]];
            
        }

        
    }
        // Configure the view for the selected state
}
-(NSMutableDictionary *)getServiceDataWithPrice:(NSString*)totalPrice{
    NSMutableDictionary  *selectedServiceInfo  = [[NSMutableDictionary alloc]init];
    
    [selectedServiceInfo setValue:_subServiceName forKey:SERVICE_NAME];
    [selectedServiceInfo setValue:totalPrice forKey:SERVICE_PRICE];
    [selectedServiceInfo setValue:_serviceId forKey:@"serviceID"];
    [selectedServiceInfo setValue:_parentServiceName forKey:@"parentServiceName"];
    [selectedServiceInfo setValue:_childServiceName forKey:@"childServiceName"];
    
    return selectedServiceInfo;
}
- (IBAction)plusButton_Tapped:(id)sender {
    count++;
    _counterLabel.text = [NSString stringWithFormat:@"%d",count];
    int finalPrice = count*originalPrice;
    [APPDELEGATE addServiceInfoWithServiceDict:[self getServiceDataWithPrice:[NSString stringWithFormat:@"%d",finalPrice]]];

    [self updatePriceWithPrice:finalPrice];

}
- (IBAction)minusButton_Tapped:(id)sender {
    
    if (count>0) {
        count--;
        //    if (count==0) {
        //        count = 1;
        //        _counterLabel.text = [NSString stringWithFormat:@"1"];
        //    }else{
        _counterLabel.text = [NSString stringWithFormat:@"%d",count];
        //    }
        int finalPrice=0;
        
        if (count==0) {
            finalPrice = originalPrice;
            [APPDELEGATE removeServiceInfoWithServiceDict:[self getServiceDataWithPrice:[NSString stringWithFormat:@"%d",finalPrice]]];

        }else{
            finalPrice = count*originalPrice;
            
        }
        
        //    if (count==1) {
        //    }else{
        //        [APPDELEGATE addServiceInfoWithName:_subServiceName andPrice:[NSString stringWithFormat:@"%d",finalPrice] WithServiceId:_serviceId];
        //
        //    }
        
        [self updatePriceWithPrice:finalPrice];
    }

}

-(void)updatePriceWithPrice:(int)Price{
        _priceLabel.text = [NSString stringWithFormat:@"₹ %d",Price];
}

@end
