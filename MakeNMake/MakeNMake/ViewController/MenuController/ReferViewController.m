//
//  OrdersViewController.m
//  MakeNMake
//
//  Created by Chirag Patel on 2/17/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "ReferViewController.h"
#import "SWRevealViewController.h"

@interface ReferViewController ()
@property (weak, nonatomic) IBOutlet UIButton *btnSlider;

@end

@implementation ReferViewController

- (BOOL)prefersStatusBarHidden {
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
