//
//  OrdersViewController.m
//  MakeNMake
//
//  Created by Chirag Patel on 2/17/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "OrdersViewController.h"
#import "OrderConfirmTableCell.h"
#import "SWRevealViewController.h"
#import "OrderConfirmFooterCell.h"
#import "APIList.h"
#import "AlertView.h"
#import "AFNetworking.h"
#import "Constant.h"
#import "LoadingView.h"
#import "NetworkCheck.h"


@interface OrdersViewController ()
@property (weak, nonatomic) IBOutlet UIButton *btnSlider;

@end

@implementation OrdersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //_orderId.text = _txtOrderId;
    _serviceName.text = _txtServiceName;
    
    NSMutableDictionary *param = [[NSMutableDictionary alloc]init];
    
    [param setValue:[APPDELEGATE getUserID] forKey:@"userId"];
    [param setValue:[NSString stringWithFormat:@"%d",_selectedPaymentMode] forKey:@"paymentMethod"];
    [param setValue:[_TotalAmount stringByReplacingOccurrencesOfString:@"₹ " withString:@""] forKey:@"totalAmount"];
    [param setValue:_serviceTaxAmount forKey:@"serviceTaxAmount"];
    [param setValue:_taxRate forKey:@"taxRate"];
    [param setValue:@"MI" forKey:@"channel"];
    [param setValue:@"0" forKey:@"discount"];
    [param setValue:_uniqueReferId forKey:@"uniqueRefId"];

    [self buyServiceCallMethodWith:param];
    
    //SWRevealViewController *revealController = self.revealViewController;
   // [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    //[_btnSlider addTarget:revealController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}
- (IBAction)backButton_Tapped:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:true];
}

#pragma arguments
#pragma mark -UITableview Delegate & Datasource Method

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    OrderConfirmTableCell * Cell = [tableView dequeueReusableCellWithIdentifier:@"OrderConfirmTableCell"];
    if (!Cell){
        NSArray * arrNib = [[NSBundle mainBundle ]loadNibNamed:@"OrderConfirmTableCell" owner:self options:nil];
        Cell = (OrderConfirmTableCell *)arrNib[0];
    }
    Cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [Cell.serviceName setText:[NSString stringWithFormat:@"%@->%@->%@",[[_selectedServiceArray objectAtIndex:indexPath.row] valueForKey:@"parentServiceName"],[[_selectedServiceArray objectAtIndex:indexPath.row] valueForKey:@"childServiceName"],[[_selectedServiceArray objectAtIndex:indexPath.row] valueForKey:SERVICE_NAME]]];
    
    [Cell.statusLabel setTextColor:[UIColor greenColor]];
    [Cell.statusLabel setText:@"Order Placed"];
    
    return Cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.001f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    OrderConfirmTableCell * Cell = [tableView dequeueReusableCellWithIdentifier:@"OrderConfirmTableCell"];
    if (!Cell){
        NSArray * arrNib = [[NSBundle mainBundle ]loadNibNamed:@"OrderConfirmTableCell" owner:self options:nil];
        Cell = (OrderConfirmTableCell *)arrNib[0];
    }
    [Cell.serviceName setFont:[UIFont boldSystemFontOfSize:15]];
    return Cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _selectedServiceArray.count;
}

#pragma mark - Call Login webservice Method


- (void)buyServiceCallMethodWith:(NSMutableDictionary *)diction {
    [LoadingView showLoaderWithOverlay:YES];
    if([NetworkCheck isInternetOff]) {
        [AlertView showAlert:@"The Internet connection appears to be offline." alertType:AJNotificationTypeRed];
        [LoadingView dismissLoader];
        
    }else {
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSString *strPostUrl;
        if (_isFromPlan) {
        strPostUrl = [NSString stringWithFormat:@"%@BuyPlan",BACKENDBASEURL];

        }else{
      strPostUrl = [NSString stringWithFormat:@"%@BuyService",BACKENDBASEURL];
            
        }
        AFJSONRequestSerializer* serialiser = [AFJSONRequestSerializer serializer];
        //    serialiser.timeoutInterval = 15.0;
        manager.responseSerializer=[AFJSONResponseSerializer serializer];
        manager.requestSerializer = serialiser;
        manager.responseSerializer.acceptableStatusCodes = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(200, 403)];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [manager POST:strPostUrl parameters:diction success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [LoadingView dismissLoader];
            
            NSMutableDictionary *dict = (NSMutableDictionary* )responseObject;
        
            if ([[dict valueForKey:@"message"] isEqualToString:@"Service bought successfully"]) {
               
                if ([[[dict valueForKey:@"data"] valueForKey:@"BuyResult"] count]>0) {
                     _orderId.text = [NSString stringWithFormat:@"Order Id : %@",[[[[dict valueForKey:@"data"] valueForKey:@"BuyResult"] objectAtIndex:0] valueForKey:@"invoiceNumber"]];
                }else{
                     _orderId.text = @"Couldnt fetch order Id";
                }
               
                
                [AlertView showAlert:@"Service bought successfully" hideAfterDelay:3.0f alertType:AJNotificationTypeGreen];
            }else{
                [self.navigationController popViewControllerAnimated:true];
                [AlertView showAlert:@"Some Error occurred" hideAfterDelay:3.0f alertType:AJNotificationTypeRed];
            }
            
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [LoadingView dismissLoader];
            [AlertView showAlert:[error localizedDescription] alertType:AJNotificationTypeRed];
            
        }];
    }
}
- (IBAction)home_Action:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:true];
}

#pragma mark -

@end
