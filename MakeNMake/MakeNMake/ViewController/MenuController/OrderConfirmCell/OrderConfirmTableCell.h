//
//  OrderConfirmTableCell.h
//  MakeNMake
//
//  Created by Chirag Patel on 3/14/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderConfirmTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *serviceName;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;

@end
