//
//  OrdersViewController.h
//  MakeNMake
//
//  Created by Chirag Patel on 2/17/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrdersViewController : UIViewController /**<, UITableViewDataSource>*/
@property (weak, nonatomic) IBOutlet UILabel *orderId;
@property (weak, nonatomic) IBOutlet UILabel *serviceName;


@property (weak, nonatomic) NSString *txtServiceName;
@property (weak, nonatomic) NSString *txtOrderId;
@property (weak, nonatomic) NSString *serviceTaxAmount;
@property (weak, nonatomic) NSString *taxRate;
@property (weak, nonatomic) NSString *TotalAmount;
@property (weak, nonatomic) NSString *uniqueReferId;

@property (strong, nonatomic) NSArray *selectedServiceArray;


@property (assign) int selectedPaymentMode;
@property (assign) BOOL isFromPlan;

@property (weak, nonatomic) IBOutlet UITableView *tblView;
@end
