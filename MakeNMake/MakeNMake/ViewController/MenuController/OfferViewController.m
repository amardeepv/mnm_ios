//
//  OrdersViewController.m
//  MakeNMake
//
//  Created by Chirag Patel on 2/17/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "OfferViewController.h"
#import "SWRevealViewController.h"
#import "OrderSummaryDetailCell.h"
#import "OrderSummaryCell.h"
#import "AppDelegate.h"
#import "Constant.h"
#import "APIList.h"
#import "PaymentVCViewController.h"
#import "AlertView.h"
#import "Utility.h"
#import "AFNetworking.h"
#import "LoadingView.h"
#import "NetworkCheck.h"

@interface OfferViewController () {
    NSMutableArray *arrList;
    NSMutableArray *calculatedPriceArray;
    float grandTotal;
    NSString *serviceTax;
    NSString *taxAmount;
    BOOL isCheckTerms;
    
}
@property (weak, nonatomic) IBOutlet UIButton *btnSlider;

@end

@implementation OfferViewController

- (BOOL)prefersStatusBarHidden {
    return YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self calculateTotalSumAndTax];
    
    if (APPDELEGATE.showCouponField) {
        //hide Coupon Field
        _couponContainerView.hidden = true;
        _tblViewTopConstraint.constant = -_couponContainerView.frame.size.height;
    }
    
    [self.navigationController setNavigationBarHidden:true];
    _tblView.tableFooterView = [UIView new];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma arguments
#pragma mark -UITableview Delegate & Datasource Method

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    OrderSummaryDetailCell * Cell = [_tblView dequeueReusableCellWithIdentifier:@"OrderSummaryDetailCell"];
    if (!Cell){
        NSArray * arrNib = [[NSBundle mainBundle ]loadNibNamed:@"OrderSummaryDetailCell" owner:self options:nil];
        Cell = (OrderSummaryDetailCell *)arrNib[0];
    }
    if (indexPath.section == 0) {
  
        Cell.lblText.text = [NSString stringWithFormat:@"%@ -> %@ -> %@",[[[APPDELEGATE getSelecteServices] objectAtIndex:indexPath.row] valueForKey:@"parentServiceName"],[[[APPDELEGATE getSelecteServices] objectAtIndex:indexPath.row] valueForKey:@"childServiceName"],[[[APPDELEGATE getSelecteServices] objectAtIndex:indexPath.row] valueForKey:SERVICE_NAME]];
        
        
        if ([[[APPDELEGATE getSelecteServices] objectAtIndex:indexPath.row] valueForKey:SERVICE_PRICE]>0) {
            Cell.lblPrice.text = [NSString stringWithFormat:@"₹ %@",[[[APPDELEGATE getSelecteServices] objectAtIndex:indexPath.row] valueForKey:SERVICE_PRICE]];

        }else{
            Cell.lblPrice.text = [NSString stringWithFormat:@"₹ 0"];

        }
        
        
    }
    if (indexPath.section == 1) {
        
        if ([[arrList objectAtIndex:indexPath.row] isEqualToString:@"Grand Total"]) {
            [Cell.lblText setFont:[UIFont boldSystemFontOfSize:15]];
            [Cell.lblPrice setFont:[UIFont boldSystemFontOfSize:15]];
        }
        
        Cell.lblText.text = [arrList objectAtIndex:indexPath.row];
        Cell.lblPrice.text = [calculatedPriceArray objectAtIndex:indexPath.row];
        Cell.editButton.hidden  = true;
    }
    Cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return Cell;
    
}



- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        OrderSummaryCell * Cell = [tableView dequeueReusableCellWithIdentifier:@"OrderSummaryCell"];
        if (!Cell){
            NSArray * arrNib = [[NSBundle mainBundle ]loadNibNamed:@"OrderSummaryCell" owner:self options:nil];
            Cell = (OrderSummaryCell *)arrNib[0];
        }
        Cell.mainServiceNameLbl.text =[[NSString stringWithFormat:@"  Selected Services"/*,_mainServiceTitle */] uppercaseString];
        return Cell;
    }else {
        
        UIView *viewSection = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.tblView.frame.size.width, 5)];
        viewSection.backgroundColor = [UIColor colorWithRed:235.0f/255.0f green:235.0f/255.0f blue:241.0f/255.0f alpha:1];
        return viewSection;
        
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return [[APPDELEGATE getSelecteServices] count];
    }else {
        return [arrList count];
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if(section == 0) {
        return 55;
    }else {
        return 5;
    }

}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}
#pragma mark - Custom Methods

-(void)calculateTotalSumAndTax{
    float totalAmount = 0;
    for (int i=0; i<[[APPDELEGATE getSelecteServices] count]; i++) {
        totalAmount += [[[[APPDELEGATE getSelecteServices] objectAtIndex:i] valueForKey:SERVICE_PRICE] intValue];
        
    }
    
    //CALL WEBSERVICE
    
    [self getTaxDetailsForTotalSum:totalAmount];
}
#pragma mark - Api for tax calculation
-(void)getTaxDetailsForTotalSum:(float)Sum{
        //user/778/address
    NSString *serviceNameWithParam = [NSString stringWithFormat:@"ApplyServiceTax?totalAmount=%.2f",Sum];
        
        [[APIList sharedAPIList] API_GetAllDataFromWS:serviceNameWithParam ShowLoader:YES showOverlay:YES completion:^(BOOL success, id responceData, NSMutableArray *share) {
            if (success) {
                NSDictionary * result =(NSDictionary *) responceData;
                if ([[result objectForKey:@"hasError"] isEqual:@(0)]) {
                    [self GetDictionaryFromResponse:[result valueForKey:@"data"]];
                }else{
                    
                }
            }
        }];
}
-(void)GetDictionaryFromResponse:(NSMutableDictionary *)dict{
    
    serviceTax = [dict valueForKey:@"taxRate"];
    taxAmount = [dict valueForKey:@"taxAmount"];
    
    arrList = [NSMutableArray arrayWithObjects:@"Sub Total",[NSString stringWithFormat:@"Service Tax @%@%%",serviceTax],@"Coupon Discount",@"Grand Total", nil];
    
    grandTotal = [[dict valueForKey:@"billAfterTax"] floatValue];
    
    calculatedPriceArray = [NSMutableArray arrayWithObjects:[NSString stringWithFormat:@"₹ %@",[dict valueForKey:@"totalPayableAmount"]],[NSString stringWithFormat:@"₹ %@",taxAmount],@"₹ 0",[NSString stringWithFormat:@"₹ %@",[dict valueForKey:@"billAfterTax"]], nil];
    
    [self.tblView reloadData];
    
}
#pragma mark - UIButton Actions

-(IBAction)bntCheckBoxPressed:(UIButton *)sender; {
    if (sender.selected) {
        sender.selected = false;
       isCheckTerms= FALSE;
    }else {
        sender.selected = true;
        isCheckTerms= TRUE;
        
    }
}
- (IBAction)backButton_tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}
#pragma mark - get Coupon Code
- (IBAction)couponButton_Tapped:(id)sender {
    
    if (_couponTxtFld.text.length>0) {
        [_couponTxtFld resignFirstResponder];
        
        NSMutableDictionary *param = [[NSMutableDictionary alloc]init];
        NSMutableArray *tempArray = [[NSMutableArray alloc]init];
    
        if ([[Utility sharedInstance] unArchiveFileWithFileName:OA_LOGIN_FILE]) {
            //User Logged in
          
            for (int i=0; i<[[APPDELEGATE getSelecteServices] count]; i++) {
                
                NSMutableDictionary *tempDict = [[NSMutableDictionary alloc]init];
                
                [tempDict setObject:[APPDELEGATE getUserID] forKey:@"userID"];
                [tempDict setObject:[[[APPDELEGATE getSelecteServices] objectAtIndex:i] valueForKey:@"serviceID"] forKey:@"serviceGroupID"];
                [tempDict setObject:[NSNumber numberWithInteger:grandTotal] forKey:@"totalAmount"];
                [tempDict setObject:_couponTxtFld.text forKey:@"couponCode"];
                [tempDict setObject:@"Mi" forKey:@"channel"];
                [tempDict setObject:_selectedAddress forKey:@"addressId"];
                [tempArray addObject:tempDict];

            }
            
            
            [param setObject:tempArray forKey:@"lstCouponRequest"];
            
            [self getCouponCodeApiDiscountWithParam:param];
            
        }else{
            //User not logged in
            [AlertView showAlert:@"Please Login First to apply Coupon" alertType:AJNotificationTypeRed];
        }
    }
    
    }
    -(void)getCouponCodeApiDiscountWithParam:(NSMutableDictionary *)param{
        
        [LoadingView showLoaderWithOverlay:YES];
        if([NetworkCheck isInternetOff]) {
            
            [AlertView showAlert:@"The Internet connection appears to be offline." alertType:AJNotificationTypeRed];
            [LoadingView dismissLoader];
            
        }else {
            
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            NSString *strPostUrl = [NSString stringWithFormat:@"%@ApplyCoupon",BACKENDBASEURL];
            
            AFJSONRequestSerializer* serialiser = [AFJSONRequestSerializer serializer];
            //    serialiser.timeoutInterval = 15.0;
            manager.responseSerializer=[AFJSONResponseSerializer serializer];
            manager.requestSerializer = serialiser;
            manager.responseSerializer.acceptableStatusCodes = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(200, 403)];
            [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            
            [manager POST:strPostUrl parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
                [LoadingView dismissLoader];
                
                NSMutableDictionary *responseDict = (NSMutableDictionary *)responseObject;
                
                if ([responseDict valueForKey:@"data"] && [[[responseDict valueForKey:@"data"] valueForKey:@"lstCouponResult"] count]>0) {
                    
            [calculatedPriceArray insertObject:[NSString stringWithFormat:@"₹ %@",
                                                        [[[[responseDict valueForKey:@"data"] valueForKey:@"lstCouponResult"] firstObject] valueForKey:@"couponDiscount"]] atIndex:2];
                    
                    [calculatedPriceArray removeObjectAtIndex:3];
                    
                    
                    [calculatedPriceArray insertObject:[NSString stringWithFormat:@"₹ %@",
                                                        [[[[responseDict valueForKey:@"data"] valueForKey:@"lstCouponResult"] firstObject] valueForKey:@"discountedAmount"]] atIndex:3];
                    
                    [calculatedPriceArray removeObjectAtIndex:4];
                    
                
                    [AlertView showAlert:@"Coupon Applied Successfully" alertType:AJNotificationTypeGreen];
                    [_tblView reloadData];
                    
                }else{
                    if ([responseDict valueForKey:@"message"]) {
                        [AlertView showAlert:[responseDict valueForKey:@"message"] alertType:AJNotificationTypeRed];
                        
                    }else{
                        [AlertView showAlert:@"Something went Wrong" alertType:AJNotificationTypeRed];
                        
                    }
                    
                }
                
            }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                [LoadingView dismissLoader];
                [AlertView showAlert:[error localizedDescription] alertType:AJNotificationTypeRed];
                
            }];
        }
    }
    
    
- (IBAction)confirmButton_Tapped:(id)sender {
    
    if (isCheckTerms) {
        PaymentVCViewController *Obj = [[PaymentVCViewController alloc]initWithNibName:@"PaymentVCViewController" bundle:nil];
        
        Obj.orderValue = [NSString stringWithFormat:@"%@",[NSNumber numberWithInt:13412]];
        Obj.serviceNametxt = _mainServiceTitle;
        Obj.selectedDate = _selectedDate;
        Obj.selectedTimeSlot = _selectedTimeSlot;
        Obj.selectedAddress = _selectedAddress;
        Obj.totalAmounttxt = [NSString stringWithFormat:@"₹ %0.2f",grandTotal];
        Obj.taxRate = serviceTax;
        Obj.serviceTaxAmount = taxAmount;
        Obj.screenState = ServiceScreen;
        Obj.feedbackString = _feedbackString;
        
        [self.navigationController pushViewController:Obj animated:true];
    }else{
        [AlertView showAlert:@"Please Select terms and Conditions" hideAfterDelay:3.0f alertType:AJNotificationTypeRed];
    }
   
}


- (IBAction)couponTxtFld:(id)sender {
}
@end
