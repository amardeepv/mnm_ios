//
//  OrdersViewController.h
//  MakeNMake
//
//  Created by Chirag Patel on 2/17/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OfferViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>


@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckBox;
@property (strong, nonatomic)  NSString *mainServiceTitle;
@property (weak, nonatomic) IBOutlet UIView *couponContainerView;
@property (strong, nonatomic) NSString *serviceGroupId;

    @property (strong, nonatomic) NSString *feedbackString;

@property (strong, nonatomic) NSString *selectedDate;
@property (strong, nonatomic) NSString *selectedTimeSlot;
@property (strong, nonatomic) NSString *selectedAddress;

@property (weak, nonatomic) IBOutlet UITextField *couponTxtFld;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tblViewTopConstraint;
-(IBAction)bntCheckBoxPressed:(id)sender;

@end
