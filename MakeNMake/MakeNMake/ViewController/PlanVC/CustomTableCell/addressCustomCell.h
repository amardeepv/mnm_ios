//
//  addressCustomCell.h
//  MakeNMake
//
//  Created by Archit Saxena on 12/04/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol addressSelectedProtocol <NSObject>

@optional
-(void)selectedAddress:(NSString*)selectedAddress;
-(void)selectedAddressID:(NSString *)addressId;
@end

@interface addressCustomCell : UITableViewCell<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *addressArray;
    NSString *selectedAddress;
}
@property (weak, nonatomic) IBOutlet UIView *addressView;
@property (weak, nonatomic) IBOutlet UIView *loginOrRegisterView;
@property (weak, nonatomic) IBOutlet UITableView *addressTblView;
@property (weak, nonatomic) IBOutlet UIView *addressTxtFldView;
@property (weak, nonatomic) IBOutlet UITextField *addressTxtField1;
@property (weak, nonatomic) IBOutlet UITextField *addressTxtField2;
@property (weak, nonatomic) IBOutlet UIButton *addNewButton;
@property (weak, nonatomic) IBOutlet UIButton *existingUserButton;
@property (weak, nonatomic) IBOutlet UIButton *PUserButton;

@property (weak, nonatomic) UIViewController *parentView;


@property (strong,nonatomic) id<addressSelectedProtocol>delegate;

-(void)configAddressUI;

@end
