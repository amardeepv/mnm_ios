//
//  addressCustomCell.m
//  MakeNMake
//
//  Created by Archit Saxena on 12/04/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "addressCustomCell.h"
#import "APIList.h"
#import "AlertView.h"
#import "PlanServiceTableVC.h"
#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "showAddressTableCell.h"
#import "Constant.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "LoadingView.h"
#import "Utility.h"
#import "SelectAddressVC.h"

@implementation addressCustomCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self registerNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)configAddressUI{
 
//    _addNewButton.hidden = true;
    
    if ([[Utility sharedInstance] unArchiveFileWithFileName:OA_LOGIN_FILE]) {
        //User Logged in
                _loginOrRegisterView.hidden = true;
                [self getAddressApiCallwithUserId:[APPDELEGATE getUserID]];

        //get Address for Logged in user
        
    }else{
        //User not logged in
                _loginOrRegisterView.hidden = false;
        
    }
}
-(void)registerNib{
    
    
    UINib* nibAddress = [UINib nibWithNibName:@"showAddressTableCell" bundle:nil];
    [_addressTblView registerNib:nibAddress forCellReuseIdentifier:@"showAddressTableCell"];
    
}
#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    
       if (textField.tag==101) {
        [textField resignFirstResponder];

    }else{
        [textField resignFirstResponder];
        [_addressTxtField2 becomeFirstResponder];

    }
    
    return TRUE;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    
    if (textField.tag==101) {
        // _filterCollectionViewTopConstraint.constant = -150;
        //location filter
        
        NSString *tempString = [NSString stringWithFormat:@"%@%@",textField.text,string];
        
        if (tempString.length>3) {
            [self LocationSearchServicesApiCallMethod:[[_addressTxtField2.text stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"," withString:@""]];
        }
        
        
    }
    return YES;
    
}
#pragma mark - Location Services Called Method

- (void)LocationSearchServicesApiCallMethod:(NSString*)strSearchText {
    
    
    NSString *strSearch = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/autocomplete/json?=types=geocode&sensor=false&components=country:IN&key=AIzaSyC2Z2qRrXy_WJLHSyyrfBMTXbYObgwhbys&input=%@",strSearchText];
    
    [[APIList sharedAPIList] API_LocationServiceGetAllDataFromWS:strSearch ShowLoader:NO showOverlay:YES completion:^(BOOL success, id responceData, NSMutableArray *share) {
        if (success) {
            NSDictionary * result =(NSDictionary *) responceData;
            if ([[result objectForKey:@"status"] caseInsensitiveCompare:@"OK"] == NSOrderedSame) {
                //                NSLog(@"WS Response %@",result[@"predictions"]);
                // filteredArray = result[@"predictions"];
                // [self.collectionList reloadData];
                if ([result[@"predictions"] count]>0) {
                    if ([[result[@"predictions"] firstObject] valueForKey:@"description"]) {
                        _addressTxtField2.text = [[result[@"predictions"] firstObject] valueForKey:@"description"];
                        
                        selectedAddress = [NSString stringWithFormat:@"%@%@",_addressTxtField1.text,_addressTxtField2];
                        if ([_delegate respondsToSelector:@selector(selectedAddress:)]) {
                            [_delegate selectedAddress:selectedAddress];
                        }
                        
                        [self calladdAddressApi];
                    }
                    
                }
            }
        }
    }];
}
#pragma mark - UItableiew Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return addressArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    showAddressTableCell *cell = (showAddressTableCell*)[_addressTblView dequeueReusableCellWithIdentifier:@"showAddressTableCell"];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.addresslabel.text = [NSString stringWithFormat:@"%@, %@",[[addressArray objectAtIndex:indexPath.row] valueForKey:@"line1"], [[addressArray objectAtIndex:indexPath.row] valueForKey:@"line2"]];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (![[[addressArray objectAtIndex:indexPath.row] valueForKey:@"title"] isKindOfClass:[NSNull class]]) {
        selectedAddress = [[addressArray objectAtIndex:indexPath.row] valueForKey:@"title"] ;
        
    }else{
        selectedAddress = [[addressArray objectAtIndex:indexPath.row] valueForKey:@"area"] ;
        
    }
    
    if ([_delegate respondsToSelector:@selector(selectedAddress:)]) {
        [_delegate selectedAddress:selectedAddress];
    }
    
    if ([_delegate respondsToSelector:@selector(selectedAddressID:)]) {
        [_delegate selectedAddressID:[[addressArray objectAtIndex:indexPath.row] valueForKey:@"id"]];
    }

}
#pragma mark - API Calling

-(void)calladdAddressApi{
    //param
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    
    [parameters setValue:@"0" forKey:@"id"];
    [parameters setValue:[APPDELEGATE getUserID] forKey:@"userId"];
    [parameters setValue:_addressTxtField2.text forKey:@"title"];
    [parameters setValue:_addressTxtField1.text forKey:@"line1"];
    [parameters setValue:_addressTxtField2.text forKey:@"city"];
    [parameters setValue:_addressTxtField2.text forKey:@"state"];
    [parameters setValue:_addressTxtField2.text forKey:@"area"];
    
    NSString *strUrl = [NSString stringWithFormat:@"user/%@/address",[APPDELEGATE getUserID]];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *strPostUrl = [NSString stringWithFormat:@"%@%@",BACKENDBASEURL,strUrl];
    
    AFJSONRequestSerializer* serialiser = [AFJSONRequestSerializer serializer];
    //    serialiser.timeoutInterval = 15.0;
    manager.responseSerializer=[AFJSONResponseSerializer serializer];
    manager.requestSerializer = serialiser;
    manager.responseSerializer.acceptableStatusCodes = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(200, 403)];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [manager POST:strPostUrl parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
       // [LoadingView dismissLoader];
        
        NSDictionary *dictResponce = (NSDictionary *)responseObject;
        
        if ([[dictResponce valueForKey:@"data"] count]>0) {
            if ([[dictResponce valueForKey:@"message"] caseInsensitiveCompare:@"Address has been added."] == NSOrderedSame) {
                [AlertView showAlert:@"Address added successfully" alertType:AJNotificationTypeGreen];
            }else{
                [AlertView showAlert:@"Unable to add address" alertType:AJNotificationTypeRed];
            }
        }else{
            [AlertView showAlert:[dictResponce valueForKey:@"message"] alertType:AJNotificationTypeRed];
            
        }
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //[LoadingView dismissLoader];
        [AlertView showAlert:[error localizedDescription] alertType:AJNotificationTypeRed];
        
    }];
}
-(void)getAddressApiCallwithUserId:(NSString *)userID{
    //user/778/address
    NSString *serviceNameWithParam = [NSString stringWithFormat:@"user/%@/address",userID];
    
    [[APIList sharedAPIList] API_GetAllDataFromWS:serviceNameWithParam ShowLoader:YES showOverlay:YES completion:^(BOOL success, id responceData, NSMutableArray *share) {
        if (success) {
            NSDictionary * result =(NSDictionary *) responceData;
            if ([[result objectForKey:@"hasError"] isEqual:@(0)]) {
                [self GetAddressArrayFromResponse:[[result valueForKey:@"data"] valueForKey:@"lstAddressResult"]];
            }
        }else{
            [AlertView showAlert:@"Some Error Occured." alertType:AJNotificationTypeGreen];
            
        }
    }];
}
#pragma mark - ParseResponse Method

-(void)GetAddressArrayFromResponse:(NSMutableArray *)arrResponse {
    addressArray = [[NSMutableArray alloc]init];
    
    for (NSDictionary *dic in arrResponse) {
        if (![addressArray containsObject:dic]) {
            [addressArray addObject:dic];
        }
    }
    if (addressArray.count>0) {
//        _addNewButton.hidden = false;
        _addressTxtFldView.hidden = true;
        [_addressTblView reloadData];
        _addressTblView.hidden = false;
    }else{
        _addressTblView.hidden = true;
//        _addNewButton.hidden = true;
        _addressTxtFldView.hidden = false;
        
        
    }
}
- (IBAction)addNewButton_Tapped:(id)sender {

    SelectAddressVC *Obj = [[SelectAddressVC alloc]initWithNibName:@"SelectAddressVC" bundle: nil];
    
    [_parentView presentViewController:Obj animated:true completion:nil];
    
    //    addressArray = [[NSMutableArray alloc]init];
//
//    _addressTxtFldView.hidden = false;
//    _addressTblView.hidden = true;
//    selectedAddress = @"";
//
//    _loginOrRegisterView.hidden = true;
}

@end
