//
//  customPlanTableView.h
//  MakeNMake
//
//  Created by Archit Saxena on 12/04/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CarbonKit/CarbonKit.h>
#import "PlanServiceTableVC.h"
#import "addressCustomCell.h"

@protocol customPlanInfoProtocol <NSObject>

@optional
-(void)getSelectedPlanInfoWithDict:(NSMutableDictionary *)info;

@end

@interface customPlanTableView : UITableViewCell<unlimitedSelectedServiceProtocol,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,CarbonTabSwipeNavigationDelegate,addressSelectedProtocol>{
    
    NSMutableArray *items, *serviceList, *tenureArray;
    CarbonTabSwipeNavigation *carbonTabSwipeNavigation;
    float finalPrice;
    int selectedTenureLoad;
    float selectedTotalAmount , defaultServiceAmount;
    NSString *selectedServiceId;
}
typedef enum ScreenTypes
{
    Unlimited_Pan,
    AddOn_Plan,
    Fixed_Plan
} isFromScreen;

@property (weak, nonatomic) IBOutlet UILabel *selectTenureLabel;

@property (assign) isFromScreen screenState;
@property (weak, nonatomic) IBOutlet UITextField *couponFieldTxtFld;

@property (strong, nonatomic) NSString *selectedAddress;

@property (weak, nonatomic) IBOutlet UILabel *totalPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *couponDiscountLabel;
@property (weak, nonatomic) IBOutlet UILabel *aboveStringlabel;
@property (weak, nonatomic) IBOutlet UILabel *letUsknowLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *couponTopConstraint;

@property (weak, nonatomic) IBOutlet UIView *durationView;
@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;
@property (weak, nonatomic) IBOutlet UIView *targetView;
@property (weak, nonatomic) UIViewController *parentView;

@property (weak, nonatomic) IBOutlet UISlider *sliderBar;
@property (weak, nonatomic) IBOutlet UIButton *couponApplyButton;
@property (strong, nonatomic) NSString *serviceNameWithParam;
@property (strong, nonatomic) id<customPlanInfoProtocol>delegate;
@property (weak, nonatomic) IBOutlet UIButton *sixtyMinButton;

@property (weak, nonatomic) IBOutlet UIButton *allowUsButton;
@property (weak, nonatomic) IBOutlet UICollectionView *tenureCollectionView;
@property (weak, nonatomic) IBOutlet UIButton *ninetyMinButton;
-(void)configureView;

@end
