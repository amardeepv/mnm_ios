//
//  customPlanTableView.m
//  MakeNMake
//
//  Created by Archit Saxena on 12/04/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "customPlanTableView.h"
#import "APIList.h"
#import "AlertView.h"
#import "PlanServiceTableVC.h"
#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "showAddressTableCell.h"
#import "Constant.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "LoadingView.h"
#import "Utility.h"
#import "TenureCollectionCell.h"
#import "NetworkCheck.h"

@implementation customPlanTableView
{
    NSMutableDictionary *planInfoDict;
    BOOL isAvail;
    int selectedMonth;

}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self registerNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
#pragma mark - Custom Methods
-(void)configureView{
  planInfoDict  = [[NSMutableDictionary alloc]init];
    [_couponFieldTxtFld setUserInteractionEnabled:true];
   
  
    
    if (_screenState==Unlimited_Pan) {
        //Unlimited Plan
        _durationView.hidden = true;
        
        _durationView.hidden = true;
        _serviceNameWithParam = @"getUnlimtedPlanDetail";
        [_tenureCollectionView setHidden:false];
        [_letUsknowLabel setText:@"Let Us Know Your Home/Appartment Size ?"];

        selectedTenureLoad = 0;
        [self bringSubviewToFront:_tenureCollectionView];
        
        
    }else if (_screenState==Fixed_Plan){
        //Fixed Plan
        _selectTenureLabel.hidden = true;
        
        _durationView.hidden = false;
        _serviceNameWithParam = @"GetFixedPlanDetail";
        [_letUsknowLabel setText:@"Select Number Of Visits :"];
        [_tenureCollectionView setHidden:true];
        
    }else{
        //Add-On Plan
        _selectTenureLabel.hidden = true;
        _couponTopConstraint.constant = 5;
        [_letUsknowLabel setText:@"Select Number Of Visits :"];
        _durationView.hidden = true;
        _serviceNameWithParam = @"GetAddOnPlanDetail";
        [_tenureCollectionView setHidden:true];

    }
    [self getUnlimitedPlanDetails];
    
}
-(void)registerNib{
    addressCustomCell *cell = [[addressCustomCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"addressCustomCell"];
    
    cell.delegate = self;
    
    UINib* nibProfile = [UINib nibWithNibName:@"TenureCollectionCell" bundle:nil];
    [_tenureCollectionView registerNib:nibProfile forCellWithReuseIdentifier:@"TenureCollectionCell"];
}


#pragma mark - API Call
-(void)getUnlimitedPlanDetails{
 
    [[APIList sharedAPIList] API_GetAllDataFromWS:_serviceNameWithParam ShowLoader:YES showOverlay:YES completion:^(BOOL success, id responceData, NSMutableArray *share) {
        if (success) {
            NSDictionary * result =(NSDictionary *) responceData;
            if ([[result objectForKey:@"hasError"] isEqual:[NSNumber numberWithBool:false]]) {
                [self GetArrayFromResponse:[result valueForKey:@"data"]];
            }
        }else{
            [AlertView showAlert:@"Some Error Occured." alertType:AJNotificationTypeRed];
            
        }
    }];
}
#pragma mark - ParseResponse Method

- (void)GetArrayFromResponse:(NSMutableArray *)arrResponse {
    items = [[NSMutableArray alloc]init];
    serviceList = [[NSMutableArray alloc]init];
    serviceList = arrResponse;
    for (NSDictionary *dict in arrResponse) {
        if ([[dict valueForKey:@"onlyForAdmin"] isEqual:[NSNumber numberWithBool:false]]) {

        if (_screenState==Unlimited_Pan) {
            //Unlimited Plan
           // if (![items containsObject:[dict valueForKey:@"planName"]]) {
                [items addObject:[dict valueForKey:@"planName"]];
            //            }
            
            
        }else if (_screenState==Fixed_Plan){
            //Fixed Plan
            
            int numberOfCalls = 0;
            for (NSDictionary *dictTemp in [dict valueForKey:@"PlanParentServiceList"]) {
                numberOfCalls += [[dictTemp valueForKey:@"NoofCalls"] intValue];
            }
//            if (![items containsObject:[NSString stringWithFormat:@"%d Visits",numberOfCalls]]) {
                [items addObject:[NSString stringWithFormat:@"%d visits",numberOfCalls]];
//            }
            
        }else{
            //Add-On Plan
//            if (![items containsObject:[dict valueForKey:@"planName"]]) {
                [items addObject:[dict valueForKey:@"planName"]];
//            }
            
        }
        
        }
        
    }
    
    if (items.count>0) {
       

        carbonTabSwipeNavigation =
        [[CarbonTabSwipeNavigation alloc] initWithItems:items toolBar:self.toolBar delegate:self];
        [carbonTabSwipeNavigation insertIntoRootViewController:_parentView andTargetView:_targetView];
        //        _titleLabel.text = _titleString;
        [self viewDidLayoutSubviews];
        [self styleForView];
    }
}
-(void)viewDidLayoutSubviews{

    CGFloat totalToolBarWidthForOneItem = _targetView.frame.size.width/items.count;
   
    
    if (_screenState==Unlimited_Pan) {
        //Unlimited Plan
        if (!(items.count>4)) {
            for (int i=0; i<items.count; i++) {
                [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:totalToolBarWidthForOneItem forSegmentAtIndex:i];
            }
        }
    }else if (_screenState==Fixed_Plan){
        //Fixed Plan
        
        if (!(items.count>4)) {
            for (int i=0; i<items.count; i++) {
                [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:totalToolBarWidthForOneItem forSegmentAtIndex:i];
            }
        }
    }else{
        //Add-On Plan
        if (!(items.count>2)) {
            for (int i=0; i<items.count; i++) {
                [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:totalToolBarWidthForOneItem forSegmentAtIndex:i];
            }
        }
        
    }

    carbonTabSwipeNavigation.toolbarHeight.constant = 100;

    [carbonTabSwipeNavigation setTabExtraWidth:1];
}
- (void)styleForView {
    UIColor *color = [UIColor colorWithRed:129.0f/255 green:6.0f/255 blue:127.0f/255 alpha:1];
    _parentView.navigationController.navigationBar.translucent = NO;
    _parentView.navigationController.navigationBar.tintColor = [UIColor colorWithRed:129.0f/255 green:6.0f/255 blue:127.0f/255 alpha:1];
    _parentView.navigationController.navigationBar.barTintColor = color;
    _parentView.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    
    carbonTabSwipeNavigation.toolbar.translucent = NO;
    carbonTabSwipeNavigation.toolbar.backgroundColor = [UIColor whiteColor];
    carbonTabSwipeNavigation.carbonSegmentedControl.backgroundColor = [UIColor whiteColor];
    carbonTabSwipeNavigation.carbonSegmentedControl.indicatorPosition = IndicatorPositionBottom;
    [carbonTabSwipeNavigation setIndicatorColor:[UIColor colorWithRed:129.0f/255 green:6.0f/255 blue:127.0f/255 alpha:1]];//81067F
    [carbonTabSwipeNavigation setTabExtraWidth:10];
    [carbonTabSwipeNavigation setNormalColor:[color colorWithAlphaComponent:0.7] font:[UIFont boldSystemFontOfSize:14]];
    [carbonTabSwipeNavigation setSelectedColor:color font:[UIFont boldSystemFontOfSize:14]];
}

#pragma mark - CarbonTabSwipeNavigation Delegate
- (nonnull UIViewController *)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbontTabSwipeNavigation viewControllerAtIndex:(NSUInteger)index {
    PlanServiceTableVC *Obj = [[PlanServiceTableVC alloc]initWithNibName:@"PlanServiceTableVC" bundle:nil];
    
    if (![[[serviceList objectAtIndex:index] valueForKey:@"PlanParentServiceList"] isKindOfClass:[NSNull class]]) {
      
        Obj.serviceDict = [serviceList objectAtIndex:index];

        defaultServiceAmount = [[[serviceList objectAtIndex:index] valueForKey:@"cost"] intValue];
        
        if (_screenState==Unlimited_Pan) {
            //Unlimited Plan
            Obj.screenName = Unlimited_Table;
            Obj.delegate = self;
            selectedMonth = 1;
        }else if (_screenState==Fixed_Plan){
            //Fixed Plan
            Obj.screenName = Fixed_Table;
            
        }else{
            //Add-On Plan
            Obj.screenName = AddOn_Table;
        }
    }

    return Obj;
    
}
- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                 willMoveAtIndex:(NSUInteger)index{

    if (![[[serviceList objectAtIndex:index] valueForKey:@"PlanParentServiceList"] isKindOfClass:[NSNull class]]) {
       
        if (_screenState==Unlimited_Pan) {
            //Unlimited Plan
            
            [self getFactorArrayWithPlanDurationForPlanId:[[serviceList objectAtIndex:index] valueForKey:@"planID"]];
            
            [planInfoDict setValue:@"Unlimited" forKey:@"noofCalls"];
            
            if ([_delegate respondsToSelector:@selector(getSelectedPlanInfoWithDict:)]) {
                [_delegate getSelectedPlanInfoWithDict:planInfoDict];
            }
            
            
            }else if (_screenState==Fixed_Plan){
                
            //Fixed Plan
            _totalPriceLabel.text = @"₹ 0" ;
            
            [self calculateTotalPriceForSixtyMinuteServiceCost:[[[serviceList objectAtIndex:index] valueForKey:@"cost"] intValue]];
            [_sixtyMinButton setSelected:true];
            [_ninetyMinButton setSelected:false];

                int numberOfCalls = 0;
                for (NSDictionary *dictTemp in [[serviceList objectAtIndex:index] valueForKey:@"PlanParentServiceList"]) {
                    numberOfCalls += [[dictTemp valueForKey:@"NoofCalls"] intValue];
                }
                [planInfoDict setValue:[NSString stringWithFormat:@"%d",numberOfCalls] forKey:@"noofCalls"];

                
            _sixtyMinButton.tag = index;
            _ninetyMinButton.tag = index;
                        
        }else{
            //Add-On Plan
            
            _totalPriceLabel.text = @"₹ 0" ;
            [self calculateTotalPriceForSixtyMinuteServiceCost:[[[serviceList objectAtIndex:index] valueForKey:@"cost"] intValue]];
            
            [planInfoDict setValue:@"" forKey:@"noofCalls"];


        }
        
    [planInfoDict setValue:@"" forKey:@"planDetailId"];
    [planInfoDict setValue:@"" forKey:@"serviceId"];
    [planInfoDict setValue:[[serviceList objectAtIndex:index] valueForKey:@"planID"] forKey:@"planId"];
    [planInfoDict setValue:@"" forKey:@"durationId"];
    [planInfoDict setValue:@"" forKey:@"factor"];
    [planInfoDict setValue:@"" forKey:@"durationRate"];
    [planInfoDict setValue:[[serviceList objectAtIndex:index] valueForKey:@"cost"] forKey:@"planAmount"];
        [planInfoDict setValue:[[serviceList objectAtIndex:index] valueForKey:@"visitRequired"] forKey:@"visitRequired"];
        [planInfoDict setValue:[[serviceList objectAtIndex:index] valueForKey:@"description"] forKey:@"planDesc"];
    [planInfoDict setValue:@"" forKey:@"couponId"];
    [planInfoDict setValue:@"" forKey:@"couponDiscountAmt"];

        if ([_delegate respondsToSelector:@selector(getSelectedPlanInfoWithDict:)]) {
            [_delegate getSelectedPlanInfoWithDict:planInfoDict];
        }
        
    }
    
}

#pragma mark - Price Calculation - Fixed Plan
-(void)calculateTotalPriceForNinetyMinuteServices:(NSMutableArray*)servicesArray andCost:(int)serviceCost{
    float totalCosts = 0;
    for (NSDictionary *dict in servicesArray) {
        
        if (![[dict valueForKey:@"NoofCalls"] isKindOfClass:[NSNull class]] && ![[dict valueForKey:@"additionalrate"] isKindOfClass:[NSNull class]]) {
            totalCosts += [[dict valueForKey:@"additionalrate"] intValue];
        }
    }
    
    [self getTaxDetailsForTotalSum:totalCosts+serviceCost];
}
-(void)calculateTotalPriceForSixtyMinuteServiceCost:(int)serviceCost{
    [self getTaxDetailsForTotalSum:serviceCost];
}

#pragma mark - Api for tax calculation
-(void)getTaxDetailsForTotalSum:(float)Sum{

    if (Sum>0) {
       
    NSString *serviceNameWithParam = [NSString stringWithFormat:@"ApplyServiceTax?totalAmount=%f",Sum];
    
    [[APIList sharedAPIList] API_GetAllDataFromWS:serviceNameWithParam ShowLoader:NO showOverlay:YES completion:^(BOOL success, id responceData, NSMutableArray *share) {
        if (success) {
            NSDictionary * result =(NSDictionary *) responceData;
            if ([[result objectForKey:@"hasError"] isEqual:@(0)]) {
                [self GetDictionaryFromResponse:[result valueForKey:@"data"]];
            }else{
                [AlertView showAlert:@"Some Error Occured." alertType:AJNotificationTypeRed];

            }
        }
    }];
    }else{
        
        float grandTotal = 0.00;
        _totalPriceLabel.text = [NSString stringWithFormat:@"₹ %.2f",grandTotal];
        finalPrice = grandTotal;
        [planInfoDict setValue:[NSString stringWithFormat:@"%.1f",finalPrice] forKey:@"planAmount"];
        
        [planInfoDict setValue:[NSNumber numberWithInt:0] forKey:@"taxRate"];
        [planInfoDict setValue:[NSNumber numberWithInt:0] forKey:@"taxAmount"];
        
        
        if ([_delegate respondsToSelector:@selector(getSelectedPlanInfoWithDict:)]) {
            [_delegate getSelectedPlanInfoWithDict:planInfoDict];
        }

    }
}
-(void)GetDictionaryFromResponse:(NSMutableDictionary *)dict{
    
//   NSString* serviceTax = [dict valueForKey:@"taxRate"];
//   NSString* taxAmount = [dict valueForKey:@"taxAmount"];
    
    float grandTotal = [[dict valueForKey:@"billAfterTax"] floatValue];
    _totalPriceLabel.text = [NSString stringWithFormat:@"₹ %.2f",grandTotal];
    finalPrice = grandTotal;
    [planInfoDict setValue:[NSString stringWithFormat:@"%.1f",finalPrice] forKey:@"planAmount"];

    [planInfoDict setValue:[dict valueForKey:@"taxRate"] forKey:@"taxRate"];
    [planInfoDict setValue:[dict valueForKey:@"taxAmount"] forKey:@"taxAmount"];
    
    
    if ([_delegate respondsToSelector:@selector(getSelectedPlanInfoWithDict:)]) {
        [_delegate getSelectedPlanInfoWithDict:planInfoDict];
    }

    
}
- (IBAction)ninetyMinuteButton_Tapped:(UIButton*)sender {
    
    [sender setSelected:true];
    [_sixtyMinButton setSelected:false];
    [self calculateTotalPriceForNinetyMinuteServices:[[serviceList objectAtIndex:sender.tag] valueForKey:@"PlanParentServiceList"] andCost:[[[serviceList objectAtIndex:sender.tag] valueForKey:@"cost"] intValue]];
}
- (IBAction)sixtyMinuteButton_Tapped:(UIButton*)sender {
    [sender setSelected:true];
    [_ninetyMinButton setSelected:false];

    [self calculateTotalPriceForSixtyMinuteServiceCost:[[[serviceList objectAtIndex:sender.tag] valueForKey:@"cost"] intValue]];
    
    
}
#pragma mark - get Tenure Api Call

-(void)getFactorArrayWithPlanDurationForPlanId:(NSString *)planId{
    
    NSString *serviceNameWithParam = [NSString stringWithFormat:@"getPlanDuration?planid=%@",planId];
    
    [[APIList sharedAPIList] API_GetAllDataFromWS:serviceNameWithParam ShowLoader:NO showOverlay:YES completion:^(BOOL success, id responceData, NSMutableArray *share) {
        if (success) {
            NSDictionary * result =(NSDictionary *) responceData;
            if ([[result objectForKey:@"hasError"] isEqual:@(0)]) {
                [self GetDictionaryForTenureFromResponse:[result valueForKey:@"data"]];
                
            }else{
                [AlertView showAlert:@"Some Error Occured." alertType:AJNotificationTypeRed];

            }
        }
    }];
}

-(void)GetDictionaryForTenureFromResponse:(NSMutableArray *)arrayOfTenure{
    tenureArray = [[NSMutableArray alloc]initWithArray:arrayOfTenure];
    isAvail = true;

    for (NSDictionary *dict in tenureArray) {
        if ([[dict valueForKey:@"durainPeriod"] isEqual:[NSNumber numberWithInt:12]]) {
            isAvail = false;
        }
    }
    [_tenureCollectionView reloadData];
}

#pragma mark - Collection View dataSource

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat widthFrame = self.frame.size.width;
    
    if (tenureArray.count>0) {
        if (isAvail) {
            return CGSizeMake((widthFrame/(tenureArray.count)+2)-25, 50);
        }else{
            return CGSizeMake((widthFrame/(tenureArray.count)+2)-25, 50);
        }
    }else{
                if (isAvail) {
                    return CGSizeMake(widthFrame, 50);
                }else{
                    return CGSizeMake((widthFrame/(tenureArray.count)+2)-25, 50);
                }
    }
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    if (tenureArray.count>0) {
         if (isAvail) {
        return tenureArray.count + 1;
         }else{
             return tenureArray.count ;
         }
    }else{
        if (isAvail) {
            return tenureArray.count + 1;
        }else{
            return tenureArray.count ;
        }
    }
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    TenureCollectionCell *Cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TenureCollectionCell" forIndexPath:indexPath];
    if (!Cell){
        NSArray * arrNib = [[NSBundle mainBundle ]loadNibNamed:@"TenureCollectionCell" owner:self options:nil];
        Cell = (TenureCollectionCell *)arrNib[0];
    }
    selectedMonth = 12;
    if (indexPath.row==0) {
        
        if (isAvail) {
            Cell.dateNumberLabel.text = @"12";
        }else{
            Cell.dateNumberLabel.text = [NSString stringWithFormat:@"%@",[[tenureArray objectAtIndex:indexPath.row] valueForKey:@"durainPeriod"]];
        }
    }else{
        if (isAvail) {

        Cell.dateNumberLabel.text = [NSString stringWithFormat:@"%@",[[tenureArray objectAtIndex:indexPath.row-1] valueForKey:@"durainPeriod"]];
        }else{
            Cell.dateNumberLabel.text = [NSString stringWithFormat:@"%@",[[tenureArray objectAtIndex:indexPath.row] valueForKey:@"durainPeriod"]];
        }
        
      
    }
    
    if (Cell.isSelectedOnce) {
        [Cell setSelected:true];
    }else{
        [Cell setSelected:false];
    }
  

    return Cell;
}

#pragma mark - UICollection Delegate  Method

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.row==0) {
        selectedTenureLoad  = 0;
        selectedMonth = 12;
        
        if (isAvail) {
            selectedTenureLoad  = 0;
        }else{
//            selectedTenureLoad = [[[tenureArray objectAtIndex:indexPath.row] valueForKey:@"factor"] intValue];
        }
    }else{
        if (isAvail) {
            selectedMonth = [[[tenureArray objectAtIndex:indexPath.row-1] valueForKey:@"durainPeriod"] intValue];

            selectedTenureLoad = [[[tenureArray objectAtIndex:indexPath.row-1] valueForKey:@"factor"] intValue];
            [planInfoDict setValue:[[tenureArray objectAtIndex:indexPath.row-1] valueForKey:@"durationId"] forKey:@"durationId"];
            [planInfoDict setValue:[[tenureArray objectAtIndex:indexPath.row-1] valueForKey:@"factor"] forKey:@"factor"];
            [planInfoDict setValue:[[tenureArray objectAtIndex:indexPath.row-1] valueForKey:@"durationRate"] forKey:@"durationRate"];
        }else{
            selectedMonth = [[[tenureArray objectAtIndex:indexPath.row] valueForKey:@"durainPeriod"] intValue];

            selectedTenureLoad = [[[tenureArray objectAtIndex:indexPath.row] valueForKey:@"factor"] intValue];
            [planInfoDict setValue:[[tenureArray objectAtIndex:indexPath.row] valueForKey:@"durationId"] forKey:@"durationId"];
            [planInfoDict setValue:[[tenureArray objectAtIndex:indexPath.row] valueForKey:@"factor"] forKey:@"factor"];
            [planInfoDict setValue:[[tenureArray objectAtIndex:indexPath.row] valueForKey:@"durationRate"] forKey:@"durationRate"];
        }
        
    }
    
    if (selectedMonth!=12) {
        [self getAllSelectedServiceWithTotalAmount:[[[tenureArray objectAtIndex:indexPath.row-1] valueForKey:@"durationRate"] floatValue]];
    }else{
        selectedMonth = 12;
        [self getAllSelectedServiceWithTotalAmount:defaultServiceAmount];
    }
    
    _couponDiscountLabel.text = [NSString stringWithFormat:@"Discount : ₹ 00.00"];
    _couponFieldTxtFld.text = @"";
    
    
}
-(void)getAllSelectedServiceWithTotalAmount:(float)totalSelectedAmount{
    selectedTotalAmount = totalSelectedAmount;
    
    //totalCostOfServicesWithServiceFactor this is the total sum for all the selected services with service Factor
//    float totalCostAfterTenure = totalSelectedAmount +(totalSelectedAmount*selectedTenureLoad/100);
    
    //Calculate tax for this total amount of service (totalCostOfServicesWithServiceFactor)
        [self getTaxDetailsForTotalSum:selectedTotalAmount];
//    _totalPriceLabel.text = [NSString stringWithFormat:@"₹ %.2f",selectedTotalAmount];
    
}
-(void)getSelectedTotalAmountForSelectedServicesWithTotalAmount:(float)totalSelectedAmount{
    selectedTotalAmount = totalSelectedAmount*selectedMonth;
    //totalCostOfServicesWithServiceFactor this is the total sum for all the selected services with service Factor
//    float totalCostAfterTenure = selectedTotalAmount +(selectedTotalAmount*selectedTenureLoad/100);
    
    //Calculate tax for this total amount of service (totalCostOfServicesWithServiceFactor)
    [self getTaxDetailsForTotalSum:selectedTotalAmount];
//    _totalPriceLabel.text = [NSString stringWithFormat:@"₹ %.2f",totalCostAfterTenure];
    
    

}
#pragma mark - Address Delegate
-(void)selectedAddressID:(NSString *)addressId{
    _selectedAddress = addressId;
}

#pragma mark - get Coupon Code

- (IBAction)couponButtonApply_Tapped:(id)sender {
  
    if (_couponFieldTxtFld.text.length>0) {

            [_couponFieldTxtFld resignFirstResponder];
            
            NSMutableDictionary *param = [[NSMutableDictionary alloc]init];
            NSMutableArray *tempArray = [[NSMutableArray alloc]init];
            NSMutableDictionary *tempDict = [[NSMutableDictionary alloc]init];
            
            if ([[Utility sharedInstance] unArchiveFileWithFileName:OA_LOGIN_FILE]) {
                //User Logged in
                if ((_selectedAddress.length>0)) {
   
                [tempDict setObject:[APPDELEGATE getUserID] forKey:@"userID"];
                [tempDict setObject:selectedServiceId forKey:@"serviceGroupID"];
                [tempDict setObject:[NSString stringWithFormat:@"%@",[NSString stringWithString: [_totalPriceLabel.text stringByReplacingOccurrencesOfString:@"₹ " withString:@""]]] forKey:@"totalAmount"];
                [tempDict setObject:_couponFieldTxtFld.text forKey:@"couponCode"];
                [tempDict setObject:@"Mi" forKey:@"channel"];
                [tempDict setObject:_selectedAddress forKey:@"addressId"];
                
                [tempArray addObject:tempDict];
                [param setObject:tempArray forKey:@"lstCouponRequest"];
                
                [self getCouponCodeApiDiscountWithParam:param];
               
                }else{
        [AlertView showAlert:@"Please provide us your address" alertType:AJNotificationTypeRed];

                }
            }else{
                //User not logged in
                [AlertView showAlert:@"Please Login First to apply Coupon" alertType:AJNotificationTypeRed];
            }
    
        }
   }
-(void)getCouponCodeApiDiscountWithParam:(NSMutableDictionary *)param{
    
    [LoadingView showLoaderWithOverlay:YES];
    if([NetworkCheck isInternetOff]) {
        
        [AlertView showAlert:@"The Internet connection appears to be offline." alertType:AJNotificationTypeRed];
        [LoadingView dismissLoader];
        
    }else {
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSString *strPostUrl = [NSString stringWithFormat:@"%@ApplyCouponForPlan",BACKENDBASEURL];
        
        AFJSONRequestSerializer* serialiser = [AFJSONRequestSerializer serializer];
        //    serialiser.timeoutInterval = 15.0;
        manager.responseSerializer=[AFJSONResponseSerializer serializer];
        manager.requestSerializer = serialiser;
        manager.responseSerializer.acceptableStatusCodes = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(200, 403)];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [manager POST:strPostUrl parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [LoadingView dismissLoader];
            
            NSMutableDictionary *responseDict = (NSMutableDictionary *)responseObject;
            
            if ([responseDict valueForKey:@"data"] && [[[responseDict valueForKey:@"data"] valueForKey:@"lstCouponResult"] count]>0) {
                
                _couponDiscountLabel.text = [NSString stringWithFormat:@"Discount : ₹ %@",
                                             [[[[responseDict valueForKey:@"data"] valueForKey:@"lstCouponResult"] firstObject] valueForKey:@"couponDiscount"]];
                
                _totalPriceLabel.text = [NSString stringWithFormat:@"₹ %@",
                                         [[[[responseDict valueForKey:@"data"] valueForKey:@"lstCouponResult"] firstObject] valueForKey:@"discountedAmount"]];
                
                
                [planInfoDict setValue:[[[[responseDict valueForKey:@"data"] valueForKey:@"lstCouponResult"] firstObject] valueForKey:@"couponId"] forKey:@"couponId"];
                [planInfoDict setValue:[[[[responseDict valueForKey:@"data"] valueForKey:@"lstCouponResult"] firstObject] valueForKey:@"couponDiscount"] forKey:@"couponDiscountAmt"];
                
                if ([_delegate respondsToSelector:@selector(getSelectedPlanInfoWithDict:)]) {
                    [_delegate getSelectedPlanInfoWithDict:planInfoDict];
                }
                [_couponFieldTxtFld setUserInteractionEnabled:false];
                [AlertView showAlert:@"Coupon Applied Successfully" alertType:AJNotificationTypeGreen];

                
            }else{
                if ([responseDict valueForKey:@"message"]) {
                    [AlertView showAlert:[responseDict valueForKey:@"message"] alertType:AJNotificationTypeRed];

                }else{
                    [AlertView showAlert:@"Something went Wrong" alertType:AJNotificationTypeRed];

                }
                
            }
            
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [LoadingView dismissLoader];
            [AlertView showAlert:[error localizedDescription] alertType:AJNotificationTypeRed];
            
        }];
    }
}

@end
