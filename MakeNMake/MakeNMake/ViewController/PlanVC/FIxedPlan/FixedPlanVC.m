//
//  FixedPlanVC.m
//  MakeNMake
//
//  Created by Archit Saxena on 09/04/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "FixedPlanVC.h"
#import "APIList.h"
#import "AlertView.h"
#import "PlanServiceTableVC.h"
#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "showAddressTableCell.h"
#import "Constant.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "LoadingView.h"
#import "Utility.h"
#import "addressCustomCell.h"
#import "customPlanTableView.h"
#import "LoginSegment.h"

@interface FixedPlanVC ()
{
    NSMutableDictionary *planInfoParam;
}
@end

@implementation FixedPlanVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self registerNib];
    [self configUi];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [_tableViewUnlimited reloadData];
}

-(void)registerNib{
    
    UINib *nib1 = [UINib nibWithNibName:@"customPlanTableView" bundle:nil];
    UINib *nib2 = [UINib nibWithNibName:@"addressCustomCell" bundle:nil];
    
    [_tableViewUnlimited registerNib:nib1 forCellReuseIdentifier:@"customPlanTableView"];
    [_tableViewUnlimited registerNib:nib2 forCellReuseIdentifier:@"addressCustomCell"];
    
}
-(void)configUi{
    [_badgeLabel.layer setCornerRadius:_badgeLabel.frame.size.width/2];
    [_badgeLabel.layer setMasksToBounds:true];
    
}

#pragma mark - UitableView Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.row) {
        case 0:{
            customPlanTableView *cell = (customPlanTableView*)[_tableViewUnlimited dequeueReusableCellWithIdentifier:@"customPlanTableView"];
            cell.parentView = self;
            cell.delegate = self;
            cell.screenState = Fixed_Plan;
            [cell configureView];
            [cell.allowUsButton addTarget:self action:@selector(allowUsButton_Tapped:) forControlEvents:UIControlEventTouchUpInside];
            
            return cell;
            break;
        }
        default:
        {
            addressCustomCell *cell = (addressCustomCell*)[_tableViewUnlimited dequeueReusableCellWithIdentifier:@"addressCustomCell"];
            [cell configAddressUI];
            cell.delegate = self;
            cell.parentView = self;

            [cell.existingUserButton addTarget:self action:@selector(existingUserButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
            [cell.PUserButton addTarget:self action:@selector(newUserButton_Tapped:) forControlEvents:UIControlEventTouchUpInside];
            
            return cell;
            break;
        }
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:{
            return 350;
        }
        default:
        {
            return 200;
        }
    }
}


#pragma mark - UIButton Plan Actions

- (void)allowUsButton_Tapped:(id)sender {
    [AlertView showAlert:@"Work in Progress" alertType:AJNotificationTypeBlue];

}

- (IBAction)checkoutButton_Tapped:(id)sender {
    
    if ([[Utility sharedInstance] unArchiveFileWithFileName:OA_LOGIN_FILE]) {
        //User Logged in
        
        if (selectedAddressForPlan!=nil) {
            if (isCheckTerms) {
                PaymentVCViewController *Obj = [[PaymentVCViewController alloc]initWithNibName:@"PaymentVCViewController" bundle:nil];
                
                Obj.taxRate = [planInfoParam valueForKey:@"taxRate"];
                Obj.serviceTaxAmount = [planInfoParam valueForKey:@"taxAmount"];
                
                [planInfoParam removeObjectForKey:@"taxRate"];
                [planInfoParam removeObjectForKey:@"taxAmount"];
                
                [planInfoParam setValue:[APPDELEGATE getUserID] forKey:@"userId"];
                [planInfoParam setValue:[NSString stringWithFormat:@"%@",[NSDate date]] forKey:@"serviceDate"];
                [planInfoParam setValue:[NSString stringWithFormat:@"%@",[NSDate date]] forKey:@"created"];
                [planInfoParam setValue:selectedAddressForPlan forKey:@"addressId"];
                
                Obj.dictForPlanService = planInfoParam;
                
                Obj.serviceNametxt = [planInfoParam valueForKey:@"planDesc"];
                Obj.totalAmounttxt = [NSString stringWithFormat:@"₹ %@",[planInfoParam valueForKey:@"planAmount"]];
                
                
                
                
                [self.navigationController pushViewController:Obj animated:true];
            }else{
                [AlertView showAlert:@"Please Select terms and Conditions" hideAfterDelay:3.0f alertType:AJNotificationTypeRed];
            }
        }else{
            [AlertView showAlert:@"Please provide your address" hideAfterDelay:3.0f alertType:AJNotificationTypeRed];
            
        }
        
        
    }else{
        //User not logged in
        [AlertView showAlert:@"Please Login first to proceed" alertType:AJNotificationTypeRed];
    }
    
    
    
    
}
#pragma mark - Button Address Actions

- (void)existingUserButtonTapped:(id)sender {
    //redirect to login Screen
    LoginSegment  *loginObj = [[LoginSegment alloc]initWithNibName:@"LoginSegment" bundle:nil];
    loginObj.loginState = loginScreen;
    [self.navigationController pushViewController:loginObj animated:YES];

}

- (void)newUserButton_Tapped:(id)sender {
    //redirect to Register Screen
    LoginSegment  *loginObj = [[LoginSegment alloc]initWithNibName:@"LoginSegment" bundle:nil];
    loginObj.loginState = RegisterScreen;
    [self.navigationController pushViewController:loginObj animated:YES];

    
}
- (void)addPNewButtonTapped:(UIButton *)sender {
    //    addressArray = [[NSMutableArray alloc]init];
    //    selectedAddress = @"";
    sender.hidden = true;
    //    _addressTblView.hidden = true;
    //    _loginOrRegisterView.hidden = true;
    //    _addressView.hidden = false;
    
}
- (IBAction)checkBox_Tapped:(UIButton*)sender {
    if (sender.selected) {
        sender.selected = false;
        isCheckTerms= FALSE;
    }else {
        sender.selected = true;
        isCheckTerms= TRUE;
        
    }
}
#pragma mark - AddressInfo Delegate
-(void)selectedAddress:(NSString*)selectedAddress{
    selectedAddressForPlan = selectedAddress;
}
#pragma mark - PlanInfo Delegate
-(void)getSelectedPlanInfoWithDict:(NSMutableDictionary *)info{
    planInfoParam = info;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
