//
//  PlanServiceCustomTableCell.h
//  MakeNMake
//
//  Created by Archit Saxena on 09/04/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomButton.h"

@interface PlanServiceCustomTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *serviceName;
@property (weak, nonatomic) IBOutlet UILabel *Visits;
@property (weak, nonatomic) IBOutlet CustomButton *checkboxButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *checkBoxButtonWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *serviceNameLabelLeadingConstraint;
@property(strong, nonatomic) NSMutableDictionary *serviceNameDictInfo;


-(void)configureCellWithDict:(NSMutableDictionary *)info;

@end
