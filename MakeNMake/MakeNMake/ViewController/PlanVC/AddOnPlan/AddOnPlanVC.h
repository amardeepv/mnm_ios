//
//  AddOnPlanVC.h
//  MakeNMake
//
//  Created by Archit Saxena on 09/04/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "customPlanTableView.h"
#import "addressCustomCell.h"
#import "PaymentVCViewController.h"

@interface AddOnPlanVC : UIViewController<UITableViewDataSource,UITableViewDelegate,customPlanInfoProtocol,addressSelectedProtocol>{
    BOOL isCheckTerms;
    NSString * selectedAddressForPlan;


}
@property (weak, nonatomic) IBOutlet UITableView *tableViewUnlimited;
@property (weak, nonatomic) IBOutlet UILabel *badgeLabel;


@end
