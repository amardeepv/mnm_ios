//
//  PlanServiceTableVC.m
//  MakeNMake
//
//  Created by Archit Saxena on 09/04/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "PlanServiceTableVC.h"
#import "PlanServiceCustomTableCell.h"
#import "APIList.h"
#import "AlertView.h"



@interface PlanServiceTableVC ()    
{
    BOOL isCustomize;
    NSMutableArray *selectedUnlimitedServicesArray , *tempServiceArray;

}
@end

@implementation PlanServiceTableVC

- (void)viewDidLoad {
    [super viewDidLoad];
//    isCustomize = true;
    // Do any additional setup after loading the view from its nib.
    UINib *nib = [UINib nibWithNibName:@"PlanServiceCustomTableCell" bundle:nil];
    [_serviceNameTable registerNib:nib forCellReuseIdentifier:@"PlanServiceCustomTableCell"];
    selectedUnlimitedServicesArray = [[NSMutableArray alloc]init];
    [_serviceNameTable sizeToFit];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (_screenName==Unlimited_Table) {
        selectedUnlimitedServicesArray = [[NSMutableArray alloc]initWithArray:[_serviceDict valueForKey:@"PlanParentServiceList"]];
        
        
        for (NSDictionary *dict in [_serviceDict valueForKey:@"PlanParentServiceList"]) {
            
            if ([[dict valueForKey:@"NoofCalls"] length] == 1) {
                [selectedUnlimitedServicesArray removeObject:dict];
            }
            
        }
        tempServiceArray = [[NSMutableArray alloc] initWithArray: selectedUnlimitedServicesArray];
        [self calculateTotalPriceForAllServicesWithTotalCost:[[_serviceDict valueForKey:@"cost"] intValue]];
    }

    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - UITableView DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if ([[_serviceDict valueForKey:@"PlanParentServiceList"] count]>0) {
        return  [[_serviceDict valueForKey:@"PlanParentServiceList"] count]+1;
    }else{
        return 1;
    }
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    PlanServiceCustomTableCell *cell = [_serviceNameTable dequeueReusableCellWithIdentifier:@"PlanServiceCustomTableCell"];
    [cell prepareForReuse];
    
    if (indexPath.row==0) {
        cell.serviceName.text = @"Services";
        cell.Visits.text = @"Visits";
        [cell.serviceName setFont:[UIFont boldSystemFontOfSize:15]];
        [cell.Visits setFont:[UIFont boldSystemFontOfSize:15]];
        [cell.checkboxButton setHidden:true];
        
    }else{
        if ([[_serviceDict valueForKey:@"PlanParentServiceList"] count]>0) {
            if (_screenName==Unlimited_Table) {

            if ([[[[_serviceDict valueForKey:@"PlanParentServiceList"] objectAtIndex:indexPath.row-1] valueForKey:@"NoofCalls"] length] == 1) {
                [cell setUserInteractionEnabled:false];
            }else{
                [cell setUserInteractionEnabled:true];
            }
            }
            [cell configureCellWithDict:[[_serviceDict valueForKey:@"PlanParentServiceList"] objectAtIndex:indexPath.row-1]];

        }
    }
    [cell.checkboxButton addTarget:self action:@selector(checkBox_Tapped:) forControlEvents:UIControlEventTouchUpInside];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    if (cell.checkboxButton.tag==-1) {
        cell.checkboxButton.selected = false;
    }else{
        cell.checkboxButton.selected = true;
    }
    
    [cell.checkboxButton setTagIndex:(int)indexPath.row];
    
    if (isCustomize) {
        cell.checkBoxButtonWidthConstraint.constant = 25;
        cell.serviceNameLabelLeadingConstraint.constant = 27;
        
    }else{
        cell.checkBoxButtonWidthConstraint.constant = 0;
        cell.serviceNameLabelLeadingConstraint.constant = 1;
    }
    
    
    return cell;
    
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    if (_screenName==Unlimited_Table) {
        //Unlimited Plan
        return _tableFooterView;

    }else if (_screenName==Fixed_Table){
        //Fixed Plan
        return [UIView new];

    }else{
        //Add-On Plan
        return [UIView new];

    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 30.0f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 25.0f;
}

-(void)checkBox_Tapped:(CustomButton *)sender{
  
    if (sender.selected) {
       
        if (selectedUnlimitedServicesArray.count>0) {
        
        sender.tag = -1;
        sender.selected = false;
        
        [selectedUnlimitedServicesArray removeObject:[[_serviceDict valueForKey:@"PlanParentServiceList"] objectAtIndex:sender.tagIndex-1]];
            
            for (NSDictionary *dict in [_serviceDict valueForKey:@"PlanParentServiceList"]) {
                
                if ([[dict valueForKey:@"NoofCalls"] length] == 1) {
                    [selectedUnlimitedServicesArray removeObject:dict];
                }
                
            }

            
        [self calculateTotalPriceForUnlimitedPlanWithServices:selectedUnlimitedServicesArray andCost:[[_serviceDict valueForKey:@"cost"] intValue]];
            
            }
        
    }else {
        sender.selected = true;
        sender.tag = 0;
        
        if (![selectedUnlimitedServicesArray containsObject:[[_serviceDict valueForKey:@"PlanParentServiceList"] objectAtIndex:sender.tagIndex-1]]) {
            [selectedUnlimitedServicesArray addObject:[[_serviceDict valueForKey:@"PlanParentServiceList"] objectAtIndex:sender.tagIndex-1]];
        }
        
        for (NSDictionary *dict in [_serviceDict valueForKey:@"PlanParentServiceList"]) {
            
            if ([[dict valueForKey:@"NoofCalls"] length] == 1) {
                [selectedUnlimitedServicesArray removeObject:dict];
            }
            
        }

        [self calculateTotalPriceForUnlimitedPlanWithServices:selectedUnlimitedServicesArray andCost:[[_serviceDict valueForKey:@"cost"] intValue]];

      
    }
  
        [_serviceNameTable reloadData];
        [_serviceNameTable sizeToFit];
    
}
- (IBAction)customizeButton_Tapped:(UIButton *)sender {

    if (sender.selected) {
        sender.selected = false;
        isCustomize = false;
    }else {
        sender.selected = true;

        isCustomize = true;
    }
    [_serviceNameTable reloadData];
    [_serviceNameTable sizeToFit];

}


#pragma mark - Price Calculation - Unlimited Plan
-(void)calculateTotalPriceForUnlimitedPlanWithServices:(NSMutableArray*)servicesArray andCost:(int)serviceCost{
    
    if ([servicesArray count]!=[tempServiceArray count]) {
        float totalCostOfServicesWithServiceFactor = 0;
        
        float perServiceCost = (serviceCost/[tempServiceArray count]);

        for (NSDictionary *dict in servicesArray) {
            
            int factor = [[dict valueForKey:@"factor"] intValue];
            float serviceCostWithFactor = perServiceCost + (perServiceCost*factor/100);
            totalCostOfServicesWithServiceFactor += serviceCostWithFactor;
            
        }
        totalCostOfServicesWithServiceFactor = totalCostOfServicesWithServiceFactor/12;
        
        if ([_delegate respondsToSelector:@selector(getSelectedTotalAmountForSelectedServicesWithTotalAmount:)]) {
            [_delegate getSelectedTotalAmountForSelectedServicesWithTotalAmount:totalCostOfServicesWithServiceFactor];
        }

    }else{
        [self calculateTotalPriceForAllServicesWithTotalCost:serviceCost];
    }
  
    
}

-(void)calculateTotalPriceForAllServicesWithTotalCost:(int)serviceCost{
    
    if ([_delegate respondsToSelector:@selector(getAllSelectedServiceWithTotalAmount:)]) {
        [_delegate getAllSelectedServiceWithTotalAmount:serviceCost];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
