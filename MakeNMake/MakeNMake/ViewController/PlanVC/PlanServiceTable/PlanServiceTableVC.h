//
//  PlanServiceTableVC.h
//  MakeNMake
//
//  Created by Archit Saxena on 09/04/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol unlimitedSelectedServiceProtocol <NSObject>

@optional
-(void)getSelectedTotalAmountForSelectedServicesWithTotalAmount:(float)totalSelectedAmount;
-(void)getAllSelectedServiceWithTotalAmount:(float)totalSelectedAmount;

@end


@interface PlanServiceTableVC : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *serviceNameTable;
@property (weak, nonatomic) IBOutlet UIButton *customizeButton;

@property (strong, nonatomic) IBOutlet UIView *tableFooterView;
@property (strong,nonatomic) NSMutableDictionary *serviceDict;

@property (assign) id<unlimitedSelectedServiceProtocol> delegate;

typedef enum ScreenFrom
{
    Unlimited_Table,
    AddOn_Table,
    Fixed_Table
} isScreen;

@property (assign) isScreen screenName;

@end
