//
//  WelcomePageMenuVC.h
//  MakeNMake
//
//  Created by Archit Saxena on 25/03/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"
#import "AppDelegate.h"

@interface WelcomePageMenuVC : UIViewController<UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *pageScrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property(nonatomic,strong) NSMutableArray *pageMenuArray,*pageDescArray;

@end
