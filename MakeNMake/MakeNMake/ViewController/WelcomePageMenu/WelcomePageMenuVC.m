//
//  WelcomePageMenuVC.m
//  MakeNMake
//
//  Created by Archit Saxena on 25/03/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "WelcomePageMenuVC.h"
#import "HomeViewController.h"

#define welcomeDesc0 @"Our Security verified, skilled handymen are ready to serve you with any services you may require; be it electrical, plumbing, home appliances repair, car cleaning or anything else"
#define welcomeDesc1 @"Enjoy the ease of payment Go cashless with our MnM wallet or pay with your Debit or Credit card. If not, you can always pay in cash."
#define welcomeDesc2 @"Share us with your loved ones and show that you care \n While we share our benefits (earn...) with you to show that we care"
#define welcomeDesc3 @"MakenMake will make it right for you; avail home care services without hassle."

@interface WelcomePageMenuVC ()

@end

@implementation WelcomePageMenuVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
     [self configView];

}
- (BOOL)prefersStatusBarHidden {
    return YES;
}

#pragma mark - Custom Methods
-(void)configView{
    [self.navigationController setNavigationBarHidden:true];
    [self setArrayForWelcome];
    _descLabel.text = [_pageDescArray objectAtIndex:0];
}
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [self setupScrollViewForViewController];
    [self setAutomaticallyAdjustsScrollViewInsets:NO];

}
-(void)setArrayForWelcome{
    _pageMenuArray = [[NSMutableArray alloc]initWithObjects:@"Welcome0",@"Welcome1",@"Welcome2",@"Welcome3", nil];
    _pageDescArray = [[NSMutableArray alloc]initWithObjects:welcomeDesc0,welcomeDesc1,welcomeDesc2,welcomeDesc3, nil];
    
    _pageScrollView.delegate = self;
    [_pageControl setNumberOfPages:_pageMenuArray.count?_pageMenuArray.count:2];
    
}
#pragma mark -Scroll View Delegates
- (void)setupScrollViewForViewController{
    for (int i=0; i<_pageMenuArray.count; i++) {
        UIImage *image = [UIImage imageNamed:[_pageMenuArray objectAtIndex:i]];
        [self.view needsUpdateConstraints];
        UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake(i*_pageScrollView.frame.size.width, 0, _pageScrollView.frame.size.width, _pageScrollView.frame.size.height)];
        //        if([UIScreen mainScreen].bounds.size.height == [IPHONE_4S_HEIGHT floatValue]){
        //            imgV.contentMode=UIViewContentModeScaleAspectFill;
        //        }
        //        else{
        imgV.contentMode=UIViewContentModeScaleToFill;
        //        }
        [imgV setImage:image];
        [_pageScrollView addSubview:imgV];
    }
    [_pageScrollView setContentSize:CGSizeMake(_pageScrollView.frame.size.width*_pageControl.numberOfPages, _pageScrollView.frame.size.height)];
}
- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    //     Update the page when more than 50% of the previous/next page is visible
    //    CGFloat pageWidth = uiPageControl.frame.size.width;
    CGFloat pageWidth = [UIScreen mainScreen].bounds.size.width;
    int page = floor((_pageScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    
    switch (page) {
        case 1:
            _descLabel.text = [_pageDescArray objectAtIndex:1];
            break;
        case 2:
            _descLabel.text = [_pageDescArray objectAtIndex:2];
            break;
        case 3:
            _descLabel.text = [_pageDescArray objectAtIndex:3];
            break;
        default:
            _descLabel.text = [_pageDescArray objectAtIndex:0];

            break;
    }
    _pageControl.currentPage = page;
}
-(void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    
        if (targetContentOffset->y == 0 /*&& scrollView.contentOffset.y < - someOffset*/) { //use someOffset if you dont want to reload immedeately on scroll to top
    //        //dragged past top
            
            
        }
    //
    if (targetContentOffset->x == scrollView.contentSize.width-[[UIScreen mainScreen] bounds].size.width /*&&isthirdImageVisited == YES*/) {
        //        OALoginVC* viewController = [[OALoginVC alloc] initWithNibName:@"OALoginVC" bundle:nil];
        //        [self.navigationController pushViewController:viewController animated:YES];
    }
    if (targetContentOffset->x == scrollView.contentSize.width-[[UIScreen mainScreen] bounds].size.width) {
        //        isthirdImageVisited = YES;
    }
    if (targetContentOffset->x < scrollView.contentSize.width-[[UIScreen mainScreen] bounds].size.width /* && isthirdImageVisited == YES*/) {
        //        isthirdImageVisited = NO;
    }
    
}


#pragma mark - Action Methods
- (IBAction)gettingStartedButton_Action:(id)sender {
    [APPDELEGATE setupSlideBar];


}

- (IBAction)didTapPageControl:(id)sender {
    CGRect frame = CGRectMake(_pageScrollView.frame.size.width*[_pageControl currentPage], 0, _pageScrollView.frame.size.width, _pageScrollView.frame.size.height);
    [_pageScrollView scrollRectToVisible:frame animated:YES];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
