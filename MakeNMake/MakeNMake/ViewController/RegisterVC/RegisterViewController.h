//
//  RegisterViewController.h
//  MakeNMake
//
//  Created by Chirag Patel on 2/8/17.
//  Copyright © 2017 Nripendra  singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GenericTextField.h"
#import "OTPController.h"


@interface RegisterViewController : UIViewController<UITableViewDelegate>

@property (weak, nonatomic) IBOutlet GenericTextField *txtUserName;
@property (weak, nonatomic) IBOutlet GenericTextField *txtMobileNo;
@property (weak, nonatomic) IBOutlet GenericTextField *txtEmail;
@property (weak, nonatomic) IBOutlet GenericTextField *txtPassword;
@property (weak, nonatomic) IBOutlet GenericTextField *txtReferalCode;

@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
- (IBAction)btnBackPressed:(id)sender;
- (IBAction)btnSubmitPressed:(id)sender;
@end
