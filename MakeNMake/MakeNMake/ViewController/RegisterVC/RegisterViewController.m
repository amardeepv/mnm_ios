//
//  RegisterViewController.m
//  MakeNMake
//
//  Created by Chirag Patel on 2/8/17.
//  Copyright © 2017 Nripendra  singh. All rights reserved.
//

#import "RegisterViewController.h"
#import "AFHTTPRequestOperationManager.h"
#import "Reachability.h"
#import "NetworkCheck.h"
#import "MBProgressHUD.h"
#import "Constant.h"
#import "MZFormSheetContentSizingNavigationController.h"
#import "MZFormSheetPresentationViewController.h"
#import "CustomTransition.h"
#import "OTPController.h"
#import "MZFormSheetPresentationViewControllerSegue.h"
#import "AlertView.h"
#import "LoadingView.h"
#import "AppDelegate.h"
#import "NetworkCheck.h"
#import "Utility.h"

#define ACCEPTABLE_CHARACTERS_NAMING_FIELDS @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 "

@interface RegisterViewController () {
    MBProgressHUD *hud;
    NSString *strAccessToken;
    NSString *strUserId;
    NSString *strUserName;
    AppDelegate *appdel;
    NSMutableDictionary *diction, *otpInfo;
    int responseCount;
    
}

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    responseCount = 1;
    appdel = (AppDelegate *)[UIApplication sharedApplication].delegate ;
    NSInteger intCOunt = [userDefaults integerForKey:@"SocialID"];
    NSLog(@"intCOunt %ld",(long)intCOunt);
    
    [APPDELEGATE setIsOtpVerified:false];


}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if ([APPDELEGATE isOtpVerified]) {
        [self.navigationController popViewControllerAnimated:true];
    }
}

- (UINavigationController *)formSheetControllerWithNavigationController {
    return [self.storyboard instantiateViewControllerWithIdentifier:@"formSheetController"];
}


- (void)centerVerticallyAction {
    UINavigationController *navigationController = [self formSheetControllerWithNavigationController];
    MZFormSheetPresentationViewController *formSheetController = [[MZFormSheetPresentationViewController alloc] initWithContentViewController:navigationController];
    formSheetController.presentationController.shouldCenterVertically = YES;
    
    __weak typeof(formSheetController) weakFormSheet = formSheetController;
    formSheetController.presentationController.frameConfigurationHandler = ^(UIView * __nonnull presentedView, CGRect currentFrame, BOOL isKeyboardVisible) {
        if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
            return CGRectMake(CGRectGetMidX(weakFormSheet.presentationController.containerView.bounds) - 210, currentFrame.origin.y
                              +50, 420, currentFrame.size.height/2);
        }
        
        return currentFrame;
    };
    formSheetController.modalPresentationCapturesStatusBarAppearance = NO;
    [self presentViewController:formSheetController animated:YES completion:nil];
}


- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (IBAction)btnSubmitPressed:(id)sender {
   if ([_txtUserName.text isEqualToString:@""] || [_txtMobileNo.text isEqualToString:@""] || [_txtEmail.text isEqualToString:@""] || [_txtPassword.text isEqualToString:@""]) {
        [AlertView showAlert:@"Please fill the blank details." alertType:AJNotificationTypeRed];
        return;
    }else if ([_txtMobileNo.text length] < 10 ) {
        [AlertView showAlert:@"Mobile no must be a combination of minimum 10 characters." alertType:AJNotificationTypeRed];
        return ;
    }else if (![self NSStringIsValidEmail:_txtEmail.text]) {
        [AlertView showAlert:@"Please enter valid Email Address." alertType:AJNotificationTypeRed];
        return ;
    }else {
        appdel.socialID = appdel.socialID+1;

        diction = [[NSMutableDictionary alloc]init];
        
        [diction setValue:self.txtUserName.text.length>0?self.txtUserName.text:@"" forKey:@"firstName"];
        [diction setValue:@"" forKey:@"lastName"];
        [diction setValue:self.txtPassword.text.length>0?self.txtPassword.text:@"" forKey:@"password"];
        [diction setValue:self.txtEmail.text.length>0?self.txtEmail.text:@"" forKey:@"emailAddress"];
        [diction setValue:self.txtMobileNo.text.length>0?[NSString stringWithFormat:@"%@",self.txtMobileNo.text]:@"" forKey:@"mobilePrimary"];
        [diction setValue:@"MI" forKey:@"channel"];
        [diction setValue:self.txtReferalCode.text.length>0?self.txtReferalCode.text:@"" forKey:@"referalCode"];
        
        [self callPostApiForRegistration:diction withAccesToken:@""];

    }
   
}

- (BOOL)NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (void)callPostApiForRegistration:(NSMutableDictionary *)dictParam withAccesToken:(NSString *)strToken {
   
    NSLog(@"diction %@",dictParam);
    if([NetworkCheck isInternetOff]) {
        
        [AlertView showAlert:@"The Internet connection appears to be offline." alertType:AJNotificationTypeRed];
        [LoadingView dismissLoader];
    }else {
        [LoadingView showLoaderWithOverlay:YES];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        AFJSONRequestSerializer* serialiser = [AFJSONRequestSerializer serializer];
        manager.responseSerializer=[AFJSONResponseSerializer serializer];
        manager.requestSerializer = serialiser;
        manager.responseSerializer.acceptableStatusCodes = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(200, 403)];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        NSString *strPostUrl;
        if (strToken.length > 0) {
            [manager.requestSerializer setValue:strToken forHTTPHeaderField:@"Authorization"];
            strPostUrl = [NSString stringWithFormat:@"%@%@",BACKENDBASEURL,ADDMOBILE];
        }else {
            strPostUrl = [NSString stringWithFormat:@"%@%@",BACKENDBASEURL,REGISTRATION];
        }
        
        [manager POST:strPostUrl parameters:dictParam success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [LoadingView dismissLoader];
        
            NSDictionary *responseDict = (NSDictionary *)responseObject;
            
            if ([Utility isValidString:[responseDict valueForKey:@"message"]]) {
           
            if ([[[responseDict valueForKey:@"data"] valueForKey:@"accontStatus"] isEqual:[NSNumber numberWithInt:3]])     {
        //show otp screen and verify number
        //[LoadingView dismissLoader];
                if ([[responseDict valueForKey:@"data"] valueForKey:@"userId"]) {
                strUserId = [NSString stringWithFormat:@"%@",[[responseDict valueForKey:@"data"] valueForKey:@"userId"]];
                    [APPDELEGATE setUserID:strUserId];

                }
        OTPController *controller = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"OTPController"];
                controller.userId = strUserId;
                controller.mobileNumber = _txtMobileNo.text;
                [self.navigationController pushViewController:controller animated:true];
                
    }else if ([[[responseDict valueForKey:@"data"] valueForKey:@"accontStatus"] isEqual:[NSNumber numberWithInt:2]]){
        //show mobile number screen and then verify Otp
        
        
    }else if ([[[responseDict valueForKey:@"data"] valueForKey:@"accontStatus"] isEqual:[NSNumber numberWithInt:1]]){
        //registered successfully
        
        [[NSUserDefaults standardUserDefaults] setValue:appdel.userID forKey:UserId];
        [self.navigationController popViewControllerAnimated:true];
        
        [AlertView showAlert:@"You are now registered successfully" alertType:AJNotificationTypeGreen];
        
    }else{
        //do nothing
        [AlertView showAlert:[responseDict valueForKey:@"message"] alertType:AJNotificationTypeRed];
    }
            }else{
                [AlertView showAlert:@"Server Error" alertType:AJNotificationTypeRed];

            }
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [LoadingView dismissLoader];
        }];

    }
}

- (void)GetDataAndCallAPIVerifyNumber:(NSDictionary *)dictParam {
    NSLog(@"%@",dictParam );
    
        diction = [NSMutableDictionary dictionary];
        [diction setValue:strUserId forKey:@"userId"];
        [diction setValue:self.txtMobileNo.text forKey:@"mobile"];
        
        strUserName = [NSString stringWithFormat:@"%@",[dictParam valueForKey:@"userName"]];
        strAccessToken = [NSString stringWithFormat:@"%@ %@",[dictParam valueForKey:@"tokenType"],[dictParam valueForKey:@"token"]];
        
        [self callPostApiForRegistration:diction withAccesToken:strAccessToken];
    

}

- (IBAction)backButton_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}


- (void)callSecondMethod  {
    [self callPostApiForRegistration:diction withAccesToken:@"Access"];
}

- (IBAction)btnBackPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - UITextFieldDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    
    if (textField.tag==111) {
        
        NSString *tempString = [NSString stringWithFormat:@"%@%@",textField.text,string];
        
        if (tempString.length>10) {
            return NO;
            
        }else{
            return YES;
            
        }
    }else if (textField.tag==10){
            NSString *stringEntered;
            
            if(string.length>1){
                stringEntered = string;
            }
            
            unichar unicodevalue = [stringEntered characterAtIndex:0];
            if (unicodevalue == 55357) {
                return NO;
            }
            else if (![self containsSpecialCharectersForName:string]){
                return NO;
            }
            else{
                stringEntered = [string length]==0?[textField.text substringToIndex:[textField.text length]-1]:[NSString stringWithFormat:@"%@%@",textField.text,string];
            }
            
            if (stringEntered.length>50 && ![stringEntered isEqual:@""]) {
                return NO;
            }
            else{
                return YES;
            }
    }
    else{
        return YES;
        
    }
    
}
-(BOOL)containsSpecialCharectersForName:(NSString*)string{
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS_NAMING_FIELDS] invertedSet];
    
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    
    return [string isEqualToString:filtered];
}
@end
