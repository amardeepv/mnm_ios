//
//  segmentViewServiceDetailVC.h
//  MakeNMake
//
//  Created by Archit Saxena on 26/03/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CarbonKit/CarbonKit.h>

@interface segmentViewServiceDetailVC : UIViewController<CarbonTabSwipeNavigationDelegate> {
    CarbonTabSwipeNavigation *carbonTabSwipeNavigation;
    NSArray *items;
}

@property (strong, nonatomic) NSString *isFromPlanScreen;

@property (weak, nonatomic) IBOutlet UIView *targetView;
@property(weak, nonatomic) IBOutlet UIToolbar *toolBar;
@property (weak, nonatomic) IBOutlet UILabel *serviceNameTitle;
@property (weak, nonatomic) NSString *NameTitle;

@property (retain, nonatomic) NSMutableArray *arrList;

@end
