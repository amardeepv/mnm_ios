//
//  segmentViewServiceDetailVC.m
//  MakeNMake
//
//  Created by Archit Saxena on 26/03/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "segmentViewServiceDetailVC.h"
#import "faqVC.h"
#import "HomeDetailViewController.h"
#import "UnlimitedPlanVC.h"
#import "FixedPlanVC.h"
#import "AddOnPlanVC.h"


@interface segmentViewServiceDetailVC ()

{
    
}

@end

@implementation segmentViewServiceDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
     [self setupPageView];
}
- (BOOL)prefersStatusBarHidden {
    return YES;
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
   
    CGFloat totalToolBarWidthForOneItem = _targetView.frame.size.width/2;
    
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:totalToolBarWidthForOneItem forSegmentAtIndex:0];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:totalToolBarWidthForOneItem forSegmentAtIndex:1];
    
    [carbonTabSwipeNavigation setTabExtraWidth:1];
}

-(void)setupPageView{
    _serviceNameTitle.text = _NameTitle;

   
    if ([_isFromPlanScreen isEqualToString:@"UnlimitedPlanVC"]) {
        items = @[@"DESCRIPTION",
                  @"ABOUT",
                  ];
    }else if ([_isFromPlanScreen isEqualToString:@"FixedPlanVC"]){
        items = @[@"DESCRIPTION",
                  @"ABOUT",
                  ];
    }else if ([_isFromPlanScreen isEqualToString:@"AddOnPlanVC"]){
        items = @[@"DESCRIPTION",
                  @"ABOUT",
                  ];
    }else{
        items = @[@"DESCRIPTION",
                  @"FAQ'S",
                  ];
    }
    
    carbonTabSwipeNavigation =
    [[CarbonTabSwipeNavigation alloc] initWithItems:items toolBar:self.toolBar delegate:self];
    [carbonTabSwipeNavigation insertIntoRootViewController:self andTargetView:self.targetView];
    
    [self style];
}
- (void)style {
    
    UIColor *color = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor = color;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    
     CGFloat totalToolBarWidthForOneItem = _targetView.frame.size.width/2;

    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:totalToolBarWidthForOneItem forSegmentAtIndex:0];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:totalToolBarWidthForOneItem forSegmentAtIndex:1];
    
    [carbonTabSwipeNavigation setTabExtraWidth:1];

    carbonTabSwipeNavigation.toolbar.translucent = NO;
    carbonTabSwipeNavigation.toolbar.backgroundColor = [UIColor blackColor];
    carbonTabSwipeNavigation.carbonSegmentedControl.backgroundColor = [UIColor blackColor];
    carbonTabSwipeNavigation.carbonSegmentedControl.indicatorPosition = IndicatorPositionBottom;
    [carbonTabSwipeNavigation setIndicatorColor:[UIColor colorWithRed:129.0f/255 green:6.0f/255 blue:127.0f/255 alpha:1]];//81067F
    [carbonTabSwipeNavigation setNormalColor:[color colorWithAlphaComponent:0.7]
                                        font:[UIFont boldSystemFontOfSize:14]];
    [carbonTabSwipeNavigation setSelectedColor:color font:[UIFont boldSystemFontOfSize:14]];
}
#pragma mark - CarbonTabSwipeNavigation Delegate
- (nonnull UIViewController *)carbonTabSwipeNavigation:
(nonnull CarbonTabSwipeNavigation *)carbontTabSwipeNavigation
                                 viewControllerAtIndex:(NSUInteger)index {
    faqVC *latestObj = [[faqVC alloc]initWithNibName:@"faqVC" bundle:nil];
  
    if ([_isFromPlanScreen isEqualToString:@"UnlimitedPlanVC"]) {

        UnlimitedPlanVC *controllerUnlimited = [[UnlimitedPlanVC alloc]initWithNibName:@"UnlimitedPlanVC" bundle:nil];
        
        if (index==0) {
            return controllerUnlimited;
        }else{
            latestObj.serviceName = @"UnlimitedPlanVC";

            return latestObj;

        }
    }else if ([_isFromPlanScreen isEqualToString:@"FixedPlanVC"]){
        FixedPlanVC *controllerFixed = [[FixedPlanVC alloc]initWithNibName:@"FixedPlanVC" bundle:nil];

        if (index==0) {
            return controllerFixed;
        }else{
            latestObj.serviceName = @"FixedPlanVC";

            return latestObj;
            
        }
    }else if ([_isFromPlanScreen isEqualToString:@"AddOnPlanVC"]){
        AddOnPlanVC *controllerAddOn = [[AddOnPlanVC alloc]initWithNibName:@"AddOnPlanVC" bundle:nil];

        if (index==0) {
            return controllerAddOn;
        }else{
            latestObj.serviceName = @"AddOnPlanVC";

            return latestObj;
            
        }
    }else{
    HomeDetailViewController *controller = [[UIStoryboard storyboardWithName:@"Second" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeDetailViewController"];
        controller.arrList = _arrList;

        if (index==0) {
            return controller;
        }else{
            latestObj.serviceName = _serviceNameTitle.text;

            return latestObj;
            
        }
    }
}
- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                 willMoveAtIndex:(NSUInteger)index {


}

- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                  didMoveAtIndex:(NSUInteger)index {
}

- (UIBarPosition)barPositionForCarbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation {
    return UIBarPositionTop; //
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backBtn_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
