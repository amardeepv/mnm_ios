//
//  SecondVC.m
//  MakeNMake
//
//  Created by Nripendra on 18/03/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "HomeDetailViewController.h"
#import "HSecondCustomCell.h"
#import "HVideoCustomCell.h"
#import "PageMenuCustomCell.h"
#import "ImageTableViewCell.h"
#import "AllServiceViewController.h"
#import "AppDelegate.h"
#import "Constant.h"
#import "Utility.h"

@interface HomeDetailViewController ()<UITableViewDataSource, UITableViewDelegate>

@end

@implementation HomeDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _lblTitle.text = [[_arrList  valueForKey:@"serviceGroupName"] objectAtIndex:0];
    _strDiscr = [NSString stringWithFormat:@"%@",[[_arrList valueForKey:@"description"] objectAtIndex:0]];
    
    if ([[[_arrList valueForKey:@"showOffer"] objectAtIndex:0] isEqual:[NSNumber numberWithInt:0]]) {
        [APPDELEGATE setShowCouponField:false];
    }else{
        [APPDELEGATE setShowCouponField:true];

    }
    _videoUrlString = [[_arrList  valueForKey:@"videoUrl"] objectAtIndex:0];
    self.tblDescription.delegate   = self;
    self.tblDescription.dataSource = self;
    self.tblDescription.estimatedRowHeight = 200;
    self.tblDescription.rowHeight = UITableViewAutomaticDimension;

    [self registerCells];
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}
- (void)registerCells
{
    [self.tblDescription registerNib:[UINib nibWithNibName:@"HSecondCustomCell" bundle:nil]forCellReuseIdentifier:@"HSecondCustomCell"];
    [self.tblDescription registerNib:[UINib nibWithNibName:@"HVideoCustomCell" bundle:nil]forCellReuseIdentifier:@"HVideoCustomCell"];
    
    [self.tblDescription registerNib:[UINib nibWithNibName:@"PageSlideTableCell" bundle:nil]forCellReuseIdentifier:@"PageSlideTableCell"];
    [self.tblDescription registerNib:[UINib nibWithNibName:@"ImageTableViewCell" bundle:nil]forCellReuseIdentifier:@"ImageTableViewCell"];

    [self.tblDescription registerNib:[UINib nibWithNibName:@"faqTableViewCell" bundle:nil]forCellReuseIdentifier:@"faqTableViewCell"];

}

- (CGSize)findHeightForText:(NSString *)text havingWidth:(CGFloat)widthValue andFont:(UIFont *)font {
    CGSize size = CGSizeZero;
    if([text isEqualToString:@"<null>"]){
        text = @"";
    }
    if (text.length > 0 ) {
        CGRect frame = [text boundingRectWithSize:CGSizeMake(widthValue, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName:font } context:nil];
        size = CGSizeMake(frame.size.width, frame.size.height + 200);
    }
    return size;
}

-(UITableViewCell *)cellForHomeSecondCell:(NSIndexPath *)indexPath : (UITableView *)tableView
{
    static NSString *cellIdentifier = @"HSecondCustomCell";
    
    HSecondCustomCell *cell = (HSecondCustomCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (![_strDiscr isEqualToString:@"<null>"]) {
        cell.strWebContent = _strDiscr;
    }else{
        cell.strWebContent = @"No Description found for this service.";
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    cell.serviceNamelabel.text = [[_arrList  valueForKey:@"serviceGroupName"] objectAtIndex:0];
    
    if ([[[_arrList  valueForKey:@"serviceGroupName"] objectAtIndex:0] caseInsensitiveCompare:@"Electrical"] == NSOrderedSame) {
        [cell.BannerImage setImage:[UIImage imageNamed:@"ELectricalBanner"]];
   
    }else if ([[[_arrList  valueForKey:@"serviceGroupName"] objectAtIndex:0] caseInsensitiveCompare:@"Plumbing"] == NSOrderedSame){
        [cell.BannerImage setImage:[UIImage imageNamed:@"PlumbingBanner"]];

    }else if ([[[_arrList  valueForKey:@"serviceGroupName"] objectAtIndex:0] caseInsensitiveCompare:@"Carpentry"]== NSOrderedSame){
        [cell.BannerImage setImage:[UIImage imageNamed:@"CarpentryBanner"]];
        
    }else if ([[[_arrList  valueForKey:@"serviceGroupName"] objectAtIndex:0] caseInsensitiveCompare:@"PEST CONTROL"]== NSOrderedSame){
        [cell.BannerImage setImage:[UIImage imageNamed:@"PestControlBanner"]];
        
    }else if ([[[_arrList  valueForKey:@"serviceGroupName"] objectAtIndex:0] caseInsensitiveCompare:@"AC"]== NSOrderedSame){
        [cell.BannerImage setImage:[UIImage imageNamed:@"ACBanner"]];
        
    }else if ([[[_arrList  valueForKey:@"serviceGroupName"] objectAtIndex:0] caseInsensitiveCompare:@"WATER TANK CLEANING"]== NSOrderedSame){
        [cell.BannerImage setImage:[UIImage imageNamed:@"WaterTankCleaningBanner"]];
        
    }else if ([[[_arrList  valueForKey:@"serviceGroupName"] objectAtIndex:0] caseInsensitiveCompare:@"SECURITY SERVICES"]== NSOrderedSame){
        [cell.BannerImage setImage:[UIImage imageNamed:@"SecurityServicesBanner"]];
        
    }else if ([[[_arrList  valueForKey:@"serviceGroupName"] objectAtIndex:0] caseInsensitiveCompare:@"Gardening"]== NSOrderedSame){
        [cell.BannerImage setImage:[UIImage imageNamed:@"GardeningBanner"]];
        
    }else if ([[[_arrList  valueForKey:@"serviceGroupName"] objectAtIndex:0] caseInsensitiveCompare:@"CCTV"]== NSOrderedSame){
        [cell.BannerImage setImage:[UIImage imageNamed:@"CCTVBanner"]];
        
    }else if ([[[_arrList  valueForKey:@"serviceGroupName"] objectAtIndex:0] caseInsensitiveCompare:@"RO"]== NSOrderedSame){
        [cell.BannerImage setImage:[UIImage imageNamed:@"ROBanner"]];
        
    }else if ([[[_arrList  valueForKey:@"serviceGroupName"] objectAtIndex:0] caseInsensitiveCompare:@"Home Cleaning"]== NSOrderedSame){
        [cell.BannerImage setImage:[UIImage imageNamed:@"HomeCleaningBanner"]];
        
    }else if ([[[_arrList  valueForKey:@"serviceGroupName"] objectAtIndex:0] caseInsensitiveCompare:@"Home Appliances"]== NSOrderedSame){
        [cell.BannerImage setImage:[UIImage imageNamed:@"HomeAppliancesBanner"]];
        
    }else if ([[[_arrList  valueForKey:@"serviceGroupName"] objectAtIndex:0] caseInsensitiveCompare:@"PEST CONTROL"]== NSOrderedSame){
        [cell.BannerImage setImage:[UIImage imageNamed:@"PestControlBanner"]];
        
    }else if ([[[_arrList  valueForKey:@"serviceGroupName"] objectAtIndex:0] caseInsensitiveCompare:@"CarCleaning"]== NSOrderedSame){
        [cell.BannerImage setImage:[UIImage imageNamed:@"CarCleaningBanner"]];
        
    }else if ([[[_arrList  valueForKey:@"serviceGroupName"] objectAtIndex:0] caseInsensitiveCompare:@"Upholstery Cleaning"]== NSOrderedSame){
        [cell.BannerImage setImage:[UIImage imageNamed:@"UphosteryCleaningBanner"]];
        
    }else {
        [cell.BannerImage setImage:[UIImage imageNamed:@"InteriorDesignBanner"]];
    }
    
    [cell configureView];
    
    return cell;
}

-(UITableViewCell *)cellForHomeVideoCell:(NSIndexPath *)indexPath : (UITableView *)tableView
{
    static NSString *cellIdentifier = @"HVideoCustomCell";
    HVideoCustomCell *cell = (HVideoCustomCell *)[tableView dequeueReusableCellWithIdentifier: cellIdentifier];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

    [cell videoSetupfrom:_videoUrlString];
    return cell;
}

-(UITableViewCell *)cellForHomePaginCell:(NSIndexPath *)indexPath : (UITableView *)tableView
{
    PageMenuCustomCell * Cell = [tableView dequeueReusableCellWithIdentifier:@"PageMenuCustomCell"];
    if (!Cell){
        NSArray * arrNib = [[NSBundle mainBundle ]loadNibNamed:@"PageMenuCustomCell" owner:self options:nil];
        Cell = (PageMenuCustomCell *)arrNib[0];
        [Cell setArrayForHomePageMenuForViewController:self];
        
    }
    Cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return Cell;
}


-(UITableViewCell *)cellForBestOfferCell:(NSIndexPath *)indexPath : (UITableView *)tableView
{
    static NSString *cellIdentifier = @"ImageTableViewCell";
    ImageTableViewCell *cell = (ImageTableViewCell *)[tableView dequeueReusableCellWithIdentifier: cellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor brownColor];
    
    return cell;
}

#pragma Mark - UITableView delegate and datasource.
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (APPDELEGATE.showCouponField) {
        return 3;
    }else{
        return 2;
    }
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
        if(indexPath.section == 0)
        {
            return UITableViewAutomaticDimension;
        }
        else if(indexPath.section == 1)
        {
            return 180.0;
        }
        else{
            if (APPDELEGATE.showCouponField) {
                if (indexPath.section == 2)
                {
                    return 120;
                }else{
                    return 170;

                }
            }else{
                return 170;

            }
        }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{


    if(indexPath.section==0) {
        return  [self cellForHomeSecondCell:indexPath :tableView];
    }
    else {
    
        if (APPDELEGATE.showCouponField && indexPath.section == 2) {
//            if (indexPath.section == 2)
//            {
                return  [self cellForBestOfferCell:indexPath:tableView];
//            }
            //else{
            //   return  [self cellForHomePaginCell:indexPath :tableView];
                
            //}
//        }
        //else{
           // return  [self cellForHomePaginCell:indexPath :tableView];
            
        //}
        }else{
            return  [self cellForHomeVideoCell:indexPath:tableView];

        }
    
}
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.001f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 15.0f;
}
- (IBAction)btnBackPressed:(id)sender; {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btnBookingPressed:(id)sender; {
    AllServiceViewController *controller = [[UIStoryboard storyboardWithName:@"Second" bundle:nil] instantiateViewControllerWithIdentifier:@"AllServiceViewController"];
    controller.isFrom  = HomeDetail;

    controller.titleString = [[_arrList  valueForKey:@"serviceGroupName"] objectAtIndex:0];
    controller.serviceGroupId = [[_arrList  valueForKey:@"serviceGroupId"] objectAtIndex:0];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
