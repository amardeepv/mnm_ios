//
//  SecondVC.h
//  MakeNMake
//
//  Created by Nripendra on 18/03/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeDetailViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tblDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet NSString *videoUrlString;

@property (weak, nonatomic) IBOutlet UIButton *descBtn;
@property (retain, nonatomic) NSMutableArray *arrList;

@property (retain, nonatomic) NSString  *strDiscr;
@property (retain, nonatomic) IBOutlet UIButton  *btnBack;
@property (retain, nonatomic) IBOutlet UIButton  *btnBooking;
- (IBAction)btnBackPressed:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *faqBtn;
- (IBAction)btnBookingPressed:(id)sender;
@end
