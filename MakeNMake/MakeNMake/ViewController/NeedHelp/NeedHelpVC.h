//
//  NeedHelpVC.h
//  MakeNMake
//
//  Created by Archit Saxena on 02/05/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NeedHelpVC : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIView *needHelpIconView;
@property (weak, nonatomic) IBOutlet UITableView *needHelpTableView;
@property (weak, nonatomic) IBOutlet UIView *tableBackView;

@end
