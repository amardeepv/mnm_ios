//
//  NeedHelpVC.m
//  MakeNMake
//
//  Created by Archit Saxena on 02/05/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "NeedHelpVC.h"
#import "needHelpTableCell.h"
#import "ContactUsVC.h"
#import "VerifyMobileNumberVC.h"
#import "AppDelegate.h"
#import "Constant.h"
#import "Utility.h"
#import "LoginSegment.h"

@interface NeedHelpVC ()
{
    NSMutableArray *dataArray;
}
@end

@implementation NeedHelpVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
   // [self getDataForOpenTicket];
    [self registerNibs];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self configureView];

    }
#pragma mark - Custom Methods

-(void)registerNibs{
    
    UINib* nibAddress = [UINib nibWithNibName:@"needHelpTableCell" bundle:nil];
    [_needHelpTableView registerNib:nibAddress forCellReuseIdentifier:@"needHelpTableCell"];
    
    
}
-(void)configureView{
    
    if (![[Utility sharedInstance] unArchiveFileWithFileName:OA_LOGIN_FILE]) {
        LoginSegment  *loginObj = [[LoginSegment alloc]initWithNibName:@"LoginSegment" bundle:nil];
        loginObj.loginState = loginScreen;
        [self.navigationController pushViewController:loginObj animated:YES];
        
    }else{

    
    dataArray  = [[NSMutableArray alloc]init];
    
    [dataArray addObject:@{ @"IconName" : @"lockIcon" , @"text" : @"Create New Password"}];
    [dataArray addObject:@{ @"IconName" : @"UserMobile" , @"text" : @"Contact Us"}];

    
    [_needHelpIconView.layer setShadowColor:[UIColor blackColor].CGColor];
    [_needHelpIconView.layer setShadowRadius:8.0f];
    [_needHelpIconView.layer setMasksToBounds:false];
    [_needHelpIconView.layer setShadowOpacity: 0.5];
    
    }
}

-(void)viewDidLayoutSubviews{
    
    [super viewDidLayoutSubviews];
    //    [_serviceTableView sizeToFit];
    
}

#pragma mark - TableView DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    needHelpTableCell *cell = [_needHelpTableView dequeueReusableCellWithIdentifier:@"needHelpTableCell"];
   
    [cell configureCellWithLabelText:[[dataArray objectAtIndex:indexPath.section] valueForKey:@"text"] andImageName:[[dataArray objectAtIndex:indexPath.section] valueForKey:@"IconName"]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell.layer setCornerRadius:8];
    [cell.layer setMasksToBounds:true];

    return cell;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    switch (indexPath.section) {
        case 0:{
            VerifyMobileNumberVC *obj = [[VerifyMobileNumberVC alloc]initWithNibName:@"VerifyMobileNumberVC" bundle:nil];
            obj.userId = [APPDELEGATE getUserID];
            [obj setIsFromScreen:ChangePassword];
            
            [self.navigationController pushViewController:obj animated:true];
        }
            break;
        case 1:{
            ContactUsVC *Obj = [[ContactUsVC alloc]initWithNibName:@"ContactUsVC" bundle:nil];
            [self.navigationController pushViewController:Obj animated:true];
        }
            break;
        default:{
            ContactUsVC *Obj = [[ContactUsVC alloc]initWithNibName:@"" bundle:nil];
            [self.navigationController pushViewController:Obj animated:true];
        }
            break;
    }
}


#pragma mark - TableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section!=0) {
        return 0.001f;
    }else{
        return 15;      //First section
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    //    if (section==serviceListArray.count-1) {
    //        return 0.001f;
    //    }else{
    return 15;
    //    }
}
#pragma mark - Api Calling

-(void)getDataForOpenTicket{
    
//    NSString *userId = @"4120";  //[APPDELEGATE getUserID]
//    NSString *serviceNameWithParam = [NSString stringWithFormat:@"GetPlanForOpenTicket?UserId=%@",userId];
//    
//    [[APIList sharedAPIList] API_GetAllDataFromWS:serviceNameWithParam ShowLoader:YES showOverlay:YES completion:^(BOOL success, id responceData, NSMutableArray *share) {
//        if (success) {
//            NSDictionary * result =(NSDictionary *) responceData;
//            if ([[result objectForKey:@"hasError"] isEqual:@(0)]) {
//                [self GetArrayFromResponse:[[result valueForKey:@"data"] valueForKey:@"Plans"]];
//            }
//        }else{
//            [AlertView showAlert:@"Some Error Occured." alertType:AJNotificationTypeGreen];
//            
//        }
//    }];
    
    
    
}

- (IBAction)backButton_Tapped:(id)sender {
    
    [self.navigationController popViewControllerAnimated:true];
    
}
#pragma mark - ParseResponse Method

- (void)GetArrayFromResponse:(NSMutableArray *)arrResponse {
    
//    NSLog(@"array recieved - %@",arrResponse);
//    serviceListArray = [[NSMutableArray alloc]init];
//    serviceListArray = [[arrResponse firstObject] valueForKey:@"serviceList"];
//    
//    [_serviceTableView reloadData];
//    [_serviceTableView sizeToFit];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
@end
