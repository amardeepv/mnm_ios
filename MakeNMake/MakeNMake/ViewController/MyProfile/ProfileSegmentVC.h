//
//  ProfileSegmentVC.h
//  MakeNMake
//
//  Created by Archit Saxena on 01/05/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CarbonKit/CarbonKit.h>

@interface ProfileSegmentVC : UIViewController<CarbonTabSwipeNavigationDelegate>{
    CarbonTabSwipeNavigation *carbonTabSwipeNavigation;
    NSArray *items;
}
@property (weak, nonatomic) IBOutlet UIView *targetView;
@property (weak, nonatomic) IBOutlet UIToolbar *toolbarView;
@property (weak, nonatomic) IBOutlet UIView *profileIconView;

@end
