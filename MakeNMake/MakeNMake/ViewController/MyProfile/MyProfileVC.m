//
//  MyProfileVC.m
//  MakeNMake
//
//  Created by Archit Saxena on 01/05/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "MyProfileVC.h"
#import "ProfileCustomTableCell.h"
#import "ProfileAddressTableCell.h"
#import "APIList.h"
#import "AlertView.h"
#import "Constant.h"
#import "Utility.h"
#import "TicketServiceNameTableViewCell.h"
#import "LoadingView.h"
#import "AFNetworking.h"
#import "NetworkCheck.h"
#import "AddAddressCustomCell.h"
#import "OpenTicketVC.h"

@interface MyProfileVC ()
{
    NSMutableArray *titleLabelArray, *IconNameArray, *textFieldArray, *addressArray, *tempAddressArray;
    OAPopOverListVC *listVC;
    int selectedRowIndex;
    int selectedIndex;
    BOOL isNewCell;
    CGRect popOverFrame;
    NSMutableDictionary *ProfileUpdatedDictionary, *editedAddressDictionary;
}
@end

@implementation MyProfileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self registerNib];
    [self configureView];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}
#pragma mark - Custom Methods

-(void)configureView{
    
    if ([[Utility sharedInstance] unArchiveFileWithFileName:OA_LOGIN_FILE]) {
        //User Logged in
        [_customNavigationBar setHidden:true];
        if (_fromScreen==AddressScreen || _fromScreen==TicketScreen) {
            if (_fromScreen==TicketScreen){
                [_customNavigationBar setHidden:false];
            }
        isNewCell = false;
        editedAddressDictionary = [[NSMutableDictionary alloc]init];
        [_saveButton setHidden:true];
        
            //get Address for Logged in user
            [self getAddressApiCallwithUserId:[APPDELEGATE getUserID]];
    }else{
        [self getDataForMyProfile];
        [_saveButton setHidden:false];
        
    }
        [_noDataFoundView removeFromSuperview];
    }else{
        
        //User not logged in
        [_saveButton setHidden:true];
        _noDataFoundView.center = _profileTableView.center;
        [self.view addSubview:_noDataFoundView];
        
        
    }
}
-(void)setDOBRange{
    //    CGRect frame = viewDatePicker.frame;
    //    frame.size.width = self.view.frame.size.width;
    //    frame.size.height = self.view.frame.size.height;
    //    viewDatePicker.frame = frame;
    
    //    [viewDatePicker setHidden:false];
    [_datePicker setDatePickerMode:UIDatePickerModeDate];
    NSCalendar* calender = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents* component = [[NSDateComponents alloc] init];
    [component setYear:-100];
    _datePicker.date = [calender dateByAddingComponents:component toDate:[NSDate date] options:0];
    NSDate * minDate = [calender dateByAddingComponents:component toDate:[NSDate date] options:0];
    [_datePicker setMinimumDate:minDate];
    [component setYear:-0];
    NSDate* maxDate = [calender dateByAddingComponents:component toDate:[NSDate date] options:0];
    [_datePicker setMaximumDate:maxDate];
    _datePicker.date = maxDate;
    [self.view addSubview:_datePickerUIView];
    [_datePicker setMinimumDate:minDate];
    [_datePicker setMaximumDate:maxDate];
    

}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    if ([[Utility sharedInstance] unArchiveFileWithFileName:OA_LOGIN_FILE]) {
        //User Logged in

    }else{
        _noDataFoundView.center = _profileTableView.center;
        [self.view addSubview:_noDataFoundView];
    }
    
    CGRect frame = _datePickerUIView.frame;
    frame.size.width = _profileTableView.frame.size.width;
    frame.size.height = _profileTableView.frame.size.height;
    _datePickerUIView.frame = frame;
    
    [_datePickerUIView setHidden:true];
    [self setDOBRange];
}
-(void)registerNib{
    UINib *nib = [UINib nibWithNibName:@"ProfileCustomTableCell" bundle:nil];
    [_profileTableView registerNib:nib forCellReuseIdentifier:@"ProfileCustomTableCell"];
    
    UINib *nib2 = [UINib nibWithNibName:@"ProfileAddressTableCell" bundle:nil];
    [_profileTableView registerNib:nib2 forCellReuseIdentifier:@"ProfileAddressTableCell"];
    
    UINib *nib3 = [UINib nibWithNibName:@"AddAddressCustomCell" bundle:nil];
    [_profileTableView registerNib:nib3 forCellReuseIdentifier:@"AddAddressCustomCell"];

}
-(void)editButtonTapped:(CustomButton*)sender{

        selectedRowIndex= sender.tagIndex;
        selectedIndex = sender.tagIndex;
    
        CGRect superviewFrame= [_profileTableView rectForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:selectedRowIndex]];
        CGRect superViewFinalFrame =  [_profileTableView convertRect:superviewFrame toView:[_profileTableView superview]];
        popOverFrame = sender.frame;
        CGRect tempframe = popOverFrame;
        tempframe.origin.x = self.view.frame.size.width-60;
        tempframe.origin.y = superViewFinalFrame.origin.y+5;
        popOverFrame = tempframe;
    
    
    NSMutableArray *tempArray = [[NSMutableArray alloc]init];
        [tempArray addObject:@"Edit"];
        [tempArray addObject:@"Delete"];

        [self showPopOverControllerForArray:tempArray andFrame:CGRectZero];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (_fromScreen==AddressScreen || _fromScreen==TicketScreen) {
        ProfileAddressTableCell *cell2 = [tableView dequeueReusableCellWithIdentifier:@"ProfileAddressTableCell"];
        AddAddressCustomCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AddAddressCustomCell"];
//        [cell2.editButton setHidden:true];

        if (isNewCell) {
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [cell prepareForReuse];
            
            [cell.cityTxtFld setDelegate:self];
            [cell.stateTxtFld setDelegate:self];
            [cell.addressLine1TxtFld setDelegate:self];
            
            
            [cell.addressHeaderLabel setText:[NSString stringWithFormat:@"Address %d",selectedIndex+1]];
            [cell configureCellWithDict:[addressArray objectAtIndex:selectedIndex]];
            
            [editedAddressDictionary setValue:[[addressArray objectAtIndex:selectedIndex] valueForKey:@"line1"] forKey:@"line1"];
            [editedAddressDictionary setValue:[[addressArray objectAtIndex:selectedIndex] valueForKey:@"title"] forKey:@"title"];
            [editedAddressDictionary setValue:[[addressArray objectAtIndex:selectedIndex] valueForKey:@"city"] forKey:@"city"];
            [editedAddressDictionary setValue:[[addressArray objectAtIndex:selectedIndex] valueForKey:@"area"] forKey:@"area"];
            [editedAddressDictionary setValue:[[addressArray objectAtIndex:selectedIndex] valueForKey:@"state"] forKey:@"state"];
            [editedAddressDictionary setValue:[[addressArray objectAtIndex:selectedIndex] valueForKey:@"id"] forKey:@"id"];

            [self.saveButton setHidden:false];
            [self.saveButton setTitle:@"Add New Address" forState:UIControlStateNormal];
            return cell;

        }else{
//            [cell2.editButton setHidden:false];

            [cell2 setSelectionStyle:UITableViewCellSelectionStyleNone];
            [cell2 prepareForReuse];
            [cell2.editButton addTarget:self action:@selector(editButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
            [cell2.addHeader setText:[NSString stringWithFormat:@"Address %ld",(long)indexPath.section + 1]];
            [cell2 configureCellWithDictionary:[addressArray objectAtIndex:indexPath.section]];
            [cell2.editButton setTagIndex:(int)indexPath.section];
            
            return cell2;

        }

    }else{
        ProfileCustomTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProfileCustomTableCell"];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell prepareForReuse];
        [cell.textField setTag:indexPath.section];
        [cell.textField setDelegate:self];
        [cell configureCellWithTitleLabel:[titleLabelArray objectAtIndex:indexPath.section] andTextfieldText:[textFieldArray objectAtIndex:indexPath.section] andIconName:[IconNameArray objectAtIndex:indexPath.section]];
        
        return cell;
    }
    
}
#pragma mark - UITableView Delegate
    - (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
        if (_fromScreen==TicketScreen){
            NSLog(@"Addrss selected");
           OpenTicketVC *Obj = [[OpenTicketVC alloc] initWithNibName:@"OpenTicketVC" bundle:nil];
            Obj.selectedAddress = [addressArray objectAtIndex:indexPath.section];
        [[NSUserDefaults standardUserDefaults] setValue:[[addressArray objectAtIndex:indexPath.section] valueForKey:@"id"] forKey:@"selectedAddressIDOpenTicket"];

         [self.navigationController pushViewController:Obj animated:true];
            
        }
    }

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    if (_fromScreen==AddressScreen || _fromScreen==TicketScreen) {
        return addressArray.count;
    }else{
        return titleLabelArray.count;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_fromScreen==AddressScreen || _fromScreen==TicketScreen) {
        if (isNewCell) {
            return 150;

        }else{
            return 100;

        }
    }else{
        return 50;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.001f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10.0f;
}
#pragma mark - UITextField Delegate
- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (_fromScreen==AddressScreen || _fromScreen==TicketScreen) {
                if ([textField.placeholder isEqualToString:@"Address Line 1"]) {
                    
                    NSLog(@"%@",textField.text);
                    [editedAddressDictionary setValue:textField.text forKey:@"line1"];
                    [editedAddressDictionary setValue:textField.text forKey:@"title"];

                }else if ([textField.placeholder isEqualToString:@"City"]){
                    NSLog(@"%@",textField.text);
                    [editedAddressDictionary setValue:textField.text forKey:@"city"];
                    [editedAddressDictionary setValue:textField.text forKey:@"area"];

                }else{
                    NSLog(@"%@",textField.text);
                    [editedAddressDictionary setValue:textField.text forKey:@"state"];
                }
                
    }else{
    switch (textField.tag) {
        case 0:
            [ProfileUpdatedDictionary setValue:[[textField.text componentsSeparatedByString:@" "] firstObject] forKey:@"firstName"];
            [ProfileUpdatedDictionary setValue:[[textField.text componentsSeparatedByString:@" "] lastObject] forKey:@"lastName"];
            break;
        case 1:
            [ProfileUpdatedDictionary setValue:textField.text forKey:@"mobileNumber"];
            break;
        case 2:
            [ProfileUpdatedDictionary setValue:@[@{@"alterNateContactNumber": textField.text }] forKey:@"consumerContact"];
            break;
        case 3:
            [ProfileUpdatedDictionary setValue:textField.text forKey:@"emailId"];
            break;
        case 4:
            [ProfileUpdatedDictionary setValue:textField.text forKey:@"dob"];
            break;
        case 5:
            [ProfileUpdatedDictionary setValue:textField.text forKey:@"gender"];
            break;
            
        default:
            break;
    }
    }
}


- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    switch (textField.tag) {
            
        case 0:
            [textField setKeyboardType:UIKeyboardTypeAlphabet];
            [textField setReturnKeyType:UIReturnKeyNext];
            
            break;
        case 1:
            [textField setKeyboardType:UIKeyboardTypePhonePad];
            [textField setReturnKeyType:UIReturnKeyNext];
            
            break;
        case 2:
            [textField setKeyboardType:UIKeyboardTypePhonePad];
            [textField setReturnKeyType:UIReturnKeyNext];
            
            break;
        case 3:
            [textField setKeyboardType:UIKeyboardTypeEmailAddress];
            [textField setReturnKeyType:UIReturnKeyDone];
            break;
        case 4:
            //            [textField setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
            //            [textField setReturnKeyType:UIReturnKeyNext];
        {
            [_datePickerUIView setHidden:false];
            [textField resignFirstResponder];
        }
            
            break;
        case 5:
            //            [textField setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
            //            [textField setReturnKeyType:UIReturnKeyDone];
        { selectedRowIndex = (int)textField.tag;
            selectedIndex = (int)textField.tag;
            [textField resignFirstResponder];
            
            CGRect superviewFrame= [_profileTableView rectForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:selectedRowIndex]];
            CGRect superViewFinalFrame =  [_profileTableView convertRect:superviewFrame toView:[_profileTableView superview]];
            popOverFrame = textField.frame;
            CGRect tempframe = popOverFrame;
            tempframe.origin.x = textField.center.x-200;
            tempframe.origin.y = superViewFinalFrame.origin.y+15;
            popOverFrame = tempframe;
            
            
            NSMutableArray *tempArray = [[NSMutableArray alloc]init];
            [tempArray addObject:@"Male"];
            [tempArray addObject:@"Female"];
            
            [self showPopOverControllerForArray:tempArray andFrame:CGRectZero];
        }
            break;
            
        default:
            NSLog(@"textField Not Recognised");
            break;
    }
    
}


#pragma mark - API Call
-(void)getAddressApiCallwithUserId:(NSString *)userID{
    //user/778/address
    NSString *serviceNameWithParam = [NSString stringWithFormat:@"user/%@/address",userID];
    
    [[APIList sharedAPIList] API_GetAllDataFromWS:serviceNameWithParam ShowLoader:YES showOverlay:YES completion:^(BOOL success, id responceData, NSMutableArray *share) {
        if (success) {
            NSDictionary * result =(NSDictionary *) responceData;
            if ([[result objectForKey:@"hasError"] isEqual:@(0)]) {
                [self GetArrayFromResponse:[[result valueForKey:@"data"] valueForKey:@"lstAddressResult"]];
            }
        }else{
            [AlertView showAlert:@"Some Error Occured." alertType:AJNotificationTypeGreen];
            
        }
    }];
}
-(void)updateUserProfile{
    
    [LoadingView showLoaderWithOverlay:YES];
    if([NetworkCheck isInternetOff]) {
        [AlertView showAlert:@"The Internet connection appears to be offline." alertType:AJNotificationTypeRed];
        [LoadingView dismissLoader];
        
    }else {
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSString *strPostUrl = [NSString stringWithFormat:@"%@UpdateConsumerProfile",BACKENDBASEURL];
        
        AFJSONRequestSerializer* serialiser = [AFJSONRequestSerializer serializer];
        //    serialiser.timeoutInterval = 15.0;
        manager.responseSerializer=[AFJSONResponseSerializer serializer];
        manager.requestSerializer = serialiser;
        manager.responseSerializer.acceptableStatusCodes = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(200, 403)];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [manager POST:strPostUrl parameters:ProfileUpdatedDictionary success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [LoadingView dismissLoader];
            
            NSMutableDictionary *dict = (NSMutableDictionary *)responseObject;
            
            [AlertView showAlert:[dict valueForKey:@"message"] alertType:AJNotificationTypeGreen];
            
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [LoadingView dismissLoader];
            [AlertView showAlert:[error localizedDescription] alertType:AJNotificationTypeRed];
            
        }];
    }
    
}
-(void)deleteAddressFromTheListWithAddressID:(NSString *)addressId{
    [LoadingView showLoaderWithOverlay:YES];
    if([NetworkCheck isInternetOff]) {
        [AlertView showAlert:@"The Internet connection appears to be offline." alertType:AJNotificationTypeRed];
        [LoadingView dismissLoader];
        
    }else {
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSString *strPostUrl = [NSString stringWithFormat:@"%@user/%@/address/DeleteAddress?AddressId=%@",BACKENDBASEURL,[APPDELEGATE getUserID],addressId];
        
        AFJSONRequestSerializer* serialiser = [AFJSONRequestSerializer serializer];
        //    serialiser.timeoutInterval = 15.0;
        manager.responseSerializer=[AFJSONResponseSerializer serializer];
        manager.requestSerializer = serialiser;
        manager.responseSerializer.acceptableStatusCodes = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(200, 403)];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [manager POST:strPostUrl parameters:ProfileUpdatedDictionary success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [LoadingView dismissLoader];
            
            NSMutableDictionary *dict = (NSMutableDictionary *)responseObject;
            [AlertView showAlert:[dict valueForKey:@"message"] alertType:AJNotificationTypeGreen];
            
            [self getAddressApiCallwithUserId:[APPDELEGATE getUserID]];
            
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [LoadingView dismissLoader];
            [AlertView showAlert:[error localizedDescription] alertType:AJNotificationTypeRed];
            
        }];
    }
    
}
- (IBAction)backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}
-(void)calladdAddressApi{
    //param
    [editedAddressDictionary setValue:[APPDELEGATE getUserID] forKey:@"userId"];
    
    NSString *strUrl = [NSString stringWithFormat:@"user/%@/address",[APPDELEGATE getUserID]];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *strPostUrl = [NSString stringWithFormat:@"%@%@",BACKENDBASEURL,strUrl];
    
    AFJSONRequestSerializer* serialiser = [AFJSONRequestSerializer serializer];
    //    serialiser.timeoutInterval = 15.0;
    manager.responseSerializer=[AFJSONResponseSerializer serializer];
    manager.requestSerializer = serialiser;
    manager.responseSerializer.acceptableStatusCodes = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(200, 403)];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [LoadingView showLoaderWithOverlay:YES];

    [manager POST:strPostUrl parameters:editedAddressDictionary success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [LoadingView dismissLoader];
        
        NSDictionary *dictResponce = (NSDictionary *)responseObject;
        
        if ([[dictResponce valueForKey:@"data"] count]>0) {
           
            [AlertView showAlert:[dictResponce valueForKey:@"message"] alertType:AJNotificationTypeGreen];
            
            [self addressAddedSuccess];
            
        }else{
            [AlertView showAlert:[dictResponce valueForKey:@"message"] alertType:AJNotificationTypeRed];
            
        }
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [LoadingView dismissLoader];
        [AlertView showAlert:[error localizedDescription] alertType:AJNotificationTypeRed];
        
    }];
}

#pragma mark - ParseResponse Method

- (void)GetArrayFromResponse:(NSMutableArray *)arrResponse {
     addressArray = [[NSMutableArray alloc]init];
    
    for (NSDictionary *dic in arrResponse) {
        if (![addressArray containsObject:dic]) {
            [addressArray addObject:dic];
        }
    }
    tempAddressArray = [[NSMutableArray alloc]initWithArray:addressArray];
    
    if (!(addressArray.count>0)) {
        _noDataFoundView.center = _profileTableView.center;
        [self.view addSubview:_noDataFoundView];
    }else{
    [_profileTableView reloadData];
    }
    
}

-(void)addressAddedSuccess{
    [self.saveButton setHidden:true];
    
    [addressArray removeObjectAtIndex:selectedIndex];
    [_profileTableView deleteSections:[NSIndexSet indexSetWithIndex:selectedIndex] withRowAnimation:UITableViewRowAnimationNone];
    isNewCell = false;
    [addressArray insertObject:[tempAddressArray objectAtIndex:selectedIndex] atIndex:selectedIndex];
    [_profileTableView insertSections:[NSIndexSet indexSetWithIndex:selectedIndex] withRowAnimation:UITableViewRowAnimationTop];
    [self getAddressApiCallwithUserId:[APPDELEGATE getUserID]];

}

#pragma mark - Popover Delegate
-(void) clickedFor:(NSString*)selection{
 
    [self dismissViewControllerAnimated:true completion:^{
        if ([selection isEqualToString:@"Edit"]) {
            NSLog(@"Edit here");
            [addressArray removeObjectAtIndex:selectedIndex];
            [_profileTableView deleteSections:[NSIndexSet indexSetWithIndex:selectedIndex] withRowAnimation:UITableViewRowAnimationNone];
           isNewCell = true;
            [addressArray insertObject:[tempAddressArray objectAtIndex:selectedIndex] atIndex:selectedIndex];
            
            [_profileTableView insertSections:[NSIndexSet indexSetWithIndex:selectedIndex] withRowAnimation:UITableViewRowAnimationTop];
            
        }else if ([selection isEqualToString:@"Male"]){
            [textFieldArray replaceObjectAtIndex:5 withObject: @"M"];
            [_profileTableView reloadData];
        }else if ([selection isEqualToString:@"Female"]){
            [textFieldArray replaceObjectAtIndex:5 withObject: @"F"];
            [_profileTableView reloadData];
        }
        else{
            NSLog(@"Delete here");
            [self deleteAddressFromTheListWithAddressID:[[addressArray objectAtIndex:selectedIndex] valueForKey:@"id"]];
        }
    }];
}
#pragma mark - PopOver method

-(void)showPopOverControllerForArray:(NSArray*)arrayList andFrame:(CGRect)frame{
    listVC = [[OAPopOverListVC alloc] initWithNibName:@"OAPopOverListVC" bundle:nil];
    listVC.arrayListData = arrayList;
    [listVC.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    listVC.preferredContentSize = CGSizeMake(self.view.frame.size.width/3, 30.0*arrayList.count);
    
    [listVC setDelegate:self];
    [listVC reloadtableView];
    listVC.modalPresentationStyle = UIModalPresentationPopover;
    
    UIPopoverPresentationController *popOverController = listVC .popoverPresentationController;
    //    popOverController.popoverContentSize = CGSizeMake(self.view.frame.size.width, 160);
    [popOverController setDelegate:self];
    
    popOverController.sourceView = self.view;
    popOverController.sourceRect = popOverFrame;
    popOverController.permittedArrowDirections = UIPopoverArrowDirectionAny;
    
    [self presentViewController:listVC
                       animated:YES
                     completion:nil];
}
- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller {
    return UIModalPresentationNone;
}
-(void)getDataForMyProfile{
    
    NSString *userId = [APPDELEGATE getUserID];
    NSString *serviceNameWithParam = [NSString stringWithFormat:@"ConsumerByUserId?UserId=%@",userId];
    
    [[APIList sharedAPIList] API_GetAllDataFromWS:serviceNameWithParam ShowLoader:YES showOverlay:YES completion:^(BOOL success, id responceData, NSMutableArray *share) {
        NSDictionary * result =(NSDictionary *) responceData;
        
        if (success) {
            if ([[result objectForKey:@"hasError"] isEqual:@(0)]) {
                [self GetProfileArrayFromResponse:[result valueForKey:@"data"]];
            }
        }else{
            [AlertView showAlert:[result valueForKey:@"message"] alertType:AJNotificationTypeGreen];
        }
    }];
}



#pragma mark - ParseResponse Method

- (void)GetProfileArrayFromResponse:(NSMutableDictionary *)dictResponse {
    
    _myProfiledata = [[NSMutableDictionary alloc]init];
    _myProfiledata = dictResponse;
    ProfileUpdatedDictionary = [[NSMutableDictionary alloc]init];

    titleLabelArray = [[NSMutableArray alloc]initWithObjects:@"Full Name",@"Mobile Number",@"Alternate Mobile Number",@"Email Id",@"Date of Birth",@"Gender", nil];
    
    IconNameArray = [[NSMutableArray alloc]initWithObjects:@"UserName",@"profileMobile",@"profileMobile",@"profileEmail",@"profileCalender",@"profileGender",nil];
    
    NSString *selectedDate = [[[[[_myProfiledata valueForKey:@"customers"] firstObject] valueForKey:@"dob"] componentsSeparatedByString:@"T"] firstObject];
    
    selectedDate = [NSString stringWithFormat:@"%@-%@-%@",[[selectedDate componentsSeparatedByString:@"-"] lastObject],[[selectedDate componentsSeparatedByString:@"-"] objectAtIndex:1],[[selectedDate componentsSeparatedByString:@"-"] firstObject]];
    
    textFieldArray = [[NSMutableArray alloc]initWithObjects:[NSString stringWithFormat:@"%@ %@",[[_myProfiledata valueForKey:@"firstName"] length]>0?[_myProfiledata valueForKey:@"firstName"]:@"",[[_myProfiledata valueForKey:@"lastName"] length]>0?[_myProfiledata valueForKey:@"lastName"]:@""],   //Full Name
                      [[_myProfiledata valueForKey:@"mobileNumber"] length]>0?[_myProfiledata valueForKey:@"mobileNumber"]:@"",    //Mobile Number
                      [[[[_myProfiledata valueForKey:@"ConsumerContacts"] lastObject] valueForKey:@"alterNateContactNumber"] length]>0?[[[_myProfiledata valueForKey:@"ConsumerContacts"] lastObject] valueForKey:@"alterNateContactNumber"]:@"",    //Alternate Number
                      [[_myProfiledata valueForKey:@"emailId"] length]>0?[_myProfiledata valueForKey:@"emailId"]:@"",    //Email id
                      selectedDate,    //DOB
                      [[[[_myProfiledata valueForKey:@"customers"] firstObject] valueForKey:@"gender"] length]>0?[[[_myProfiledata valueForKey:@"customers"] firstObject] valueForKey:@"gender"]:@"",    //Gender
                      nil];
    
    [ProfileUpdatedDictionary setValue:[APPDELEGATE getUserID] forKey:@"userId"];
    [ProfileUpdatedDictionary setValue:[[_myProfiledata valueForKey:@"firstName"] length]>0?[_myProfiledata valueForKey:@"firstName"]:@"" forKey:@"firstName"];
    [ProfileUpdatedDictionary setValue:[[_myProfiledata valueForKey:@"lastName"] length]>0?[_myProfiledata valueForKey:@"lastName"]:@"" forKey:@"lastName"];
    
    [ProfileUpdatedDictionary setValue:[[_myProfiledata valueForKey:@"emailId"] length]>0?[_myProfiledata valueForKey:@"emailId"]:@"" forKey:@"emailId"];
    
    [ProfileUpdatedDictionary setValue:[[[[_myProfiledata valueForKey:@"customers"] firstObject] valueForKey:@"dob"] length]>0?[[[_myProfiledata valueForKey:@"customers"] firstObject] valueForKey:@"dob"]:@"" forKey:@"dob"];
    
    [ProfileUpdatedDictionary setValue:[[[[_myProfiledata valueForKey:@"customers"] firstObject] valueForKey:@"gender"] length]>0?[[[_myProfiledata valueForKey:@"customers"] firstObject] valueForKey:@"gender"]:@"" forKey:@"gender"];
    
    [ProfileUpdatedDictionary setValue:[[_myProfiledata valueForKey:@"mobileNumber"] length]>0?[_myProfiledata valueForKey:@"mobileNumber"]:@"" forKey:@"mobileNumber"];
    
    [ProfileUpdatedDictionary setValue:@[@{@"alterNateContactNumber": [[[[_myProfiledata valueForKey:@"ConsumerContacts"] lastObject] valueForKey:@"alterNateContactNumber"] length]>0?[[[_myProfiledata valueForKey:@"ConsumerContacts"] lastObject] valueForKey:@"alterNateContactNumber"]:@"" }] forKey:@"ConsumerContacts"];

    [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@",[[_myProfiledata valueForKey:@"firstName"] length]>0?[_myProfiledata valueForKey:@"firstName"]:@""] forKey:@"userName"];
    
    [_profileTableView reloadData];
}

#pragma mark -

- (IBAction)saveButton_Tapped:(id)sender {
    if (_fromScreen==AddressScreen || _fromScreen==TicketScreen) {
        [self calladdAddressApi];
    
    }else{
        [[NSUserDefaults standardUserDefaults] setValue:[ProfileUpdatedDictionary valueForKey:@"firstName"] forKey:@"userName"];
        [self updateUserProfile];
    }
 
}
- (IBAction)datePIcker_DONE_Tapped:(id)sender {
    [_datePickerUIView setHidden:true];
    
    [textFieldArray replaceObjectAtIndex:4 withObject: [[Utility sharedInstance] convertDate:_datePicker.date toDateFormat:@"dd/MM/yyyy"]];
    
    [_profileTableView reloadData];
}
- (IBAction)datePicker_CANCEL_Tapped:(id)sender {
    [_datePickerUIView setHidden:true];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
