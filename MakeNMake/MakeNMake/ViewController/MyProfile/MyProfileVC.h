//
//  MyProfileVC.h
//  MakeNMake
//
//  Created by Archit Saxena on 01/05/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OAPopOverListVC.h"

@interface MyProfileVC : UIViewController<UITableViewDelegate, UITableViewDataSource,OAPopOverListVCDelegate,UITextFieldDelegate>
typedef enum{
    ProfileScreen,
    AddressScreen,
    TicketScreen
    
} isFromScreen;

@property (strong, nonatomic) IBOutlet UIView *noDataFoundView;
@property (weak, nonatomic) IBOutlet UITableView *profileTableView;
@property(assign) isFromScreen fromScreen;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (strong, nonatomic) NSMutableDictionary *myProfiledata;
@property (strong, nonatomic) IBOutlet UIView *datePickerUIView;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
    @property (weak, nonatomic) IBOutlet UIView *customNavigationBar;

@end
