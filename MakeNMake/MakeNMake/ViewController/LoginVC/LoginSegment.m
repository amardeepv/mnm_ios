//
//  LoginSegment.m
//  MakeNMake
//
//  Created by Archit Saxena on 06/05/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "LoginSegment.h"

@interface LoginSegment ()

@end

@implementation LoginSegment

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setupPageView];
    
}
- (BOOL)prefersStatusBarHidden {
    return YES;
}
-(void)viewWillLayoutSubviews{
    
    CGFloat totalToolBarWidthForOneItem = _targetView.frame.size.width/2;
    
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:totalToolBarWidthForOneItem forSegmentAtIndex:0];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:totalToolBarWidthForOneItem forSegmentAtIndex:1];
    
    [carbonTabSwipeNavigation setTabExtraWidth:1];
}
-(void)setupPageView{
    items = @[@"SIGN IN",
              @"SIGN UP",
              ];
    
    carbonTabSwipeNavigation =
    [[CarbonTabSwipeNavigation alloc] initWithItems:items toolBar:self.toolbarView delegate:self];
    [carbonTabSwipeNavigation insertIntoRootViewController:self andTargetView:self.targetView];
    
    [self style];
}
- (void)style {
    
    UIColor *color = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.barTintColor = color;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    
    CGFloat totalToolBarWidthForOneItem = _targetView.frame.size.width/2;
    
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:totalToolBarWidthForOneItem forSegmentAtIndex:0];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:totalToolBarWidthForOneItem forSegmentAtIndex:1];
    
    [carbonTabSwipeNavigation setTabExtraWidth:1];
    
    carbonTabSwipeNavigation.toolbar.translucent = NO;
    carbonTabSwipeNavigation.toolbar.backgroundColor = [UIColor clearColor];
    carbonTabSwipeNavigation.carbonSegmentedControl.backgroundColor = [UIColor clearColor];
    carbonTabSwipeNavigation.carbonSegmentedControl.indicatorPosition = IndicatorPositionBottom;
    [carbonTabSwipeNavigation setIndicatorColor:[UIColor colorWithRed:129.0f/255 green:6.0f/255 blue:127.0f/255 alpha:1]];//81067F
    [carbonTabSwipeNavigation setNormalColor:[color colorWithAlphaComponent:0.5]
                                        font:[UIFont boldSystemFontOfSize:14]];
    [carbonTabSwipeNavigation setSelectedColor:color font:[UIFont boldSystemFontOfSize:14]];
}
#pragma mark - CarbonTabSwipeNavigation Delegate
- (nonnull UIViewController *)carbonTabSwipeNavigation:
(nonnull CarbonTabSwipeNavigation *)carbontTabSwipeNavigation
                                 viewControllerAtIndex:(NSUInteger)index {
    
    
if (index==0) {

    LoginViewController *loginObj = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoginViewController"];
        

        return loginObj;
    }else{

        
        RegisterViewController *regController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"RegisterViewController"];
        

        return regController;
    }
    
}
- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                 willMoveAtIndex:(NSUInteger)index {
    // NSLog(@"Will move at index: %ld", index);
    
}

- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                  didMoveAtIndex:(NSUInteger)index {
}
- (IBAction)backButton_Tapped:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:true];
}

- (UIBarPosition)barPositionForCarbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation {
    return UIBarPositionTop; //
}


#pragma mark - Api Calling

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
