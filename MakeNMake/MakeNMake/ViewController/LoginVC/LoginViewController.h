//
//  LoginViewController.h
//  MakeNMake
//
//  Created by Chirag Patel on 2/8/17.
//  Copyright © 2017 Nripendra  singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CarbonKit/CarbonKit.h>
#import "GenericTextField.h"
#import <Google/SignIn.h>

@interface LoginViewController : UIViewController/*CarbonTabSwipeNavigationDelegate> {
    CarbonTabSwipeNavigation *carbonTabSwipeNavigation;
    NSArray *items;
}*/
@property(weak, nonatomic) IBOutlet GIDSignInButton *signInButton;
@property (weak, nonatomic) IBOutlet UIImageView *backImage;
@property (weak, nonatomic) IBOutlet GenericTextField *txtUserName;
@property (weak, nonatomic) IBOutlet UIView *targetView;
@property (weak, nonatomic) IBOutlet GenericTextField *txtPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnFaceBook;
@property (weak, nonatomic) IBOutlet UIButton *btnSignUp;
@property (weak, nonatomic) IBOutlet UIButton *btnGmail;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
- (IBAction)btnFaceBookPressed:(id)sender;
- (IBAction)btnGmailPressed:(id)sender;
- (IBAction)btnSignUpPressed:(id)sender;
- (IBAction)btnLoginPressed:(id)sender;

@end
