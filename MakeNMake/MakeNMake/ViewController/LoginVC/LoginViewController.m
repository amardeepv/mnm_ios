//
//  LoginViewController.m
//  MakeNMake
//
//  Created by Chirag Patel on 2/8/17.
//  Copyright © 2017 Nripendra  singh. All rights reserved.
//

#import "LoginViewController.h"
#import "RegisterViewController.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "AFHTTPRequestOperationManager.h"
#import "Reachability.h"
#import "NetworkCheck.h"
#import "MBProgressHUD.h"
#import "Constant.h"
#define KFBLogin @"FaceBook"
#define KGoogleLogin @"Google"
#import "AppDelegate.h"
#import "AlertView.h"
#import "LoadingView.h"
#import "APIList.h"
#import "Utility.h"
#import "VerifyMobileNumberVC.h"

static NSString * const kClientId = @"385097562119-a2o1udc08fd6cku9iuecjq5qdn60pj12.apps.googleusercontent.com";

@interface LoginViewController ()<GIDSignInUIDelegate , GoogleSignInCustomProtocol> {
    MBProgressHUD *hud;
}

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [GIDSignIn sharedInstance].uiDelegate = self;

    APPDELEGATE.GoogleCustomDelegate = self;
    // Do any additional setup after loading the view.
    [self.txtUserName resignFirstResponder];
    [APPDELEGATE setIsOtpVerified:false];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:true];
    self.navigationController.navigationBarHidden = YES;

    if ([APPDELEGATE isOtpVerified]) {
        [self.navigationController popViewControllerAnimated:true];
        
    }
}
#pragma mark -
- (void)signIn:(GIDSignIn *)signIn
presentViewController:(UIViewController *)viewController
{
    [self presentViewController:viewController animated:YES completion:nil];
}

// Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn
dismissViewController:(UIViewController *)viewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)googleSignInUserWithDict:(NSMutableDictionary *)dict{
    
    //Gmail Sign in
    
    [self commonLoginWithCredenatils:dict withMethod:@"Gmail"];
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnFaceBookPressed:(id)sender {
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    appDel.loginType = KFBLogin;
    [self faceBookloginButtonClicked];

    }

-(void)faceBookloginButtonClicked { 
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    NSArray *permissionsNeeded = @[@"public_profile", @"user_birthday", @"user_photos", @"email", @"user_about_me"];
    [login logInWithReadPermissions: permissionsNeeded
                 fromViewController:self
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                if (error) {
                                    NSLog(@"Process error");
                                } else if (result.isCancelled) {
                                    NSLog(@"Cancelled");
                                } else {
                                    NSLog(@"Logged in"); {
                                    

                                        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"email, first_name, last_name"}] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                                            if (!error) {
                                                NSDictionary *fbDetaiDict = [[NSDictionary alloc]initWithDictionary:result];
                                                NSMutableDictionary *diction = [NSMutableDictionary dictionary];
                                                [diction setValue:[fbDetaiDict objectForKey:@"first_name"] forKey:@"firstName"];
                                                [diction setValue:[fbDetaiDict objectForKey:@"email"] forKey:@"email"];

                                                [diction setValue:[fbDetaiDict objectForKey:@"last_name"] forKey:@"LastName"];
                                                [diction setValue:[fbDetaiDict objectForKey:@"id"] forKey:@"SocialId"];
                                                [diction setValue:@"1" forKey:@"Provider"];
                                                
                                                NSString *strFBToken = [[FBSDKAccessToken currentAccessToken] tokenString];
                                                 [diction setValue:strFBToken forKey:@"AccessToken"];
                                    
                                                [self commonLoginWithCredenatils:diction withMethod:@"Facebook"];
                                            }else {
                                    
                                            }
                                        }];

                                    }
                                }
                            }];
}


-(void)commonLoginWithCredenatils :(NSMutableDictionary*)dic withMethod:(NSString*)endPointUrl {
    if ([endPointUrl isEqualToString:@"Facebook"]) {
//hit api for social register
    
        [self SocialRegisterWithDict:dic];
    }else if ([endPointUrl isEqualToString:@"Gmail"]) {
        [self SocialRegisterWithDict:dic];

    }else {
        
    }
}
- (IBAction)backButtonTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:true];

}
-(void)SocialRegisterWithDict:(NSMutableDictionary *)dict{
    [LoadingView showLoaderWithOverlay:YES];
    if([NetworkCheck isInternetOff]) {
        [AlertView showAlert:@"The Internet connection appears to be offline." alertType:AJNotificationTypeRed];
        [LoadingView dismissLoader];
        
    }else {
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSString *strPostUrl = [NSString stringWithFormat:@"%@Account/RegisterSocial/Mobile",BACKENDBASEURL];
        
        AFJSONRequestSerializer* serialiser = [AFJSONRequestSerializer serializer];
        //    serialiser.timeoutInterval = 15.0;
        manager.responseSerializer=[AFJSONResponseSerializer serializer];
        manager.requestSerializer = serialiser;
        manager.responseSerializer.acceptableStatusCodes = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(200, 403)];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [manager POST:strPostUrl parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            if ([[[responseObject valueForKey:@"data"] valueForKey:@"status"] isEqual:[NSNumber numberWithInt:3]])     {
                //show otp screen and verify number
                [LoadingView dismissLoader];

                NSString*  strUserId = [NSString stringWithFormat:@"%@",[[responseObject valueForKey:@"data"] valueForKey:@"userId"]];
                OTPController *controller = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"OTPController"];
                controller.userId = strUserId;
                controller.mobileNumber = [[[responseObject valueForKey:@"data"] valueForKey:@"access"] valueForKey:@"userName"];
                [self.navigationController pushViewController:controller animated:true];
                
            }else if ([[[responseObject valueForKey:@"data"] valueForKey:@"status"] isEqual:[NSNumber numberWithInt:2]]){
                //show mobile number screen and then verify Otp
                //ask mobile number screen
                [LoadingView dismissLoader];
                
                VerifyMobileNumberVC *obj = [[VerifyMobileNumberVC alloc]initWithNibName:@"VerifyMobileNumberVC" bundle:nil];
                [obj setIsFromScreen:AddMobile];

                obj.userId = [[responseObject valueForKey:@"data"] valueForKey:@"userId"];
                [self.navigationController pushViewController:obj animated:true];
                
                
            }
    else if ([[[responseObject valueForKey:@"data"] valueForKey:@"status"] isEqual:[NSNumber numberWithInt:1]] || [[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"message"]] containsString:@"is already taken"]){
                //LoggedIn Successfully
                [LoadingView dismissLoader];

                [[Utility sharedInstance] archiveFileWithFileName:OA_LOGIN_FILE andData:[responseObject valueForKey:@"data"]];
                [[NSUserDefaults standardUserDefaults] setValue:[[responseObject valueForKey:@"data"] valueForKey:@"userId"] forKey:UserId];
                [self.navigationController popViewControllerAnimated:true];
                [AlertView showAlert:@"User LoggedIn Successfully" alertType:AJNotificationTypeGreen];
                
            }else{
                //do nothing
                [LoadingView dismissLoader];

                [AlertView showAlert:@"Some Error Occured.Please try again" alertType:AJNotificationTypeRed];
            }
            
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [LoadingView dismissLoader];
            [AlertView showAlert:[error localizedDescription] alertType:AJNotificationTypeRed];
            
        }];
    }

}
- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
 
    
    
}

- (IBAction)btnGmailPressed:(id)sender {
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    appDel.loginType = KGoogleLogin;
    [[GIDSignIn sharedInstance] setClientID:kClientId];
    [[GIDSignIn sharedInstance] signIn];

}

- (IBAction)btnSignUpPressed:(id)sender {
    RegisterViewController*regController = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterViewController"];
    [self.navigationController pushViewController:regController animated:YES];
    

}
- (IBAction)btnLoginPressed:(id)sender {
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
  
    [parameters setValue:self.txtUserName.text forKey:@"userName"];
    [parameters setValue:self.txtPassword.text forKey:@"password"];
    
   [self LoginWebserviceCallMethodWith:parameters];
    
   // [APPDELEGATE setupSlideBar];
    
}
- (IBAction)troubleButton_Tapped:(id)sender {
    VerifyMobileNumberVC *obj = [[VerifyMobileNumberVC alloc]initWithNibName:@"VerifyMobileNumberVC" bundle:nil];
    obj.userId = [APPDELEGATE getUserID];
    [obj setIsFromScreen:ChangePassword];
    [self.navigationController pushViewController:obj animated:true];
    
}

#pragma arguments
#pragma mark - Call Login webservice Method


- (void)LoginWebserviceCallMethodWith:(NSDictionary *)diction {
    [LoadingView showLoaderWithOverlay:YES];
    if([NetworkCheck isInternetOff]) {
        [AlertView showAlert:@"The Internet connection appears to be offline." alertType:AJNotificationTypeRed];
        [LoadingView dismissLoader];

    }else {
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSString *strPostUrl = [NSString stringWithFormat:@"%@%@",BACKENDBASEURL,LOGIN];
        
        AFJSONRequestSerializer* serialiser = [AFJSONRequestSerializer serializer];
        //    serialiser.timeoutInterval = 15.0;
        manager.responseSerializer=[AFJSONResponseSerializer serializer];
        manager.requestSerializer = serialiser;
        manager.responseSerializer.acceptableStatusCodes = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(200, 403)];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
 
        [manager POST:strPostUrl parameters:diction success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [LoadingView dismissLoader];
           
    if ([[[responseObject valueForKey:@"data"] valueForKey:@"accountStatus"] isEqual:[NSNumber numberWithInt:3]])     {
                //show otp screen and verify number

                NSString*  strUserId = [NSString stringWithFormat:@"%@",[[responseObject valueForKey:@"data"] valueForKey:@"userId"]];
                OTPController *controller = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"OTPController"];
                controller.userId = strUserId;
        [APPDELEGATE setUserID:strUserId];
                controller.mobileNumber = [[[responseObject valueForKey:@"data"] valueForKey:@"access"] valueForKey:@"userName"];
                [self.navigationController pushViewController:controller animated:true];
                
    }else if ([[[responseObject valueForKey:@"data"] valueForKey:@"accountStatus"] isEqual:[NSNumber numberWithInt:2]]){
                //show mobile number screen and then verify Otp
                
                
    }else if ([[[responseObject valueForKey:@"data"] valueForKey:@"accountStatus"] isEqual:[NSNumber numberWithInt:1]]){
                //LoggedIn Successfully
          
                [[Utility sharedInstance] archiveFileWithFileName:OA_LOGIN_FILE andData:[responseObject valueForKey:@"data"]];
                [[NSUserDefaults standardUserDefaults] setValue:[[responseObject valueForKey:@"data"] valueForKey:@"userId"] forKey:UserId];
       
            [APPDELEGATE setUserID:[[responseObject valueForKey:@"data"] valueForKey:@"userId"]];
//
            [[NSUserDefaults standardUserDefaults] setValue:[[responseObject valueForKey:@"data"] valueForKey:@"name"] forKey:@"userName"];

                [self.navigationController popViewControllerAnimated:true];
                [AlertView showAlert:[NSString stringWithFormat:@"Welcome %@",[[responseObject valueForKey:@"data"] valueForKey:@"name"]] alertType:AJNotificationTypeGreen];
            
    }else{
                //do nothing
        [AlertView showAlert:[responseObject valueForKey:@"message"] alertType:AJNotificationTypeRed];
        
            }
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [LoadingView dismissLoader];
            [AlertView showAlert:[error localizedDescription] alertType:AJNotificationTypeRed];

        }];
    }
}
#pragma mark - UITextFieldDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    
    if (textField.tag==111) {
    
        NSString *tempString = [NSString stringWithFormat:@"%@%@",textField.text,string];
        
        if (tempString.length>10) {
            return NO;
    
        }else{
            return YES;

        }
    }else{
        return YES;

    }
    
}

- (void)postLoginApiCallMethod:(NSMutableDictionary*)dict {
    
    [[APIList sharedAPIList] API_sendLoginDetails:dict ShowLoader:YES showOverlay:YES completion:^(BOOL success, id responceData, NSMutableArray *share) {
        if (success) {
            [APPDELEGATE setupSlideBar];
        }

    }];
    
}

@end
