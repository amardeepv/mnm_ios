//
//  LoginSegment.h
//  MakeNMake
//
//  Created by Archit Saxena on 06/05/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CarbonKit/CarbonKit.h>
#import "RegisterViewController.h"
#import "LoginViewController.h"

typedef enum {
    loginScreen,
    RegisterScreen
    
}LoginState;

@interface LoginSegment : UIViewController<CarbonTabSwipeNavigationDelegate>{
    CarbonTabSwipeNavigation *carbonTabSwipeNavigation;
    NSArray *items;
}
@property (assign, nonatomic)  int *selectedIndex;

@property (assign, nonatomic) LoginState *loginState;
@property (weak, nonatomic) IBOutlet UIView *targetView;
@property (weak, nonatomic) IBOutlet UIToolbar *toolbarView;

@end
