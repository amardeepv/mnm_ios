//
//  ServicesViewController.m
//  MakeNMake
//
//  Created by Chirag Patel on 3/16/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "ServicesViewController.h"
#import "ServicesCustomTableCell.h"
#import "APIList.h"
#import "AlertView.h"

@interface ServicesViewController ()
{
    NSMutableArray *rateTypeArray,*serviceNameArray,*serviceDescArray, *priceArray, *serviceIdArray , *notesArray;
    UILabel *noDataFound;
    
}
@end

@implementation ServicesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self configView];
    [self GetServiceCategoryApiCallMethod];
    
}

-(void)configView{
    
    UINib* nibProfile = [UINib nibWithNibName:@"ServicesCustomTableCell" bundle:nil];
    [_tblService registerNib:nibProfile forCellReuseIdentifier:@"ServicesCustomTableCell"];
    
    noDataFound = [[UILabel alloc]initWithFrame:CGRectMake(self.view.center.x-60, self.view.center.y-100, 120, 30)];
    [noDataFound setTextAlignment:NSTextAlignmentCenter];
    [noDataFound setText:@"No data found."];
    [self.view addSubview:noDataFound];
    noDataFound.hidden = true;
    _tblService.estimatedRowHeight = 80;
    self.tblService.tableFooterView = [UIView new];
    
    
}

#pragma mark - GetAllServices Method

- (void)GetServiceCategoryApiCallMethod {

    NSString *serviceNameWithParam = [NSString stringWithFormat:@"Services?parentId=%@&serviceGroupId=%@",_serviceParentId,_serviceGroupId];
    
    [[APIList sharedAPIList] API_GetAllDataFromWS:serviceNameWithParam ShowLoader:YES showOverlay:YES completion:^(BOOL success, id responceData, NSMutableArray *share) {
        if (success) {
            NSDictionary * result =(NSDictionary *) responceData;
            if ([[result objectForKey:@"hasError"] isEqual:@(0)]) {
                [self GetArrayFromResponse:[result valueForKey:@"data"]];
            }
        }
    }];
}


#pragma mark - ParseResponse Method

- (void)GetArrayFromResponse:(NSMutableArray *)arrResponse {
    
    if (arrResponse.count>0) {
        _tblService.hidden = false;

        g_AppDelegate.strtBookingServiceDetail = [NSMutableArray array];
        rateTypeArray = [[NSMutableArray alloc]init];
        serviceNameArray = [[NSMutableArray alloc]init];
        serviceDescArray = [[NSMutableArray alloc]init];
        priceArray = [[NSMutableArray alloc]init];
        serviceIdArray = [[NSMutableArray alloc]init];
        notesArray = [[NSMutableArray alloc] init];
        
        //servicename
        //desc
        //    "rateType": "Base Rate(1st hour)",
        
        for (NSDictionary *dictionResult in arrResponse) {
            [g_AppDelegate.strtBookingServiceDetail addObject:dictionResult];
            [serviceDescArray addObject:[dictionResult valueForKey:@"desc"]];
            [serviceNameArray addObject:[dictionResult valueForKey:@"servicename"]];
            [rateTypeArray addObject:[dictionResult valueForKey:@"rateType"]];
            [priceArray addObject:[dictionResult valueForKey:@"rate"]];
            [serviceIdArray addObject:[dictionResult valueForKey:@"serviceid"]];
            [notesArray addObject:[dictionResult valueForKey:@"Notes"]];
        }
        
        for (int i=0; i<_masterServiceArray.count; i++) {
            NSDictionary *dict = [[NSDictionary alloc] initWithDictionary:[_masterServiceArray objectAtIndex:i]];
            for (int j=0; j<serviceIdArray.count; j++) {
                if ([[NSString stringWithFormat:@"%@",[serviceIdArray objectAtIndex:j]] isEqual:[NSString stringWithFormat:@"%@",[dict valueForKey:@"serviceId"]]] ) {
                    [priceArray insertObject:[NSNumber numberWithInt:0] atIndex:j];
                    [priceArray removeObjectAtIndex:j+1];
                    }
                }
            }
    
        [self.tblService reloadData];
    }else{
        noDataFound.hidden = false;
        _tblService.hidden = true;
    }
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}
#pragma arguments
#pragma mark -UITableview Delegate & Datasource Method

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ServicesCustomTableCell *cell = (ServicesCustomTableCell*)[_tblService dequeueReusableCellWithIdentifier:@"ServicesCustomTableCell"];

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell.infoButtonTapped setTag:indexPath.row];
    [cell.infoButtonTapped addTarget:self action:@selector(infoButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    cell.serviceTitle.text = [serviceNameArray objectAtIndex:indexPath.row];
    cell.serviceDesc.text = [serviceDescArray objectAtIndex:indexPath.row];
    cell.serviceType = [rateTypeArray objectAtIndex:indexPath.row];
    cell.subServiceName = [serviceNameArray objectAtIndex:indexPath.row];
    cell.serviceId = [serviceIdArray objectAtIndex:indexPath.row];
    cell.servicePrice = [priceArray objectAtIndex:indexPath.row];
    cell.parentServiceName = _parentServiceName;
    cell.childServiceName = _childServiceName;
    
    [cell configCell];
    
    return cell;
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  serviceNameArray.count;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
   // NSLog(@"Selected service Name - %@, service Price - %@",_subServiceName,[priceArray objectAtIndex:indexPath.row]);
}

-(void)infoButtonTapped:(UIButton *)sender{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Info" message:[notesArray objectAtIndex:sender.tag] preferredStyle:UIAlertControllerStyleAlert];
   
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                         {
                             [self dismissViewControllerAnimated:true completion:nil];
                         }];

    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
    
}


@end
