//
//  AllServiceViewController.m
//  MakeNMake
//
//  Created by Chirag Patel on 3/21/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "AllServiceViewController.h"
#import "ServicesViewController.h"
#import "Constant.h"
#import "ScheduleViewController.h"
#import "AlertView.h"


@interface AllServiceViewController ()
{
    UILabel *noDataFoundView;
}
@end

@implementation AllServiceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  
   noDataFoundView = [[UILabel alloc]initWithFrame:CGRectMake(0, self.view.center.y, self.view.frame.size.width, 30)];
    [noDataFoundView setText:@"No Data Found. Please try again later."];
    [noDataFoundView setTextAlignment:NSTextAlignmentCenter];
    [self.view addSubview:noDataFoundView];
    noDataFoundView.hidden = true;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateBadgeCount:) name:@"UpdateBadgeCount" object:nil];
    
    [self GetServiceCategoryApiCallMethod];
    
    [_badgeLabel.layer setCornerRadius:_badgeLabel.frame.size.width/2];
    [_badgeLabel.layer setMasksToBounds:true];

    
    // Do any additional setup after loading the view.
}
#pragma mark - GetAllServices Method

- (void)GetServiceCategoryApiCallMethod {
    
    NSString *serviceNameWithParam = [NSString stringWithFormat:@"ServiceGroup/ServiceGroups?serviceGroupId=%@",_serviceGroupId];
    
    [[APIList sharedAPIList] API_GetAllDataFromWS:serviceNameWithParam ShowLoader:YES showOverlay:YES completion:^(BOOL success, id responceData, NSMutableArray *share) {
        if (success) {
            NSDictionary * result =(NSDictionary *) responceData;
            if ([[result objectForKey:@"hasError"] isEqual:@(0)]) {
                [self GetArrayFromResponse:[result valueForKey:@"data"]];
            }else{
                [AlertView showAlert:@"Some error occured" alertType:AJNotificationTypeRed];
            }
        }
    }];
}

-(void)getServiceCategoryForNoServiceFound{
    
    NSString *serviceNameWithParam = [NSString stringWithFormat:@"ServiceGroup/ServiceGroups?serviceGroupId=0"];
    
    [[APIList sharedAPIList] API_GetAllDataFromWS:serviceNameWithParam ShowLoader:YES showOverlay:YES completion:^(BOOL success, id responceData, NSMutableArray *share) {
        if (success) {
            NSDictionary * result =(NSDictionary *) responceData;
            if ([[result objectForKey:@"hasError"] isEqual:@(0)]) {
                [self GetArrayForNoServiceFromResponse:[result valueForKey:@"data"]];
            }else{
                [AlertView showAlert:@"Some error occured" alertType:AJNotificationTypeRed];
            }
        }
    }];
}

#pragma mark - ParseResponse Method
- (void)GetArrayForNoServiceFromResponse:(NSMutableArray *)arrResponse {
    g_AppDelegate.strtBookingServiceDetail = [NSMutableArray array];
    items = [[NSMutableArray alloc]init];
    serviceGroupId = [[NSMutableArray alloc]init];
    parentIdArray = [[NSMutableArray alloc]init];
    
    for (NSDictionary *dictionResult in arrResponse) {
        [g_AppDelegate.strtBookingServiceDetail addObject:dictionResult];
        [items addObject:[dictionResult valueForKey:@"serviceGroupName"]];
        [parentIdArray addObject:[dictionResult valueForKey:@"parentID"]];
        [serviceGroupId addObject:[dictionResult valueForKey:@"serviceGroupId"]];
        
    }
    
    if (items.count!=0) {
        [items removeObjectAtIndex:0];
        [parentIdArray removeObjectAtIndex:0];
        [serviceGroupId removeObjectAtIndex:0];
        
        if (items.count>0) {
            carbonTabSwipeNavigation =
            [[CarbonTabSwipeNavigation alloc] initWithItems:items toolBar:self.toolBar delegate:self];
            [carbonTabSwipeNavigation insertIntoRootViewController:self andTargetView:self.targetView];
            _titleLabel.text = _titleString;
            [self style];
        }else{
            noDataFoundView.hidden = false;
        }
        
        
    }
    
    NSLog(@"This is item array : %@",items);
    
    //[self.tblHome reloadData];
    
}
- (void)GetArrayFromResponse:(NSMutableArray *)arrResponse {
    g_AppDelegate.strtBookingServiceDetail = [NSMutableArray array];
    items = [[NSMutableArray alloc]init];
    serviceGroupId = [[NSMutableArray alloc]init];
    parentIdArray = [[NSMutableArray alloc]init];
    
    for (NSDictionary *dictionResult in arrResponse) {
        [g_AppDelegate.strtBookingServiceDetail addObject:dictionResult];
        [items addObject:[dictionResult valueForKey:@"serviceGroupName"]];
        [parentIdArray addObject:[dictionResult valueForKey:@"parentID"]];
        [serviceGroupId addObject:[dictionResult valueForKey:@"serviceGroupId"]];

    }
    
    if (items.count!=0) {
        [items removeObjectAtIndex:0];
        [parentIdArray removeObjectAtIndex:0];
        [serviceGroupId removeObjectAtIndex:0];
        
        if (items.count>0) {
            carbonTabSwipeNavigation =
            [[CarbonTabSwipeNavigation alloc] initWithItems:items toolBar:self.toolBar delegate:self];
            [carbonTabSwipeNavigation insertIntoRootViewController:self andTargetView:self.targetView];
            _titleLabel.text = _titleString;
            [self style];
        }else{
            noDataFoundView.hidden = false;
            [self getServiceCategoryForNoServiceFound];
        }
        

    }
    
    NSLog(@"This is item array : %@",items);
    
    //[self.tblHome reloadData];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)prefersStatusBarHidden {
    return YES;
}
- (void)style {
    
    UIColor *color = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor = color;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    
    carbonTabSwipeNavigation.toolbar.translucent = NO;
    carbonTabSwipeNavigation.toolbar.backgroundColor = [UIColor blackColor];
    carbonTabSwipeNavigation.carbonSegmentedControl.backgroundColor = [UIColor blackColor];
    carbonTabSwipeNavigation.carbonSegmentedControl.indicatorPosition = IndicatorPositionBottom;
    [carbonTabSwipeNavigation setIndicatorColor:[UIColor colorWithRed:129.0f/255 green:6.0f/255 blue:127.0f/255 alpha:1]];//81067F
    [carbonTabSwipeNavigation setTabExtraWidth:10];
    [carbonTabSwipeNavigation setNormalColor:[color colorWithAlphaComponent:0.7]
                                        font:[UIFont boldSystemFontOfSize:14]];
    [carbonTabSwipeNavigation setSelectedColor:color font:[UIFont boldSystemFontOfSize:14]];
}

#pragma mark - CarbonTabSwipeNavigation Delegate
- (nonnull UIViewController *)carbonTabSwipeNavigation:
(nonnull CarbonTabSwipeNavigation *)carbontTabSwipeNavigation
                                 viewControllerAtIndex:(NSUInteger)index {
    ServicesViewController *latestObj = [[UIStoryboard storyboardWithName:@"Second" bundle:nil] instantiateViewControllerWithIdentifier:@"ServicesViewController"];
    
    if (_isFrom==OpenTicket) {
        latestObj.isFrom = OpenTicketScreen;
        latestObj.masterServiceArray = _masterServiceArray;
    }else{
        latestObj.isFrom = HomeDetailScreen;

    }
    
    latestObj.serviceParentId = [parentIdArray objectAtIndex:index];
    latestObj.serviceGroupId = [serviceGroupId objectAtIndex:index];
    latestObj.subServiceName = [items objectAtIndex:index];
    latestObj.childServiceName = [items objectAtIndex:index];
    latestObj.parentServiceName = _titleString;
    
    return latestObj;
    
    
};
- (IBAction)checkoutButton_Tapped:(id)sender {
    
    if (APPDELEGATE.serviceArray.count>0) {
        
        ScheduleViewController *scheduleObj = [[ScheduleViewController alloc]initWithNibName:@"ScheduleViewController" bundle:nil];
        scheduleObj.mainServiceName = _titleString;
        scheduleObj.selectedAddressDict = _selectedAddress;
        scheduleObj.serviceGroupId = _serviceGroupId;
        [self.navigationController pushViewController:scheduleObj animated:true];
        
        
    }else{
        [AlertView showAlert:@"Please select atleast One service" alertType:AJNotificationTypeRed];

    }
    
}
- (IBAction)btnBackPressedMethod:(id)sender{
    [self.navigationController popViewControllerAnimated:true];
}

- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                 willMoveAtIndex:(NSUInteger)index {
//    NSLog(@"Will move at index: %ld", index);
    
}

- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                  didMoveAtIndex:(NSUInteger)index {
}

- (UIBarPosition)barPositionForCarbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation {
    return UIBarPositionTop; //
}
#pragma mark - Notification Observer
-(void)updateBadgeCount:(NSNotification*)notification{
    
    NSMutableArray *tempArray = notification.object;
    
    _badgeLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)tempArray.count];
    
    
    
}
@end
