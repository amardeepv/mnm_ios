//
//  ServicesViewController.h
//  MakeNMake
//
//  Created by Chirag Patel on 3/16/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

@interface ServicesViewController : UIViewController<UITableViewDataSource>

typedef enum {
    OpenTicketScreen=1,
    HomeDetailScreen
} isFromView;

@property (strong, nonatomic) NSMutableArray *masterServiceArray;

@property (nonatomic) isFromView isFrom;

@property (weak, nonatomic) IBOutlet UITableView *tblService;
@property(strong,nonatomic) NSString *serviceParentId,*serviceGroupId,*subServiceName , *parentServiceName ,*childServiceName;






@end
