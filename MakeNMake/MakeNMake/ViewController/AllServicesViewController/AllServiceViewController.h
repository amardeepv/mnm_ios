//
//  AllServiceViewController.h
//  MakeNMake
//
//  Created by Chirag Patel on 3/21/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CarbonKit/CarbonKit.h>
#import "APIList.h"

typedef enum {
    OpenTicket=1,
    HomeDetail
} isFromScreen;


@interface AllServiceViewController : UIViewController<CarbonTabSwipeNavigationDelegate> {
    CarbonTabSwipeNavigation *carbonTabSwipeNavigation;
    NSMutableArray *items, *parentIdArray , *serviceGroupId;
}
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property(weak, nonatomic) IBOutlet UIToolbar *toolBar;
@property(weak, nonatomic) IBOutlet UIView *targetView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) NSString *titleString;
@property (strong, nonatomic) NSString *serviceGroupId;
@property (weak, nonatomic) IBOutlet UILabel *badgeLabel;
@property (strong, nonatomic) NSMutableArray *masterServiceArray;

@property (strong, nonatomic) NSMutableDictionary *selectedAddress;


@property (nonatomic) isFromScreen isFrom;

- (IBAction)btnBackPressedMethod:(id)sender;
@end
