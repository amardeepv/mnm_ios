//
//  ReferAndEarnVC.m
//  MakeNMake
//
//  Created by Archit Saxena on 30/04/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "ReferAndEarnVC.h"
#import "APIList.h"
#import "AlertView.h"
#import "Constant.h"
#import "Utility.h"
#import "LoginSegment.h"

@interface ReferAndEarnVC ()
{
    CAShapeLayer *border;
    NSString *referralCode, *shareString;
}
@end

@implementation ReferAndEarnVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
   
}
-(void)viewWillAppear:(BOOL)animated{
        [super viewWillAppear:animated];
    
    if (![[Utility sharedInstance] unArchiveFileWithFileName:OA_LOGIN_FILE]) {
        LoginSegment  *loginObj = [[LoginSegment alloc]initWithNibName:@"LoginSegment" bundle:nil];
        loginObj.loginState = loginScreen;
        [self.navigationController pushViewController:loginObj animated:YES];
        
    }else{
        [self getDataForRefer];
        // Do any additional setup after loading the view from its nib.
    }
    }
#pragma mark - UIActivityViewController
-(void)showShareSheet{
    
    NSArray *objectsToShare = @[shareString];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];
}
- (IBAction)whatsappButton_Tapped:(id)sender {
   
    NSString * urlWhats = [NSString stringWithFormat:@"whatsapp://send?text=%@",shareString.length>0?shareString:@""];
    NSURL * whatsappURL = [NSURL URLWithString:[urlWhats stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
   
    
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
            [[UIApplication sharedApplication] openURL: whatsappURL];
        } else {
            [self showShareSheet];
    }
    
}

- (IBAction)GoogleButton_Tapped:(id)sender {
    [self showShareSheet];
}

- (IBAction)FbButton_Tapped:(id)sender {
    [self showShareSheet];
}

- (IBAction)OtherShareButton_Tapped:(id)sender {
    [self showShareSheet];
}

- (IBAction)backButton_Tapped:(id)sender {
    
    [self.navigationController popViewControllerAnimated:true];
}

#pragma mark - Api Calling

-(void)getDataForRefer{
    
    NSString *userId = [APPDELEGATE getUserID];
    NSString *serviceNameWithParam = [NSString stringWithFormat:@"GetReferralcode?UserId=%@",userId];
    
    [[APIList sharedAPIList] API_GetAllDataFromWS:serviceNameWithParam ShowLoader:YES showOverlay:YES completion:^(BOOL success, id responceData, NSMutableArray *share) {
        NSDictionary * result =(NSDictionary *) responceData;
        
        if (success) {
            if ([[result objectForKey:@"hasError"] isEqual:@(0)]) {
                [self GetArrayFromResponse:[result valueForKey:@"data"]];
            }
        }else{
            [AlertView showAlert:[result valueForKey:@"message"] alertType:AJNotificationTypeGreen];
        }
    }];
    
    
    
}
- (IBAction)shareButtonTapped:(id)sender {
    [self showShareSheet];

}
#pragma mark - ParseResponse Method

- (void)GetArrayFromResponse:(NSMutableDictionary *)dictResponse {
    
    shareString = [dictResponse valueForKey:@"referalMessage"];
    referralCode = [dictResponse valueForKey:@"referalCode"];
  
    [_referralCodeTextField setText:referralCode.length>0?referralCode:@"No Data Found"];
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
