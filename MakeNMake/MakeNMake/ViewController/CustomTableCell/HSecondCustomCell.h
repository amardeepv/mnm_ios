//
//  HSecondCustomCell.h
//  MakeNMake
//
//  Created by Nripendra on 13/03/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HSecondCustomCell : UITableViewCell// <UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webContentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webLayoutHeight;
@property (nonatomic,retain) NSString *strWebContent;

@end
