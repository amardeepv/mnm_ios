//
//  ViewHeader.h
//  Vyapar_Samachar
//
//  Created by Tops on 2/20/17.
//  Copyright © 2017 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewHeader : UIView
@property (weak, nonatomic) IBOutlet UILabel *lblSectionTitle;
@end
