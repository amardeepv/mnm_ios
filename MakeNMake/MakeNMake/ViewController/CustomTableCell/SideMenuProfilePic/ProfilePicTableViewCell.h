//
//  ProfilePicTableViewCell.h
//  MakeNMake
//
//  Created by archit.saxena on 21/03/17.
//  Copyright © 2017 Nripendra  singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfilePicTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *userName;

@end
