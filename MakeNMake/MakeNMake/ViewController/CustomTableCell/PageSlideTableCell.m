//
//  PageSlideTableCell.m
//  MakeNMake
//
//  Created by Chirag Patel on 3/13/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "PageSlideTableCell.h"

@implementation PageSlideTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
