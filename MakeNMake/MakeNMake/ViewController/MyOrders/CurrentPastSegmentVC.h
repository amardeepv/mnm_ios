//
//  CurrentPastSegmentVC.h
//  MakeNMake
//
//  Created by Archit Saxena on 25/04/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CarbonKit/CarbonKit.h>

@interface CurrentPastSegmentVC : UIViewController<CarbonTabSwipeNavigationDelegate> {
    CarbonTabSwipeNavigation *carbonTabSwipeNavigation;
    NSArray *items;
}

@property (weak, nonatomic) IBOutlet UIView *targetView;
@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;

@end
