//
//  OrderPlansVC.m
//  MakeNMake
//
//  Created by Archit Saxena on 28/04/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "OrderPlansVC.h"
#import "OrderPlanCustumCell.h"
#import "APIList.h"
#import "AlertView.h"
#import "Constant.h"

@interface OrderPlansVC ()
{
    NSMutableArray* planArray;
}
@end

@implementation OrderPlansVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self registerNibs];
    [self getDataForOrders];
    
    [_planTableView.layer setCornerRadius:8];
    [_planTableView.layer setMasksToBounds:true];
    [_planTableView.layer setShadowColor:[UIColor grayColor].CGColor];
    [_planTableView.layer setShadowRadius:5.0f];
    [_planTableView.layer setShadowOffset:CGSizeMake(0.0f, 0.0f)];
    [_planTableView.layer setMasksToBounds:false];
    [_planTableView.layer setShadowOpacity: 0.5];

    
    // Do any additional setup after loading the view from its nib.
}
#pragma mark - Custom Methods

-(void)registerNibs{
    
    UINib* nibAddress = [UINib nibWithNibName:@"OrderPlanCustumCell" bundle:nil];
    [_planTableView registerNib:nibAddress forCellReuseIdentifier:@"OrderPlanCustumCell"];
}


#pragma mark - UItable view dataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 1;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return planArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    OrderPlanCustumCell *cell = [_planTableView dequeueReusableCellWithIdentifier:@"OrderPlanCustumCell"];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell prepareForReuse];
    
    [cell configureCellWithDataDictionary:[planArray objectAtIndex:indexPath.section]];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
        return 175;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.001f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10.0f;
}
#pragma mark - Api Calling

-(void)getDataForOrders{
    
    NSString *userId = [APPDELEGATE getUserID];
    NSString *serviceNameWithParam;
    //    if (_fromScreen==CurrentScreen) {
    //        serviceNameWithParam = [NSString stringWithFormat:@"ServiceDetailsCurrentList?UserID=%@",userId];
    //
    //    }else{
    serviceNameWithParam = [NSString stringWithFormat:@"PlanDetailsList?UserID=%@",userId];
    
    //    }
    
    [[APIList sharedAPIList] API_GetAllDataFromWS:serviceNameWithParam ShowLoader:YES showOverlay:YES completion:^(BOOL success, id responceData, NSMutableArray *share) {
        NSDictionary * result =(NSDictionary *) responceData;
        
        if (success) {
            if ([[result objectForKey:@"hasError"] isEqual:@(0)]) {
                [self GetArrayFromResponse:[[result valueForKey:@"data"] valueForKey:@"PlanOrders"]];
            }
        }else{
            [AlertView showAlert:[result valueForKey:@"message"] alertType:AJNotificationTypeGreen];
            
        }
    }];
    
    
    
}
#pragma mark - ParseResponse Method

- (void)GetArrayFromResponse:(NSMutableArray *)arrResponse {
    
    planArray = [[NSMutableArray alloc]init];
    planArray = arrResponse;
    
    [_planTableView reloadData];
    [_planTableView sizeToFit];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
