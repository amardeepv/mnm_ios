//
//  ServicePlanSegmentVC.h
//  MakeNMake
//
//  Created by Archit Saxena on 25/04/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CarbonKit/CarbonKit.h>

@interface ServicePlanSegmentVC :  UIViewController<CarbonTabSwipeNavigationDelegate>{
    CarbonTabSwipeNavigation *carbonTabSwipeNavigation;
    NSArray *items;
}
@property (weak, nonatomic) IBOutlet UIView *targetView;
@property (weak, nonatomic) IBOutlet UIToolbar *toolbarView;
@property (weak, nonatomic) IBOutlet UIView *orderIconView;

@end
