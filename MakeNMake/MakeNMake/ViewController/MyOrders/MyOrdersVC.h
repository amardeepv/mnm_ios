//
//  MyOrdersVC.h
//  MakeNMake
//
//  Created by Archit Saxena on 25/04/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderCustomTableCell.h"

typedef enum{
    CurrentScreen,
    PastScreen,
} isFromScreen;

@interface MyOrdersVC : UIViewController<UITableViewDelegate,UITableViewDataSource,ReloadTableDelegate>

@property (weak, nonatomic) IBOutlet UITableView *orderTableView;
@property (nonatomic, retain) NSIndexPath * expandedCellIndexPath;

@property(assign) isFromScreen fromScreen;

@end
