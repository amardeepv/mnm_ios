//
//  ServicePlanSegmentVC.m
//  MakeNMake
//
//  Created by Archit Saxena on 25/04/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "ServicePlanSegmentVC.h"
#import "CurrentPastSegmentVC.h"
#import "OrderPlansVC.h"
#import "Utility.h"
#import "LoginSegment.h"
#import "Constant.h"

@interface ServicePlanSegmentVC ()

@end

@implementation ServicePlanSegmentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if (![[Utility sharedInstance] unArchiveFileWithFileName:OA_LOGIN_FILE]) {
        LoginSegment  *loginObj = [[LoginSegment alloc]initWithNibName:@"LoginSegment" bundle:nil];
        loginObj.loginState = loginScreen;
        [self.navigationController pushViewController:loginObj animated:YES];
        
    }else{
    
    [self setupPageView];
    [_orderIconView.layer setShadowColor:[UIColor whiteColor].CGColor];
    [_orderIconView.layer setShadowRadius:8.0f];
    [_orderIconView.layer setMasksToBounds:false];
    [_orderIconView.layer setShadowOpacity: 0.5];
    }

}
- (BOOL)prefersStatusBarHidden {
    return YES;
}
-(void)viewWillLayoutSubviews{
    
    CGFloat totalToolBarWidthForOneItem = _targetView.frame.size.width/2;
    
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:totalToolBarWidthForOneItem forSegmentAtIndex:0];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:totalToolBarWidthForOneItem forSegmentAtIndex:1];
    
    [carbonTabSwipeNavigation setTabExtraWidth:1];
}
-(void)setupPageView{
    items = @[@"SERVICES ",
              @"  PLANS  ",
              ];
    
    carbonTabSwipeNavigation =
    [[CarbonTabSwipeNavigation alloc] initWithItems:items toolBar:self.toolbarView delegate:self];
    [carbonTabSwipeNavigation insertIntoRootViewController:self andTargetView:self.targetView];
    
    [self style];
}
- (void)style {
    
    UIColor *color = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor = color;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    
    CGFloat totalToolBarWidthForOneItem = _targetView.frame.size.width/2;
    
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:totalToolBarWidthForOneItem forSegmentAtIndex:0];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:totalToolBarWidthForOneItem forSegmentAtIndex:1];
    
    [carbonTabSwipeNavigation setTabExtraWidth:1];
    
    carbonTabSwipeNavigation.toolbar.translucent = NO;
    carbonTabSwipeNavigation.toolbar.backgroundColor = [UIColor blackColor];
    carbonTabSwipeNavigation.carbonSegmentedControl.backgroundColor = [UIColor blackColor];
    carbonTabSwipeNavigation.carbonSegmentedControl.indicatorPosition = IndicatorPositionBottom;
    [carbonTabSwipeNavigation setIndicatorColor:[UIColor colorWithRed:129.0f/255 green:6.0f/255 blue:127.0f/255 alpha:1]];//81067F
    [carbonTabSwipeNavigation setNormalColor:[color colorWithAlphaComponent:0.5]
                                        font:[UIFont boldSystemFontOfSize:14]];
    [carbonTabSwipeNavigation setSelectedColor:color font:[UIFont boldSystemFontOfSize:14]];
}
#pragma mark - CarbonTabSwipeNavigation Delegate
- (nonnull UIViewController *)carbonTabSwipeNavigation:
(nonnull CarbonTabSwipeNavigation *)carbontTabSwipeNavigation
                                 viewControllerAtIndex:(NSUInteger)index {
    CurrentPastSegmentVC *latestObj = [[CurrentPastSegmentVC alloc]initWithNibName:@"CurrentPastSegmentVC" bundle:nil];
    OrderPlansVC  *planObj = [[OrderPlansVC alloc]initWithNibName:@"OrderPlansVC" bundle:nil];
        if (index==0) {
            return latestObj;
        }else{
            return planObj;
        }
    
}
- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                 willMoveAtIndex:(NSUInteger)index {
    // NSLog(@"Will move at index: %ld", index);
    
}

- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                  didMoveAtIndex:(NSUInteger)index {
}
- (IBAction)backButton_Tapped:(id)sender {
        [self.navigationController popViewControllerAnimated:true];
    }

- (UIBarPosition)barPositionForCarbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation {
    return UIBarPositionTop; //
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
