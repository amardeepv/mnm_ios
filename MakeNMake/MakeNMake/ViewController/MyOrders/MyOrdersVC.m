//
//  MyOrdersVC.m
//  MakeNMake
//
//  Created by Archit Saxena on 25/04/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "MyOrdersVC.h"
#import "OrderCustomTableCell.h"
#import "APIList.h"
#import "AlertView.h"
#import "Constant.h"
#import "CustomButton.h"

@interface MyOrdersVC ()
{
    NSMutableArray *orderList;
    BOOL isDetailButtonTapped, isDescriptionButtonTapped;
}
@end

@implementation MyOrdersVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self registerNibs];
    [self getDataForOrders];
    [_orderTableView reloadData];
    isDescriptionButtonTapped = false;
}

#pragma mark - Custom Methods

-(void)registerNibs{
    
    UINib* nibAddress = [UINib nibWithNibName:@"OrderCustomTableCell" bundle:nil];
    [_orderTableView registerNib:nibAddress forCellReuseIdentifier:@"OrderCustomTableCell"];
}

#pragma mark - UITableView DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return orderList.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    OrderCustomTableCell *cell = [_orderTableView dequeueReusableCellWithIdentifier:@"OrderCustomTableCell"];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell prepareForReuse];
    cell.delegate = self;
    
    if (isDescriptionButtonTapped) {
        [cell.detailCustomView setHidden:false];

    }else{
        [cell.detailCustomView setHidden:true];

    }
    
    cell.descriptionButton.indexPath = indexPath;
    [cell.descriptionButton setSelected:false];

    [cell configureCellWithDataDictionary:[orderList objectAtIndex:indexPath.section]];

    [cell.detailsButton addTarget:self action:@selector(details_ButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    cell.detailsButton.indexPath = indexPath;
  
    if (isDetailButtonTapped) {
        
        if (cell.detailsButton.selected) {
            [cell.detailsButton setSelected:false];
            [cell.detailCustomView setHidden:true];

        }else{
            [cell.detailsButton setSelected:true];
            [cell.detailCustomView setHidden:false];

        }
    }else{
        [cell.detailsButton setSelected:false];
        [cell.detailCustomView setHidden:true];

    }
    
    return cell;
}


#pragma mark - UITableView Delegate

- (void)changeCellHeights:(NSIndexPath *)indexPath
{
    if (isDetailButtonTapped) {
        if (self.expandedCellIndexPath) {
            //        [self.orderTableView reloadSections:[NSIndexSet indexSetWithIndex:self.expandedCellIndexPath.section] withRowAnimation:UITableViewRowAnimationNone];
            
        }
        self.expandedCellIndexPath = indexPath;
        [self.orderTableView reloadSections:[NSIndexSet indexSetWithIndex:self.expandedCellIndexPath.section] withRowAnimation:UITableViewRowAnimationNone];
    }else{
        
        [self.orderTableView reloadSections:[NSIndexSet indexSetWithIndex:self.expandedCellIndexPath.section] withRowAnimation:UITableViewRowAnimationNone];

    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (isDescriptionButtonTapped) {
        return 325;
    }
    if (isDetailButtonTapped) {
        if (self.expandedCellIndexPath && indexPath.row == self.expandedCellIndexPath.row && indexPath.section == self.expandedCellIndexPath.section) {
            if (isDetailButtonTapped) {
                return 266;
                
            }else{
                return 325;
            }
            
        }else{
            return 150;
            
        }

    }else{
        return 150;

    }
   }

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.001f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10.0f;
}
#pragma mark - Custom Methods

-(void)details_ButtonTapped:(CustomButton *)sender{

    if (sender.selected) {
        [sender setSelected:false];
        
        isDetailButtonTapped = false;
        [self changeCellHeights:sender.indexPath];

    }else{
        isDetailButtonTapped = true;

        [sender setSelected:true];

        [self changeCellHeights:sender.indexPath];

    }
    
    [_orderTableView reloadData];
}


#pragma mark - Api Calling

-(void)getDataForOrders{
    
    NSString *userId = [APPDELEGATE getUserID];
    NSString *serviceNameWithParam;
    
    if (_fromScreen==CurrentScreen) {
        serviceNameWithParam = [NSString stringWithFormat:@"ServiceDetailsCurrentList?UserID=%@",userId];
    }else{
        serviceNameWithParam = [NSString stringWithFormat:@"ServiceDetailsList?UserID=%@",userId];

    }


    [[APIList sharedAPIList] API_GetAllDataFromWS:serviceNameWithParam ShowLoader:YES showOverlay:YES completion:^(BOOL success, id responceData, NSMutableArray *share) {
        NSDictionary * result =(NSDictionary *) responceData;

        if (success) {
            if ([[result objectForKey:@"hasError"] isEqual:@(0)]) {
                [self GetArrayFromResponse:[[result valueForKey:@"data"] valueForKey:@"ServiceDetaillist"]];
            }
        }else{
            [AlertView showAlert:[result valueForKey:@"message"] alertType:AJNotificationTypeGreen];
            
        }
    }];
    
    
    
}
#pragma mark - Custom delegates
-(void)reloadSectionAtIndexPath:(NSIndexPath *)indexPath isSelected:(BOOL)isSelected{

    OrderCustomTableCell *cell = [_orderTableView dequeueReusableCellWithIdentifier:@"OrderCustomTableCell" forIndexPath:indexPath];
    
    if (isSelected) {
        isDescriptionButtonTapped = true;

    }else{
        cell.descriptionButton.selected = true;
    }
    
    [self changeCellHeights:indexPath];

}


#pragma mark - ParseResponse Method

- (void)GetArrayFromResponse:(NSMutableArray *)arrResponse {
    
    orderList = [[NSMutableArray alloc]init];
    orderList = arrResponse;
    
    [_orderTableView reloadData];
//    [_orderTableView sizeToFit];
}


#pragma mark -
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
