//
//  OrderPlansVC.h
//  MakeNMake
//
//  Created by Archit Saxena on 28/04/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderPlansVC : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *planTableView;

@end
