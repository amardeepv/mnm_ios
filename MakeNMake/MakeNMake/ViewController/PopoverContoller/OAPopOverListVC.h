//
//  OAPopOverListVC.h
//  OneAssist
//
//  Created by Gagan Maini on 18/10/16.
//  Copyright © 2016 Kellton Tech Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OAPopOverListVCDelegate <NSObject>

@optional
-(void) clickedFor:(NSString*)selection;
@end
@interface OAPopOverListVC : UITableViewController

@property(nonatomic,strong)NSArray* arrayListData;
@property(nonatomic) id <OAPopOverListVCDelegate> delegate;
-(void)reloadtableView;

@end
