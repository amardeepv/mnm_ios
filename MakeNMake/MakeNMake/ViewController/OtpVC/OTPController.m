//
//  OTPController.m
//  MakeNMake
//
//  Created by Chirag Patel on 2/11/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "OTPController.h"
#import "AFHTTPRequestOperationManager.h"
#import "Reachability.h"
#import "NetworkCheck.h"
#import "MBProgressHUD.h"
#import "Constant.h"
#import "AlertView.h"
#import "LoadingView.h"
#import "Utility.h"
#import "APIList.h"


@interface OTPController () {
    int countdownTime;
    
    NSTimer *timerCount;
    NSMutableDictionary *otpVerifyDict;
    
}

@end

@implementation OTPController

-(BOOL)prefersStatusBarHidden{
    return YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    countdownTime = 300;

    timerCount = [NSTimer scheduledTimerWithTimeInterval:1
                                             target:self
                                           selector:@selector(countDown)
                                           userInfo:nil
                                            repeats:YES];
    if ([_isFromVC isEqualToString:@"ChangePassword"]) {
        [_passwrdTxtFld setHidden:false];
    }else{
        [_passwrdTxtFld setHidden:true];

    }
}

-(void)countDown {
    countdownTime --;
    [self.lblTimer setText:[NSString stringWithFormat:@"%d seconds left",countdownTime]];
    if (countdownTime == 0) {
        [self.lblTimer setText:[NSString stringWithFormat:@"Time Up!"]];
        [timerCount invalidate];
        [self.navigationController popViewControllerAnimated:true];
    }
    
}
-(void)setOtpInfoWithOtp:(NSString *)otpString{
    
    _otpInfo = [[NSMutableDictionary alloc]init];
    
    [_otpInfo setValue:_mobileNumber forKey:@"mobile"];
    [_otpInfo setValue:_userId forKey:@"userId"];
    [_otpInfo setValue:otpString forKey:@"otp"];
    
    if ([_isFromVC isEqualToString:@"ChangePassword"]) {
        [self apiCallForCreatePasswordWithParam:otpString];
    }else{
        [self apiCallForOtpVerificationWithParam:_otpInfo];
    }
}

#pragma mark - Api call

-(void)apiCallForCreatePasswordWithParam:(NSString *)Otp{
    NSString *serviceNameWithParam = [NSString stringWithFormat:@"Account/ForgotPasswordOTPConfrim?mobile=%@&OTP=%@&password=%@",_mobileNumber,Otp,_passwrdTxtFld.text];
    
    [[APIList sharedAPIList] API_GetAllDataFromWS:serviceNameWithParam ShowLoader:YES showOverlay:YES completion:^(BOOL success, id responceData, NSMutableArray *share) {
        NSDictionary * result =(NSDictionary *) responceData;

        if (success) {
            [AlertView showAlert:[result valueForKey:@"message"] alertType:AJNotificationTypeGreen];
        }else{
            [AlertView showAlert:[result valueForKey:@"message"] alertType:AJNotificationTypeRed];
        }
        [self.navigationController popToRootViewControllerAnimated:true];

    }];

}

-(void)apiCallForOtpVerificationWithParam:(NSMutableDictionary *)param{
    NSLog(@"diction %@",param);
    if([NetworkCheck isInternetOff]) {
        
        [AlertView showAlert:@"The Internet connection appears to be offline." alertType:AJNotificationTypeRed];
        [LoadingView dismissLoader];
    }else {
        [LoadingView showLoaderWithOverlay:YES];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        AFJSONRequestSerializer* serialiser = [AFJSONRequestSerializer serializer];
        //    serialiser.timeoutInterval = 15.0;
        manager.responseSerializer=[AFJSONResponseSerializer serializer];
        manager.requestSerializer = serialiser;
        manager.responseSerializer.acceptableStatusCodes = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(200, 403)];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        NSString * strPostUrl = [NSString stringWithFormat:@"%@Account/VerifyMobile",BACKENDBASEURL];
        
        [manager POST:strPostUrl parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [LoadingView dismissLoader];

            if ([[responseObject valueForKey:@"message"] isEqualToString:@"Mobile verified successfully"]) {

                [[Utility sharedInstance] archiveFileWithFileName:OA_LOGIN_FILE andData:@"Registered"];
                
                [[NSUserDefaults standardUserDefaults] setValue:_userId forKey:UserId];
                
                [APPDELEGATE setIsOtpVerified:true];
                
                [self.navigationController popToRootViewControllerAnimated:true];
                
                [AlertView showAlert:@"You are now registered successfully" alertType:AJNotificationTypeGreen];
                
            }else{

                [AlertView showAlert:@"Otp Verification Failed" alertType:AJNotificationTypeRed];
                
            }
            
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [LoadingView dismissLoader];
        }];
        
    }
    
}
#pragma mark - UITextFieldDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    
    if (textField.tag==101) {
        
        NSString *tempString = [NSString stringWithFormat:@"%@%@",textField.text,string];
        
        if (tempString.length>4) {
            return NO;
            
        }else{
            return YES;
            
        }
    }else if(textField.tag==201){
        return YES;

    }else{
        return YES;
        
    }
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (self.canBecomeFirstResponder) {
        [self.txtOTP becomeFirstResponder];
    }
}

- (IBAction)btnBackPressed:(id)sender;{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)btnSubmitPressed:(id)sender; {
    
    [self setOtpInfoWithOtp:_txtOTP.text];
    [self dismissViewControllerAnimated:YES completion:nil];

}
- (IBAction)retryButton_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
