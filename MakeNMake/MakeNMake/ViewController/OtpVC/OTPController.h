//
//  OTPController.h
//  MakeNMake
//
//  Created by Chirag Patel on 2/11/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GenericTextField.h"


@interface OTPController : UIViewController

@property (strong, nonatomic) IBOutlet GenericTextField *txtOTP;
@property (weak, nonatomic) IBOutlet UILabel *lblTimer;
@property (weak, nonatomic) IBOutlet UILabel *lblSet;

@property (strong, nonatomic)  NSString *userId;
@property (strong, nonatomic)  NSString *mobileNumber;
@property (strong, nonatomic)  NSString *isFromVC;
@property (strong, nonatomic)  NSMutableDictionary *otpInfo;

@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UITextField *passwrdTxtFld;

- (IBAction)btnBackPressed:(id)sender;
- (IBAction)btnSubmitPressed:(id)sender;

@end
