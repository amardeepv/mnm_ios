//
//  MyWalletVC.m
//  MakeNMake
//
//  Created by Archit Saxena on 04/05/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "MyWalletVC.h"
#import "TransactionTableCell.h"
#import "APIList.h"
#import "AlertView.h"
#import "Constant.h"
#import "Utility.h"
#import "LoginSegment.h"

@interface MyWalletVC ()
{
    NSMutableArray *dataArray;
}
@end

@implementation MyWalletVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self registerNibs];
    
}
#pragma mark - Custom Methods

-(void)registerNibs{
    
    UINib* nibAddress = [UINib nibWithNibName:@"TransactionTableCell" bundle:nil];
    [_transactionTableView registerNib:nibAddress forCellReuseIdentifier:@"TransactionTableCell"];
    
    
}
-(void)viewWillAppear:(BOOL)animated{
        [super viewWillAppear:animated];
        
        [self configureView];
        [self getTransactionHistory];

}
-(void)configureView{
    
    if (![[Utility sharedInstance] unArchiveFileWithFileName:OA_LOGIN_FILE]) {
        LoginSegment  *loginObj = [[LoginSegment alloc]initWithNibName:@"LoginSegment" bundle:nil];
        loginObj.loginState = loginScreen;
        [self.navigationController pushViewController:loginObj animated:YES];

    }else{
    
    [_transactionTableView.layer setCornerRadius:8];
    [_transactionTableView.layer setMasksToBounds:true];
    [_transactionTableView.layer setShadowColor:[UIColor grayColor].CGColor];
    [_transactionTableView.layer setShadowRadius:2.0f];
    [_transactionTableView.layer setShadowOffset:CGSizeMake(0.0f, 0.0f)];
    [_transactionTableView.layer setMasksToBounds:false];
    [_transactionTableView.layer setShadowOpacity: 0.5];
    }
}

-(void)viewDidLayoutSubviews{
    
    [super viewDidLayoutSubviews];
//    [_transactionTableView sizeToFit];
    
}

#pragma mark - TableView DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    if (dataArray.count>0) {
        return dataArray.count+1;
    }else{
        return dataArray.count;
    }

}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    TransactionTableCell *cell = [_transactionTableView dequeueReusableCellWithIdentifier:@"TransactionTableCell"];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    if (indexPath.row==0) {

        [cell.dateLabel setFont:[UIFont boldSystemFontOfSize:13]];
        [cell.transactionDescLabel setFont:[UIFont boldSystemFontOfSize:13]];
        [cell.amountLabel setFont:[UIFont boldSystemFontOfSize:13]];
        [cell.amountLabel setTextColor:[UIColor blackColor]];
        [cell.dateLabel setText:@"Date"];
        [cell.transactionDescLabel setText:@"Description"];
        [cell.amountLabel setText:@"Amount"];
        
    }else{
            [cell configureCellWithictionary:[dataArray objectAtIndex:indexPath.row-1]];
    }
    
    return cell;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
  
}


#pragma mark - TableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
        return 0.001f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
        return 0.001f;
 
}
#pragma mark - Api Calling

-(void)getTransactionHistory{
    
    NSString *userId = [APPDELEGATE getUserID];
        NSString *serviceNameWithParam = [NSString stringWithFormat:@"WalletHistory?UserId=%@",userId];
    
        [[APIList sharedAPIList] API_GetAllDataFromWS:serviceNameWithParam ShowLoader:YES showOverlay:YES completion:^(BOOL success, id responceData, NSMutableArray *share) {
            if (success) {
                NSDictionary * result =(NSDictionary *) responceData;
                if ([[result objectForKey:@"hasError"] isEqual:@(0)]) {
                    [self GetArrayFromResponse:[[result valueForKey:@"data"] valueForKey:@"WalletHistory"]];
                     [_totalWalletAmount setText:[NSString stringWithFormat:@"₹ %@",[[result valueForKey:@"data"] valueForKey:@"WalletAmount"]]];
                }
            }else{
                [AlertView showAlert:@"Some Error Occured." alertType:AJNotificationTypeGreen];
    
            }
        }];
    
    
    
}

#pragma mark - ParseResponse Method

- (void)GetArrayFromResponse:(NSMutableArray *)arrResponse {
    
        NSLog(@"array recieved - %@",arrResponse);
        dataArray  = [[NSMutableArray alloc]init];
        dataArray = arrResponse;
        
        [_transactionTableView reloadData];
//        [_transactionTableView sizeToFit];
}
- (IBAction)backButton_Tapped:(id)sender {
    
    [self.navigationController popViewControllerAnimated:true];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
