//
//  MyWalletVC.h
//  MakeNMake
//
//  Created by Archit Saxena on 04/05/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyWalletVC : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *transactionTableView;
@property (weak, nonatomic) IBOutlet UILabel *totalWalletAmount;

@end
