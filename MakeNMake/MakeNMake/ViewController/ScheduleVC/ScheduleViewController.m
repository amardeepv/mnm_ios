//
//  ScheduleViewController.m
//  MakeNMake
//
//  Created by Chirag Patel on 3/19/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "ScheduleViewController.h"
#import "DateCollectionViewCell.h"
#import "Utility.h"
#import "Constant.h"
#import "APIList.h"
#import "showAddressTableCell.h"
#import "OfferViewController.h"
#import "AlertView.h"
#import "AFHTTPRequestOperationManager.h"
#import "LoadingView.h"
#import "LoginSegment.h"

@interface ScheduleViewController ()
{
    NTMonthYearPicker *datePicker;
    NSString *selectedMonthYear , *selectedDay, *selectedSlot ,*selectedAddress , *selectedAddressId ,*feedbackString;
    BOOL isAddressAdded , isFetchLocation , isAddressCustomViewMoved;
    NSInteger selectedIntegerDay;
    NSMutableArray *addressArray , *ZeroAmountServices ;
}
@end

@implementation ScheduleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self registerNib];
    isAddressAdded =false;


}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self configView];

}
#pragma mark - Custom Methods

-(void)registerNib{
    
    UINib* nibProfile = [UINib nibWithNibName:@"DateCollectionViewCell" bundle:nil];
    [_dateCollectionView registerNib:nibProfile forCellWithReuseIdentifier:@"DateCollectionViewCell"];

    UINib* nibAddress = [UINib nibWithNibName:@"showAddressTableCell" bundle:nil];
    [_addressTableView registerNib:nibAddress forCellReuseIdentifier:@"showAddressTableCell"];

}
-(void)configView{

    [self.navigationController setNavigationBarHidden:true];
    if (datePicker==nil) {
        datePicker = [[NTMonthYearPicker alloc]initWithFrame:CGRectMake(0, 20, self.view.frame.size.width, 165)];
        
        [datePicker setMinimumDate:[NSDate date]];
        
        selectedMonthYear = [[Utility sharedInstance] convertDate:datePicker.date toDateFormat:@"MMMM yyyy"];
        [_selectDateButton setTitle:selectedMonthYear forState:UIControlStateNormal];
        [_dateCollectionView reloadData];

    }
    [_dateContainerUIView addSubview:datePicker];
    _dateContainerUIView.hidden = true;
    _addnewButton.hidden = true;
    _addressTableView.hidden = true;
    _AddressCustomView.hidden = true;
    
    selectedAddress = [NSString stringWithFormat:@"%@",[_selectedAddressDict valueForKey:@"id"]];
//    [_dateCollectionView selectItemAtIndexPath:[NSIndexPath indexPathWithIndex:20] animated:true scrollPosition:UICollectionViewScrollPositionNone];
    
    if ([[Utility sharedInstance] unArchiveFileWithFileName:OA_LOGIN_FILE]) {
        //User Logged in
        _loginORregisterContainerView.hidden = true;

        if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"isOpenTicket"] isEqualToString:@"True"]) {
            _addressSectionView.hidden = true;
            _addMoreServiceButton.hidden = true;
            
        }else{
            _addressSectionView.hidden = false;
            _addMoreServiceButton.hidden = false;

            //get Address for Logged in user

            [self getAddressApiCallwithUserId:[APPDELEGATE getUserID]];

        }
        

    }else{
        //User not logged in
        _loginORregisterContainerView.hidden = false;
        
        }
    
}
-(NSString *)getMonthFromDateString:(NSString *)dateString{
    
    NSString *month = [dateString substringToIndex:3];
    
    return month;
}
-(NSString *)getWeekDayFromDate:(NSDate *)date{
    NSCalendarUnit dayOfTheWeek = [[NSCalendar currentCalendar] component:NSCalendarUnitWeekday fromDate:date];
    
    NSString *weekDay;
    
    switch ((unsigned long)dayOfTheWeek) {
        case 1:
            weekDay = @"Sat";
            break;
        case 2:
            weekDay = @"Sun";
            break;
        case 3:
            weekDay = @"Mon";
            break;
        case 4:
            weekDay = @"Tue";
            break;
        case 5:
            weekDay = @"Wed";
            break;
        case 6:
            weekDay = @"Thu";
            break;
        case 7:
            weekDay = @"Fri";
            break;
        default:
            weekDay = @"";

            break;
    }
    
    return weekDay;
}
-(BOOL)isLeapYear:(NSDate *)date{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date];
    
    NSInteger num = [components year];
    if (num/4) {
        return true;
    }else{
        return false;
    }
    
}
-(int)getNumberOfDaysInMonth:(NSString *)month{
    
    if ([month isEqualToString:@"Jan"] ||[month isEqualToString:@"Mar"] ||[month isEqualToString:@"May"] ||[month isEqualToString:@"Jul"] ||[month isEqualToString:@"Aug"] ||[month isEqualToString:@"Oct"] ||[month isEqualToString:@"Dec"] ) {
        return 31;
    }else if ([month isEqualToString:@"Apr"] ||[month isEqualToString:@"Jun"] ||[month isEqualToString:@"Sep"] ||[month isEqualToString:@"Nov"]){
        return 30;
    }else{
        if ([self isLeapYear:datePicker.date]) {
            return 29;
        }else{
            return 28;
        }
    }
    
}
#pragma mark - API Call
-(void)getAddressApiCallwithUserId:(NSString *)userID{
    //user/778/address
    NSString *serviceNameWithParam = [NSString stringWithFormat:@"user/%@/address",userID];
    
    [[APIList sharedAPIList] API_GetAllDataFromWS:serviceNameWithParam ShowLoader:YES showOverlay:YES completion:^(BOOL success, id responceData, NSMutableArray *share) {
        if (success) {
            NSDictionary * result =(NSDictionary *) responceData;
            if ([[result objectForKey:@"hasError"] isEqual:@(0)]) {
                [self GetArrayFromResponse:[[result valueForKey:@"data"] valueForKey:@"lstAddressResult"]];
            }
        }else{
            [AlertView showAlert:@"Some Error Occured." alertType:AJNotificationTypeGreen];

        }
    }];
}
#pragma mark - ParseResponse Method

- (void)GetArrayFromResponse:(NSMutableArray *)arrResponse {
    _addnewButton.hidden = false;
    _addressTableView.hidden = false;
    addressArray = [[NSMutableArray alloc]init];
    for (NSDictionary *dic in arrResponse) {
        if (![addressArray containsObject:dic]) {
            [addressArray addObject:dic];
        }
    }
    if (addressArray.count>0) {
        [_addressTableView reloadData];

        _addressTableView.hidden = false;
        _loginORregisterContainerView.hidden = true;
        _AddressCustomView.hidden = true;
        _addnewButton.hidden = false;

    }else{
        _addressTableView.hidden = true;
        _loginORregisterContainerView.hidden = true;
        _AddressCustomView.hidden = false;
//        _addnewButton.hidden = true;
    }

}
- (IBAction)addMoreItemsButton_Tapped:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:true];
}
#pragma mark - Button Methods
- (IBAction)morningButton_Action:(id)sender {
    
    int currentHourValue =  [[[Utility sharedInstance] convertDate:[NSDate date] toDateFormat:@"HH"] intValue];
    int currentDayValue =   [[[Utility sharedInstance] convertDate:[NSDate date] toDateFormat:@"dd"] intValue];

    if (currentDayValue == [selectedDay intValue]) {
        if (currentHourValue < 12.99) {
            //
            selectedSlot = @"0";
            [_morningButton setBackgroundColor:[UIColor colorWithRed:129.0f/255 green:6.0f/255 blue:127.0f/255 alpha:1]];
            [_morningButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_afternoonButton setBackgroundColor:[UIColor whiteColor]];
            [_afternoonButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
            [_eveningButton setBackgroundColor:[UIColor whiteColor]];
            [_eveningButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
            [_nightButton setBackgroundColor:[UIColor whiteColor]];
            [_nightButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        }
    }else{
        selectedSlot = @"0";
        [_morningButton setBackgroundColor:[UIColor colorWithRed:129.0f/255 green:6.0f/255 blue:127.0f/255 alpha:1]];
        [_morningButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_afternoonButton setBackgroundColor:[UIColor whiteColor]];
        [_afternoonButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [_eveningButton setBackgroundColor:[UIColor whiteColor]];
        [_eveningButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [_nightButton setBackgroundColor:[UIColor whiteColor]];
        [_nightButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];

    }
    
    
}
- (IBAction)addNewButtonTapped:(id)sender {
    
    SelectAddressVC *Obj = [[SelectAddressVC alloc]initWithNibName:@"SelectAddressVC" bundle: nil];
    
    [self presentViewController:Obj animated:true completion:nil];
    
//    addressArray = [[NSMutableArray alloc]init];
//    selectedAddress = @"";
//    _addnewButton.hidden = true;
//    _addressTableView.hidden = true;
//    _loginORregisterContainerView.hidden = true;
//    _AddressCustomView.hidden = false;
}
- (IBAction)afternoonButton_action:(id)sender {
   
    int currentHourValue =  [[[Utility sharedInstance] convertDate:[NSDate date] toDateFormat:@"HH"] intValue];
    int currentDayValue =  [[[Utility sharedInstance] convertDate:[NSDate date] toDateFormat:@"dd"] intValue];
    
    if (currentDayValue == [selectedDay intValue]) {
        if (currentHourValue < 16.99) {
            //
    selectedSlot = @"1";

    [_afternoonButton setBackgroundColor:[UIColor colorWithRed:129.0f/255 green:6.0f/255 blue:127.0f/255 alpha:1]];
    [_afternoonButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
  
    [_morningButton setBackgroundColor:[UIColor whiteColor]];
    [_morningButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [_eveningButton setBackgroundColor:[UIColor whiteColor]];
    [_eveningButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [_nightButton setBackgroundColor:[UIColor whiteColor]];
    [_nightButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        }
    }else{
            selectedSlot = @"1";
            
            [_afternoonButton setBackgroundColor:[UIColor colorWithRed:129.0f/255 green:6.0f/255 blue:127.0f/255 alpha:1]];
            [_afternoonButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            
            [_morningButton setBackgroundColor:[UIColor whiteColor]];
            [_morningButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
            [_eveningButton setBackgroundColor:[UIColor whiteColor]];
            [_eveningButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
            [_nightButton setBackgroundColor:[UIColor whiteColor]];
            [_nightButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        }
    
}

- (IBAction)eveningButton_Action:(id)sender {
   
    int currentHourValue =  [[[Utility sharedInstance] convertDate:[NSDate date] toDateFormat:@"HH"] intValue];
    int currentDayValue =  [[[Utility sharedInstance] convertDate:[NSDate date] toDateFormat:@"dd"] intValue];
    
    if (currentDayValue == [selectedDay intValue]) {
        if ( currentHourValue < 18.99) {
            //
    selectedSlot = @"2";

    [_eveningButton setBackgroundColor:[UIColor colorWithRed:129.0f/255 green:6.0f/255 blue:127.0f/255 alpha:1]];
    [_eveningButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [_afternoonButton setBackgroundColor:[UIColor whiteColor]];
    [_afternoonButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [_morningButton setBackgroundColor:[UIColor whiteColor]];
    [_morningButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [_nightButton setBackgroundColor:[UIColor whiteColor]];
    [_nightButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
            
        }
    }else{
        selectedSlot = @"2";
        
        [_eveningButton setBackgroundColor:[UIColor colorWithRed:129.0f/255 green:6.0f/255 blue:127.0f/255 alpha:1]];
        [_eveningButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [_afternoonButton setBackgroundColor:[UIColor whiteColor]];
        [_afternoonButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [_morningButton setBackgroundColor:[UIColor whiteColor]];
        [_morningButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [_nightButton setBackgroundColor:[UIColor whiteColor]];
        [_nightButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        
    }
    
}
- (IBAction)nightButtonAction:(id)sender {
    int currentHourValue =  [[[Utility sharedInstance] convertDate:[NSDate date] toDateFormat:@"HH"] intValue];
    int currentDayValue =  [[[Utility sharedInstance] convertDate:[NSDate date] toDateFormat:@"dd"] intValue];
    
    if (currentDayValue == [selectedDay intValue]) {
        if (currentHourValue < 20.99) {
            //
    
    selectedSlot = @"3";

    [_nightButton setBackgroundColor:[UIColor colorWithRed:129.0f/255 green:6.0f/255 blue:127.0f/255 alpha:1]];
    [_nightButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_morningButton setBackgroundColor:[UIColor whiteColor]];
    [_morningButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [_eveningButton setBackgroundColor:[UIColor whiteColor]];
    [_eveningButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [_afternoonButton setBackgroundColor:[UIColor whiteColor]];
    [_afternoonButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    
        }
    }else{
        
        selectedSlot = @"3";
        
        [_nightButton setBackgroundColor:[UIColor colorWithRed:129.0f/255 green:6.0f/255 blue:127.0f/255 alpha:1]];
        [_nightButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [_morningButton setBackgroundColor:[UIColor whiteColor]];
        [_morningButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [_eveningButton setBackgroundColor:[UIColor whiteColor]];
        [_eveningButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [_afternoonButton setBackgroundColor:[UIColor whiteColor]];
        [_afternoonButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    }
}

- (IBAction)selectMonth_Action:(id)sender {
    _dateContainerUIView.hidden = false;
    
}
- (IBAction)continueButton_Action:(id)sender {
    //OfferViewController
//    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"isOpenTicket"] isEqualToString:@"True"]) {
//        [self getDataForOpenTicket];
//    }else{
        if (_addressLine1TxtField.text.length>0 && _addressLine2TxtField.text.length>0) {
            NSMutableDictionary *AddressDict = [[NSMutableDictionary alloc]init];
            [AddressDict setValue:_addressLine1TxtField.text forKey:@"line1"];
            [AddressDict setValue:_addressLine2TxtField.text forKey:@"line2"];
            
            if (!(addressArray.count>0)){
                addressArray = [[NSMutableArray alloc]initWithObjects:AddressDict, nil];
                
                selectedAddress = [NSString stringWithFormat:@"%@,%@",_addressLine1TxtField.text,_addressLine2TxtField.text];
                [self calladdAddressApi];
                
            }
        }
   
//    }

    if ([self isAllEntriesFilled]) {
        
        int totalAmount = 0;
        ZeroAmountServices = [[NSMutableArray alloc]init];

        for (int i=0; i<[[APPDELEGATE getSelecteServices] count]; i++) {
            
            totalAmount += [[[[APPDELEGATE getSelecteServices] objectAtIndex:i] valueForKey:SERVICE_PRICE] intValue];
            
            if ([[[[APPDELEGATE getSelecteServices] objectAtIndex:i] valueForKey:SERVICE_PRICE] intValue] == 0) {
                [ZeroAmountServices addObject:[[APPDELEGATE getSelecteServices] objectAtIndex:i]];
            }
            
        }
        
        if (ZeroAmountServices.count>0) {
            //call api for open Ticket
            [self callAddOpenTicketApi];
        }
        
        if (totalAmount>0) {
            if ([NSString stringWithFormat:@"%@",selectedAddress].length>0) {
                OfferViewController *controller = [[UIStoryboard storyboardWithName:@"Second" bundle:nil] instantiateViewControllerWithIdentifier:@"OfferViewController"];
                
                controller.mainServiceTitle = _mainServiceName;
                controller.selectedAddress = [NSString stringWithFormat:@"%@",selectedAddress];
                controller.selectedTimeSlot = selectedSlot;
                controller.selectedDate = [NSString stringWithFormat:@"%@ %@",selectedDay,selectedMonthYear];
                controller.serviceGroupId = _serviceGroupId;
                controller.feedbackString = feedbackString;
                [self.navigationController pushViewController:controller animated:true];
                
                    }
                }else{
                    [[NSUserDefaults standardUserDefaults] setValue:@"False" forKey:@"isOpenTicket"];
                    [self.navigationController popToRootViewControllerAnimated:true];

        }
    }
}

-(void)getDataForOpenTicketForAddressId:(NSString *)addressId{
    
    NSString *userId = [APPDELEGATE getUserID];
    
    NSString *serviceNameWithParam = [NSString stringWithFormat:@"GetPlanForOpenTicket?UserId=%@&addressId=%@",userId,addressId];
    
    [[APIList sharedAPIList] API_GetAllDataFromWS:serviceNameWithParam ShowLoader:YES showOverlay:YES completion:^(BOOL success, id responceData, NSMutableArray *share) {
        if (success) {
            NSDictionary * result =(NSDictionary *) responceData;
            if ([[result objectForKey:@"hasError"] isEqual:@(0)]) {
                
                [self GetOpenTicketArrayFromResponse:[[result valueForKey:@"data"] valueForKey:@"Plans"]];
                
            }else{
                [AlertView showAlert:[result objectForKey:@"message"] alertType:AJNotificationTypeRed];
//                _noDataFoundView.center = _tableBackView.center;
//                [self.view addSubview:_noDataFoundView];
            }
        }else{
            [AlertView showAlert:@"Some Error Occured." alertType:AJNotificationTypeRed];
            
        }
    }];
    
    
    
}
- (void)GetOpenTicketArrayFromResponse:(NSMutableArray *)arrResponse {
    
    NSLog(@"array recieved services - %@",arrResponse);
    NSLog(@"selected services - %@",[APPDELEGATE getSelecteServices]);
    
    NSMutableArray *serviceListArray = [[NSMutableArray alloc]init];

    for (NSMutableDictionary *dict in arrResponse) {
        
        for (NSMutableDictionary *dictServices in [dict objectForKey:@"serviceList"]) {
            
            [serviceListArray addObjectsFromArray:[dictServices objectForKey:@"services"]];

        }
        
    }
    
    NSLog(@"array recieved services - %@",serviceListArray);

    for (NSMutableDictionary *dict in [APPDELEGATE getSelecteServices]) {

        NSString *serviceIdOne = [NSString stringWithFormat:@"%@",[dict valueForKey:@"serviceID"]];
        
        for (NSMutableDictionary *dict1 in serviceListArray) {
        
            NSString *serviceIdTwo = [NSString stringWithFormat:@"%@",[dict1 valueForKey:@"serviceId"]];

            if ([serviceIdOne isEqualToString:serviceIdTwo]) {
               
                [dict setValue:@"0" forKey:SERVICE_PRICE];
                
            }
        }

        
    }
    
    NSLog(@"array recieved services - %@",[APPDELEGATE getSelecteServices]);


}

-(void)callAddOpenTicketApi{
 
    //param
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    NSMutableArray *serviceArray = [[NSMutableArray alloc]init];

    for (NSMutableDictionary *dict in ZeroAmountServices) {
        
        [dict removeObjectForKey:@"amount"];
        [dict setValue:@"this is ticket description" forKey:@"ticketDesc"];
        [dict setValue:@"4120"/*[APPDELEGATE getUserID]*/ forKey:@"userId"];
        [dict setValue:[APPDELEGATE openTicketPlanId] forKey:@"planId"];
        [dict setValue:[NSString stringWithFormat:@"%@ %@",selectedDay,selectedMonthYear] forKey:@"prefferedDate"];
        [dict setValue:selectedSlot forKey:@"slotId"];
        [dict setValue:@"2" forKey:@"ticketType"];
        [dict setValue:@"1" forKey:@"quantity"];

        [serviceArray addObject:dict];
    }
    
    [parameters setValue:serviceArray forKey:@"lstOpenRequest"];

    NSString *strUrl = [NSString stringWithFormat:@"OpenTicket"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *strPostUrl = [NSString stringWithFormat:@"%@%@",BACKENDBASEURL,strUrl];
    
    AFJSONRequestSerializer* serialiser = [AFJSONRequestSerializer serializer];
    //    serialiser.timeoutInterval = 15.0;
    manager.responseSerializer=[AFJSONResponseSerializer serializer];
    manager.requestSerializer = serialiser;
    manager.responseSerializer.acceptableStatusCodes = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(200, 403)];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [LoadingView showLoader];
    [manager POST:strPostUrl parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [LoadingView dismissLoader];
        
        NSDictionary *dictResponce = (NSDictionary *)responseObject;
        
        if ([[dictResponce valueForKey:@"data"] count]>0) {
            if ([[dictResponce valueForKey:@"message"] isEqualToString:@"Ticket Opened Successfully"]) {
                
                [AlertView showAlert:[dictResponce valueForKey:@"message"] alertType:AJNotificationTypeGreen];

            }else{
                [AlertView showAlert:[dictResponce valueForKey:@"message"] alertType:AJNotificationTypeRed];

            }
        }else{
            [AlertView showAlert:[dictResponce valueForKey:@"message"] alertType:AJNotificationTypeRed];
                 }
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [LoadingView dismissLoader];
        [AlertView showAlert:[error localizedDescription] alertType:AJNotificationTypeRed];
        
    }];

    
    
}
-(void)calladdAddressApi{
    //param
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    
    [parameters setValue:@"0" forKey:@"id"];
    [parameters setValue:[APPDELEGATE getUserID] forKey:@"userId"];
    [parameters setValue:_addressLine2TxtField.text forKey:@"title"];
    [parameters setValue:_addressLine1TxtField.text forKey:@"line1"];
    [parameters setValue:_addressLine2TxtField.text forKey:@"city"];
    [parameters setValue:_addressLine2TxtField.text forKey:@"state"];
    [parameters setValue:_addressLine2TxtField.text forKey:@"area"];

    NSString *strUrl = [NSString stringWithFormat:@"user/%@/address",[APPDELEGATE getUserID]];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *strPostUrl = [NSString stringWithFormat:@"%@%@",BACKENDBASEURL,strUrl];
    
    AFJSONRequestSerializer* serialiser = [AFJSONRequestSerializer serializer];
    //    serialiser.timeoutInterval = 15.0;
    manager.responseSerializer=[AFJSONResponseSerializer serializer];
    manager.requestSerializer = serialiser;
    manager.responseSerializer.acceptableStatusCodes = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(200, 403)];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [AlertView showAlert:@"Adding Address" alertType:AJNotificationTypeBlue];

    [manager POST:strPostUrl parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [LoadingView dismissLoader];
        
        NSDictionary *dictResponce = (NSDictionary *)responseObject;
        
        if ([[dictResponce valueForKey:@"data"] count]>0) {
            if ([[dictResponce valueForKey:@"message"] isEqualToString:@"Address has been added."]) {
                
                selectedAddress = [[dictResponce valueForKey:@"data"] valueForKey:@"id"];
                
            }else{
                [AlertView showAlert:@"Some Error Occured." alertType:AJNotificationTypeRed];
            }
        }else{
            [AlertView showAlert:@"Bad request" alertType:AJNotificationTypeRed];
            
        }
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [LoadingView dismissLoader];
        [AlertView showAlert:[error localizedDescription] alertType:AJNotificationTypeRed];
        
    }];
}
-(BOOL)isAllEntriesFilled{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    
    NSInteger day = [components day];
    
    if (!(selectedMonthYear.length>0)) {
        [AlertView showAlert:@"Please select Month/ Year" alertType:AJNotificationTypeRed];
        return false;

    }else if(!(selectedDay.length>0)){
      
        [AlertView showAlert:@"Please select Day" alertType:AJNotificationTypeRed];
        return false;

    }else if(!(selectedIntegerDay>=day)){
        [AlertView showAlert:@"Please select Valid Day" alertType:AJNotificationTypeRed];
        return false;
    }else if (!(selectedSlot.length>0)){
        [AlertView showAlert:@"Please select time slot" alertType:AJNotificationTypeRed];
        return false;

    }
    else if (!([NSString stringWithFormat:@"%@",selectedAddress].length>0)){
            [AlertView showAlert:@"Please provide your address" alertType:AJNotificationTypeRed];
            return false;
    }else{
        return TRUE;

    }
}
- (IBAction)existingUserButton_Tapped:(id)sender {
    //redirect to login Screen
    LoginSegment  *loginObj = [[LoginSegment alloc]initWithNibName:@"LoginSegment" bundle:nil];
    loginObj.loginState = loginScreen;
    [self.navigationController pushViewController:loginObj animated:YES];

}
- (IBAction)newUserButton_Tapped:(id)sender {
    //redirect to Register Screen
    LoginSegment  *loginObj = [[LoginSegment alloc]initWithNibName:@"LoginSegment" bundle:nil];
    loginObj.loginState = RegisterScreen;
    [self.navigationController pushViewController:loginObj animated:YES];
}

- (IBAction)doneButton_Action:(id)sender {
    selectedMonthYear = [[Utility sharedInstance] convertDate:datePicker.date toDateFormat:@"MMMM yyyy"];

    [_selectDateButton setTitle:selectedMonthYear forState:UIControlStateNormal];
    _dateContainerUIView.hidden = true;

    [_dateCollectionView reloadData];
    
}
- (IBAction)backButton_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)cancelButtonAction:(id)sender {
    [_selectDateButton setTitle:@"Select Month & Year" forState:UIControlStateNormal];
    _dateContainerUIView.hidden = true;
    selectedMonthYear = nil;
}
#pragma mark - UITextFieldDelegate

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField.tag==909){
        feedbackString = textField.text;
    }
    else{
        
    }
    return YES;

}

#pragma mark - UItableiew Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return addressArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    

    //show address cell
    showAddressTableCell *cell = (showAddressTableCell*)[_addressTableView dequeueReusableCellWithIdentifier:@"showAddressTableCell"];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.addresslabel.text = [NSString stringWithFormat:@"%@ %@", [[addressArray objectAtIndex:indexPath.row] valueForKey:@"line1"], [[addressArray objectAtIndex:indexPath.row] valueForKey:@"line2"]];
    
        return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
          return 60;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
 
    if (![[[addressArray objectAtIndex:indexPath.row] valueForKey:@"title"] isKindOfClass:[NSNull class]]) {
        selectedAddress = [[addressArray objectAtIndex:indexPath.row] valueForKey:@"id"] ;
    }else{
        selectedAddress = [[addressArray objectAtIndex:indexPath.row] valueForKey:@"id"] ;

        }
    
    [self getDataForOpenTicketForAddressId:selectedAddress];
}

#pragma mark - Collection View dataSource

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    return CGSizeMake(50, 75);
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{

    if (selectedMonthYear==nil) {
        
        NSString *month = [self getMonthFromDateString:[[Utility sharedInstance] convertDate:datePicker.date toDateFormat:@"MMMM"]];
        
        return [self getNumberOfDaysInMonth:month];

    }else{
        NSString *month = [self getMonthFromDateString:selectedMonthYear];
        return [self getNumberOfDaysInMonth:month];
    }
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    DateCollectionViewCell *Cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"DateCollectionViewCell" forIndexPath:indexPath];
    
    if (!Cell){
        NSArray * arrNib = [[NSBundle mainBundle ]loadNibNamed:@"DateCollectionViewCell" owner:self options:nil];
        Cell = (DateCollectionViewCell *)arrNib[0];
    }
    if (Cell.isSelectedOnce) {
        [Cell setSelected:true];

    }else{
        [Cell setSelected:false];

    }
    
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    dayComponent.day = indexPath.row+1;
    Cell.dateNumberLabel.text = [NSString stringWithFormat:@"%ld",(long)indexPath.row+1];
    
    if (selectedMonthYear==nil) {
        
    NSDate *nextDate = [[NSCalendar currentCalendar] dateByAddingComponents:dayComponent toDate:[NSDate date] options:0];
    Cell.monthLabel.text = [self getWeekDayFromDate:nextDate];
    
            }else{
        
    NSDate *nextDate = [[NSCalendar currentCalendar] dateByAddingComponents:dayComponent toDate:datePicker.date options:0];
    Cell.monthLabel.text = [self getWeekDayFromDate:nextDate];

        }

    return Cell;
}

#pragma mark - UICollection Delegate  Method

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
        DateCollectionViewCell *Cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"DateCollectionViewCell" forIndexPath:indexPath];
        
        [Cell setSelected:true];
        
        NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
        dayComponent.day = indexPath.row+1;
        NSDate *nextDate = [[NSCalendar currentCalendar] dateByAddingComponents:dayComponent toDate:datePicker.date options:0];
        
        selectedIntegerDay = indexPath.row+1;
        
        selectedDay = [NSString stringWithFormat:@"%ld",indexPath.row+1];
        NSLog(@"Selected Month/Year - %@ , Selected Day - %ld,Selected Weekday - %@",selectedMonthYear,indexPath.row+1,[self getWeekDayFromDate:nextDate]);
    
    
    [_nightButton setBackgroundColor:[UIColor whiteColor]];
    [_nightButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_morningButton setBackgroundColor:[UIColor whiteColor]];
    [_morningButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [_eveningButton setBackgroundColor:[UIColor whiteColor]];
    [_eveningButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [_afternoonButton setBackgroundColor:[UIColor whiteColor]];
    [_afternoonButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];

}

#pragma mark -
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
