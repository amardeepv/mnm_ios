//
//  ScheduleViewController.h
//  MakeNMake
//
//  Created by Chirag Patel on 3/19/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NTMonthYearPicker.h"
#import "Utility.h"
#import "RegisterViewController.h"
#import "LoginViewController.h"
#import "SelectAddressVC.h"


@interface ScheduleViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UITableViewDelegate,UITableViewDataSource, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *dateContainerUIView;
@property (weak, nonatomic) IBOutlet UIButton *selectDateButton;
@property (weak, nonatomic) IBOutlet UICollectionView *dateCollectionView;
@property (weak, nonatomic) IBOutlet UIButton *nightButton;
@property (weak, nonatomic) IBOutlet UITableView *addressTableView;
@property (weak, nonatomic) IBOutlet UIButton *afternoonButton;
@property (weak, nonatomic) IBOutlet UIButton *morningButton;
@property (weak, nonatomic) IBOutlet UIButton *eveningButton;
@property (weak, nonatomic) IBOutlet UIView *loginORregisterContainerView;
@property (weak, nonatomic) IBOutlet UIButton *addnewButton;
@property (weak, nonatomic) IBOutlet UIView *AddressCustomView;
@property (weak, nonatomic) IBOutlet UITextField *addressLine2TxtField;
@property (weak, nonatomic) IBOutlet UITextField *addressLine1TxtField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addressTableViewTopConstraint;
@property (strong, nonatomic) NSMutableDictionary *selectedAddressDict;

@property (weak, nonatomic) IBOutlet UIButton *addMoreServiceButton;
@property (strong, nonatomic)  NSString *mainServiceName;
@property (strong, nonatomic) NSString *serviceGroupId;
@property (weak, nonatomic) IBOutlet UIView *addressSectionView;

@end
