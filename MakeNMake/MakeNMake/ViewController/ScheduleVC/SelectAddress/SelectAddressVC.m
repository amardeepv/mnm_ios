//
//  SelectAddressVC.m
//  MakeNMake
//
//  Created by Archit Saxena on 17/06/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "SelectAddressVC.h"
#import "Utility.h"
#import "Constant.h"
#import "APIList.h"
#import "AlertView.h"
#import "LoadingView.h"

@interface SelectAddressVC ()
{
    NSMutableArray *addressArray;
    NSString *selectedAddressId , *selectedState , *selectedCity;
    NSMutableDictionary *selectedAddressDict;
}
@end

@implementation SelectAddressVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _addressTableView.estimatedRowHeight = 30;
    [self configureView];
}

-(void)configureView{
    
    _addressTableView.hidden = true;
    addressArray = [[NSMutableArray alloc]init];
    selectedAddressDict = [[NSMutableDictionary alloc]init];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{

//    if (addressArray.count>0) {
//            [addressArray removeAllObjects];
//        }
    
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    
    //location filter
    if (textField.tag==101) {

        NSString *tempString = [NSString stringWithFormat:@"%@%@",textField.text,string];
    
        if (tempString.length>0) {
    
            [self LocationSearchServicesApi:[[tempString stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"," withString:@""]];
        }
    
    }
    return YES;
    
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    
    if (textField.tag==101) {
        [textField resignFirstResponder];
        
    }else{
        [textField resignFirstResponder];
        [_addressLine2 becomeFirstResponder];
        
    }
    
    return YES;
    // We do not want UITextField to insert line-breaks.
}
#pragma mark - Location Services Called Method


- (void)LocationSearchServicesApi:(NSString*)strSearchText {
    
    //https://maps.googleapis.com/maps/api/geocode/json?address=Sector-12&components=country:IN&key=AIzaSyC2Z2qRrXy_WJLHSyyrfBMTXbYObgwhbys
    
    //https://maps.googleapis.com/maps/api/place/autocomplete/json?=types=geocode&sensor=false&components=country:IN&key=AIzaSyC2Z2qRrXy_WJLHSyyrfBMTXbYObgwhbys&input=%@
    
    NSString *strSearch = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?address=%@&components=country:IN&key=AIzaSyC2Z2qRrXy_WJLHSyyrfBMTXbYObgwhbys",strSearchText];
    
    [[APIList sharedAPIList] API_LocationServiceGetAllDataFromWS:strSearch ShowLoader:NO showOverlay:YES completion:^(BOOL success, id responceData, NSMutableArray *share) {
        if (success) {
            NSDictionary * result =(NSDictionary *) responceData;
            if ([[result objectForKey:@"status"] isEqualToString:@"OK"]) {
                
                if ([[result objectForKey:@"status"] caseInsensitiveCompare:@"OK"] == NSOrderedSame) {
                    addressArray = result[@"results"];
                }
    
                _addressTableView.hidden = false;
                [_addressTableView reloadData];
                [_addressTableView sizeToFit];
                
            }
        }
    }];
}
#pragma mark - UItableiew Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return addressArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //location api response
    static NSString *simpleTableIdentifier = @"Cell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        cell.textLabel.text = [[addressArray objectAtIndex:indexPath.row] valueForKey:@"formatted_address"];
        [cell.textLabel setFont:[UIFont systemFontOfSize:14]];
        
        return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
        return 30;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    _addressTableView.hidden = true;
    _addressLine2.text = [[addressArray objectAtIndex:indexPath.row] valueForKey:@"formatted_address"];
    
    selectedAddressDict = [[[addressArray objectAtIndex:indexPath.row] valueForKey:@"geometry"] valueForKey:@"location"];
    
    selectedState = [[[[addressArray objectAtIndex:indexPath.row] valueForKey:@"address_components"] objectAtIndex:2] valueForKey:@"long_name"];
    selectedCity = [[[[addressArray objectAtIndex:indexPath.row] valueForKey:@"address_components"] firstObject] valueForKey:@"long_name"];
}
#pragma mark - Add Address Api

-(void)calladdAddressApi{
    //param
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
  
    [parameters setValue:@"0" forKey:@"id"];
    [parameters setValue:[APPDELEGATE getUserID] forKey:@"userId"];

    [parameters setValue:_addressLine1.text forKey:@"line1"];
    [parameters setValue:_addressLine2.text forKey:@"line2"];
    
    [parameters setValue:[selectedAddressDict valueForKey:@"lat"] forKey:@"latitude"];
    [parameters setValue:[selectedAddressDict valueForKey:@"lng"] forKey:@"longitude"];

    [parameters setValue:selectedState forKey:@"state"];
    [parameters setValue:selectedCity forKey:@"city"];
    [parameters setValue:[NSString stringWithFormat:@"%@ %@",_addressLine1.text,_addressLine2.text] forKey:@"title"];

    [parameters setValue:_addressLine1.text forKey:@"area"];
    
    NSString *strUrl = [NSString stringWithFormat:@"user/%@/address",[APPDELEGATE getUserID]];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *strPostUrl = [NSString stringWithFormat:@"%@%@",BACKENDBASEURL,strUrl];
    
    AFJSONRequestSerializer* serialiser = [AFJSONRequestSerializer serializer];
    //    serialiser.timeoutInterval = 15.0;
    manager.responseSerializer=[AFJSONResponseSerializer serializer];
    manager.requestSerializer = serialiser;
    manager.responseSerializer.acceptableStatusCodes = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(200, 403)];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
//    [AlertView showAlert:@"Adding Address" alertType:AJNotificationTypeBlue];
    [LoadingView showLoader];
    
    [manager POST:strPostUrl parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [LoadingView dismissLoader];
        
        NSDictionary *dictResponce = (NSDictionary *)responseObject;
        
        if ([[dictResponce valueForKey:@"data"] count]>0) {
            if ([[dictResponce valueForKey:@"message"] isEqualToString:@"Address has been added."]) {
                
                selectedAddressId = [[dictResponce valueForKey:@"data"] valueForKey:@"id"];
                
            }else{
                [AlertView showAlert:[dictResponce valueForKey:@"message"] alertType:AJNotificationTypeRed];
            }
        }else{
            [AlertView showAlert:[dictResponce valueForKey:@"message"] alertType:AJNotificationTypeRed];
            
        }
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [LoadingView dismissLoader];
        [AlertView showAlert:[error localizedDescription] alertType:AJNotificationTypeRed];
        
    }];
}

#pragma mark - UIButton Method


- (IBAction)AddAddressButton_Action:(id)sender {
    [self.view endEditing:true];
    [self calladdAddressApi];
    [self dismissViewControllerAnimated:true completion:nil];
}
- (IBAction)backButton_Action:(id)sender {
    [self.view endEditing:true];
    [self dismissViewControllerAnimated:true completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
