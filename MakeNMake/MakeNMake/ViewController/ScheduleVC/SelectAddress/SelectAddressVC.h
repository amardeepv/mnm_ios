//
//  SelectAddressVC.h
//  MakeNMake
//
//  Created by Archit Saxena on 17/06/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFHTTPRequestOperationManager.h"

@interface SelectAddressVC : UIViewController<UITableViewDelegate,UITableViewDataSource, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *addressLine1;
@property (weak, nonatomic) IBOutlet UITextField *addressLine2;
@property (weak, nonatomic) IBOutlet UITableView *addressTableView;
@end
