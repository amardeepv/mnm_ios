//
//  OpenTicketVC.m
//  MakeNMake
//
//  Created by Archit Saxena on 22/04/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "OpenTicketVC.h"
#import "TicketServiceNameTableViewCell.h"
#import "APIList.h"
#import "AlertView.h"
#import "Constant.h"
#import "AllServiceViewController.h"
#import "Utility.h"
#import "LoginSegment.h"

@interface OpenTicketVC ()
{
    NSMutableArray *serviceListArray , *mainServiceArray;
    BOOL isPlanSelected;
}
@end

@implementation OpenTicketVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if ([[Utility sharedInstance] unArchiveFileWithFileName:OA_LOGIN_FILE]) {
        [self getDataForOpenTicket];
        [self registerNibs];
    }
   

   }

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (![[Utility sharedInstance] unArchiveFileWithFileName:OA_LOGIN_FILE]) {
        LoginSegment  *loginObj = [[LoginSegment alloc]initWithNibName:@"LoginSegment" bundle:nil];
        loginObj.loginState = loginScreen;
        [self.navigationController pushViewController:loginObj animated:YES];
        
    }else{
        [self configureView];
    }

}
#pragma mark - Custom Methods

-(void)registerNibs{
    
    UINib* nibAddress = [UINib nibWithNibName:@"TicketServiceNameTableViewCell" bundle:nil];
    [_serviceTableView registerNib:nibAddress forCellReuseIdentifier:@"TicketServiceNameTableViewCell"];
}
-(void)configureView{
    
    [_ticketIconView.layer setShadowColor:[UIColor blackColor].CGColor];
    [_ticketIconView.layer setShadowRadius:8.0f];
    [_ticketIconView.layer setMasksToBounds:false];
    [_ticketIconView.layer setShadowOpacity: 0.5];

    [_serviceTableView.layer setCornerRadius:8];
    [_serviceTableView.layer setMasksToBounds:true];
    [_serviceTableView.layer setShadowColor:[UIColor grayColor].CGColor];
    [_serviceTableView.layer setShadowRadius:5.0f];
    [_serviceTableView.layer setShadowOffset:CGSizeMake(0.0f, 0.0f)];
    [_serviceTableView.layer setMasksToBounds:false];
    [_serviceTableView.layer setShadowOpacity: 0.5];
    
    [_noDataFoundView setFrame:_serviceTableView.frame];

 
}

-(void)viewDidLayoutSubviews{
    
    [super viewDidLayoutSubviews];
//    [_serviceTableView sizeToFit];
    
    if (serviceListArray.count==0) {
        _noDataFoundView.center = _serviceTableView.center;
        [_noDataFoundView setFrame:_serviceTableView.frame];
        [_tableBackView addSubview:_noDataFoundView];
    }else{
        [_noDataFoundView removeFromSuperview];
    }
    
}

#pragma mark - TableView DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
        return serviceListArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    if (isPlanSelected) {
        
        TicketServiceNameTableViewCell *cell = [_serviceTableView dequeueReusableCellWithIdentifier:@"TicketServiceNameTableViewCell"];
        [cell.infoButton setTag:indexPath.section];
        [cell.infoButton addTarget:self action:@selector(infoButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        if ([[[serviceListArray objectAtIndex:indexPath.section] valueForKey:@"serviceName"] isEqualToString:@"AC"]) {
            [cell configureCellWithServiceName:[[serviceListArray objectAtIndex:indexPath.section] valueForKey:@"serviceName"] andServiceDesc:@"wolf moon tempor, sunt aliqua put" andServiceIcon:@"acrepair"];
        }else{
            [cell configureCellWithServiceName:[[serviceListArray objectAtIndex:indexPath.section] valueForKey:@"serviceName"] andServiceDesc:@"wolf moon tempor, sunt aliqua put" andServiceIcon:[[[serviceListArray objectAtIndex:indexPath.section] valueForKey:@"serviceName"] stringByReplacingOccurrencesOfString:@" " withString:@""]];
        }
        
        return cell;

    }else{
        
        static NSString *simpleTableIdentifier = @"Cell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        [ cell.layer setCornerRadius:8];
        [ cell.layer setMasksToBounds:true];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        [cell setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.textLabel.text = [serviceListArray objectAtIndex:indexPath.section];
        [cell.textLabel setFont:[UIFont systemFontOfSize:17]];
        
        return cell;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (isPlanSelected) {
        
        [[NSUserDefaults standardUserDefaults] setValue:@"True" forKey:@"isOpenTicket"];
        
        AllServiceViewController *controller = [[UIStoryboard storyboardWithName:@"Second" bundle:nil] instantiateViewControllerWithIdentifier:@"AllServiceViewController"];
        controller.selectedAddress = _selectedAddress;
        controller.isFrom  = OpenTicket;
        controller.masterServiceArray =  [[serviceListArray objectAtIndex:indexPath.section] valueForKey:@"services"];
        
        controller.titleString = [[serviceListArray  objectAtIndex:indexPath.section] valueForKey:@"serviceName"];
        controller.serviceGroupId = [[serviceListArray  objectAtIndex:indexPath.section] valueForKey:@"serviceId"];
        [APPDELEGATE setOpenTicketPlanId:[[serviceListArray  objectAtIndex:indexPath.section] valueForKey:@"serviceId"]];
        
        [self.navigationController pushViewController:controller animated:YES];
    }else{
        isPlanSelected = true;
        
        
        serviceListArray = [[mainServiceArray objectAtIndex:indexPath.section] valueForKey:@"serviceList"];
    
        [_serviceTableView reloadData];
//        [_serviceTableView sizeToFit];
    }
    
    
    
}


#pragma mark - TableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section!=0) {
        return 0.001f;
    }else{
        return 15;      //First section
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
//    if (section==serviceListArray.count-1) {
//        return 0.001f;
//    }else{
        return 15;
//    }
}
#pragma mark - Api Calling

-(void)getDataForOpenTicket{
    
    NSString *userId = [APPDELEGATE getUserID];
    NSString *addressId = [_selectedAddress valueForKey:@"id"];

    NSString *serviceNameWithParam = [NSString stringWithFormat:@"GetPlanForOpenTicket?UserId=%@&addressId=%@",userId,addressId];
    
    [[APIList sharedAPIList] API_GetAllDataFromWS:serviceNameWithParam ShowLoader:YES showOverlay:YES completion:^(BOOL success, id responceData, NSMutableArray *share) {
        if (success) {
            NSDictionary * result =(NSDictionary *) responceData;
            if ([[result objectForKey:@"hasError"] isEqual:@(0)]) {
                
                [self GetArrayFromResponse:[[result valueForKey:@"data"] valueForKey:@"Plans"]];

            }else{
                [AlertView showAlert:[result objectForKey:@"message"] alertType:AJNotificationTypeRed];
                _noDataFoundView.center = _tableBackView.center;
                [self.view addSubview:_noDataFoundView];
            }
        }else{
            [AlertView showAlert:@"Some Error Occured." alertType:AJNotificationTypeRed];
            
        }
    }];
    
    
    
}
-(void)infoButtonTapped:(UIButton *)sender{
    
    NSString *alertMsg ;
    
    if ([[[serviceListArray objectAtIndex:sender.tag] valueForKey:@"totalCalls"] isEqual:[NSNumber numberWithInteger:99]]) {
       alertMsg = [NSString stringWithFormat:@"Total Calls : Unlimited Calls"];
    }else{
        
        alertMsg = [NSString stringWithFormat:@"Total Calls : %@",[[serviceListArray objectAtIndex:sender.tag] valueForKey:@"totalCalls"]];
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Info" message:alertMsg preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                         {
                             [self dismissViewControllerAnimated:true completion:nil];
                         }];
    
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];

}
- (IBAction)backButton_Tapped:(id)sender {
    
    [self.navigationController popViewControllerAnimated:true];
    
}
#pragma mark - ParseResponse Method

- (void)GetArrayFromResponse:(NSMutableArray *)arrResponse {
    
    NSLog(@"array recieved - %@",arrResponse);
    serviceListArray = [[NSMutableArray alloc]init];
    mainServiceArray = [[NSMutableArray alloc] init];
    
    mainServiceArray = arrResponse;
    
    for (NSDictionary *dict in arrResponse) {
        
        if (![serviceListArray containsObject:[dict valueForKey:@"planName"]]) {
            [serviceListArray addObject:[dict valueForKey:@"planName"]];
        }
    }
    
    if (serviceListArray.count==0) {
        _noDataFoundView.center = _serviceTableView.center;
        [_serviceTableView addSubview:_noDataFoundView];
    }else{
        [_noDataFoundView removeFromSuperview];
    }
    
    [_serviceTableView reloadData];
//    [_serviceTableView sizeToFit];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
