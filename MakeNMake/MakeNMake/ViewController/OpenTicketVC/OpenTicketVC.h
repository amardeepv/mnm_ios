//
//  OpenTicketVC.h
//  MakeNMake
//
//  Created by Archit Saxena on 22/04/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OpenTicketVC : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIView *ticketIconView;
@property (weak, nonatomic) IBOutlet UITableView *serviceTableView;
@property (weak, nonatomic) IBOutlet UIView *tableBackView;
@property (strong, nonatomic) IBOutlet UIView *noDataFoundView;
@property (strong, nonatomic) NSMutableDictionary *selectedAddress;

@end
