//
//  VerifyMobileNumberVC.h
//  MakeNMake
//
//  Created by Archit Saxena on 08/04/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    
    AddMobile=0,
    ChangePassword=1
    
} isFromScreen;


@interface VerifyMobileNumberVC : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UITextField *mobileNumberTxtField;
@property (weak, nonatomic) NSString *userId;
@property (assign, nonatomic) isFromScreen *isFromScreen;

@end
