//
//  VerifyMobileNumberVC.m
//  MakeNMake
//
//  Created by Archit Saxena on 08/04/17.
//  Copyright © 2017 Chirag. All rights reserved.
//

#import "VerifyMobileNumberVC.h"
#import "OTPController.h"
#import "Constant.h"
#import "AppDelegate.h"
#import "APIList.h"
#import "AlertView.h"
#import "AFNetworking.h"
#import "LoadingView.h"

@interface VerifyMobileNumberVC ()

@end

@implementation VerifyMobileNumberVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:true];
    if ([APPDELEGATE isOtpVerified]) {
        [self.navigationController popViewControllerAnimated:true];
        
    }
}
- (IBAction)submitButtonTapped:(id)sender {
    
    if (_mobileNumberTxtField.text.length>9) {
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        
        [dict setValue:_userId forKey:@"userId"];
        [dict setValue:_mobileNumberTxtField.text forKey:@"mobile"];
        
        if (_isFromScreen==ChangePassword) {
            [self verifyMobileForChangePasswordWithMobileNumber:_mobileNumberTxtField.text];
        }else{
            [self addPhoneNumber:dict];
        }

    }else{
        [AlertView showAlert:@"Enter Valid Mobile Number" alertType:AJNotificationTypeRed];
    }
    
}

- (void)verifyMobileForChangePasswordWithMobileNumber:(NSString *)mobileNumber{
    
    NSString *serviceNameWithParam = [NSString stringWithFormat:@"Account/ForgotPasswordOTP?mobile=%@",mobileNumber];
    
    [[APIList sharedAPIList] API_GetAllDataFromWS:serviceNameWithParam ShowLoader:YES showOverlay:YES completion:^(BOOL success, id responceData, NSMutableArray *share) {
        if (success) {
            NSDictionary * result =(NSDictionary *) responceData;
    
            [AlertView showAlert:[result valueForKey:@"message"] alertType:AJNotificationTypeGreen];
            
            OTPController *controller = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"OTPController"];
            controller.mobileNumber = mobileNumber;
            if (_isFromScreen==ChangePassword) {
                controller.isFromVC = @"ChangePassword";
            }
            [self.navigationController pushViewController:controller animated:true];
        }
    }];
}


-(void)addPhoneNumber:(NSMutableDictionary *)dict{
    [LoadingView showLoaderWithOverlay:YES];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSString *strPostUrl = [NSString stringWithFormat:@"%@Account/AddMobile",BACKENDBASEURL];
        
        AFJSONRequestSerializer* serialiser = [AFJSONRequestSerializer serializer];
        //    serialiser.timeoutInterval = 15.0;
        manager.responseSerializer=[AFJSONResponseSerializer serializer];
        manager.requestSerializer = serialiser;
        manager.responseSerializer.acceptableStatusCodes = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(200, 403)];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [manager POST:strPostUrl parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [LoadingView dismissLoader];
            
            if ([[responseObject valueForKey:@"message"] isEqualToString:@"Mobile has been added with account"])     {
                //show otp screen and verify number
                NSString*  strUserId = [NSString stringWithFormat:@"%@",[[responseObject valueForKey:@"data"] valueForKey:@"userId"]];
                OTPController *controller = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"OTPController"];
                controller.userId = strUserId;
                controller.mobileNumber = [dict valueForKey:@"mobile"];
                if (_isFromScreen==ChangePassword) {
                    controller.isFromVC = @"ChangePassword";
                }
                [self.navigationController pushViewController:controller animated:true];
                
                [APPDELEGATE setIsOtpVerified:true];
//                [AlertView showAlert:[responseObject valueForKey:@"message"] alertType:AJNotificationTypeGreen];

            }else{
                [APPDELEGATE setIsOtpVerified:false];

                [AlertView showAlert:@"Something went wrong. Please try again" alertType:AJNotificationTypeRed];
                
            }
//            [self.navigationController popViewControllerAnimated:true];

        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [LoadingView dismissLoader];
            [AlertView showAlert:[error localizedDescription] alertType:AJNotificationTypeRed];
            
        }];
    }

#pragma mark - UITextFieldDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
        NSString *tempString = [NSString stringWithFormat:@"%@%@",textField.text,string];
        
        
        if (tempString.length == 10) {
            [_submitButton.layer setOpacity:1];
            return YES;
        }else if(tempString.length >10){
            return NO;
        }
        else if(tempString.length <10){
            
            [_submitButton.layer setOpacity:0.5];
            return YES;
            
        }else{
            return YES;
        
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
