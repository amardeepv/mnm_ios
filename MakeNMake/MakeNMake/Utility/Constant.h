//
//  Constant.h
//
//
//  Created by Nripendra  singh
//  Copyright © 2016 Nripendra  singh. All rights reserved.
//

#ifndef Constant_h
#define Constant_h
#import "Reachability.h"
#import "MBProgressHUD.h"
#import "AFHTTPSessionManager.h"
#import "StaticClass.h"
#import "AppDelegate.h"

#define NETWORKERRORMESSAGEALERT [[UIAlertView alloc] initWithTitle:@"Alert" message:@"The Internet connection appears to be offline." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil]

#define  APPDELEGATE     [AppDelegate sharedInstance]

#define g_StringFormate(fmt, args...)   [NSString stringWithFormat:fmt, ##args]
#define g_AppDelegate ((AppDelegate *)  [[UIApplication sharedApplication] delegate])

//UAT URL
//#define BACKENDBASEURL    @"http://103.25.131.238/mnmapi/"

//Production URL
#define BACKENDBASEURL @"https://www.makenmake.in/mnmapi/"


#define REGISTRATION      @"Account/Register"
#define ADDMOBILE             @"Addmobile"
#define EXTERNALLOGIN             @"ExternalLogin"
#define ADDVERIFY             @"addmobile"
#define LOGIN @"Account/login"
#define OA_LOGIN_FILE @"loginDetails"
#define UserId @"UserId"
#define SERVICE_NAME @"serviceName"
#define SERVICE_PRICE @"amount"

// API Request Name`
#define g_Request_Login             @"user_inquiry"

#define userDefaults    [NSUserDefaults standardUserDefaults]

#define DICT_TO_OBJECT(OBJECT) (id)OBJECT!=[NSNull null]?OBJECT:nil
#define DICT_TO_TEXT(TEXT,NULL) (id)TEXT!=NULL?TEXT:@""
#define TEXT_TO_DICT(TEXT,NIL,NULL) TEXT!=NIL?TEXT:NULL
#define RGBCOLOR(r,g,b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1]

// Fonts
#define g_Font_Lato_Regular_13 [UIFont fontWithName:@"Lato-Regular" size:13.0]
#define g_Font_Lato_Bold_13 [UIFont fontWithName:@"Lato-Bold" size:13.0]

// RGB Color
#define g_RGB(R, G, B)      [UIColor colorWithRed:R/255.0 green:G/255.0 blue:B/255.0 alpha:1]
#define g_RGBA(R, G, B, A)  [UIColor colorWithRed:R/255.0 green:G/255.0 blue:B/255.0 alpha:A]

// NSDictionary Key
#define g_Dict_SelectedType             @"SelectedType"
#define g_Dict_SelectedPaymentMethod    @"SelectedPaymentMethod"
#define g_Dict_SelectedCurrency         @"SelectedCurrency"
#define g_Dict_SelectedFromDate         @"SelectedFromDate"
#define g_Dict_SelectedToDate           @"SelectedToDate"

// NSNotificationCenter Key
#define g_NotificationCenter_AddExpense         @"AddExpense"

// NSUserDefaults Key
#define g_UserDefaults_IsUserLogin          @"IsUserLogin"
#define g_UserDefaults_AppLunchFirstTime    @"AppLunchFirstTime"
#define g_UserDefaults_DictExpensesSorting  @"DictExpensesSorting"


#define g_UserDefaults_Token    @"TOKEN"
// Check Screen Size
#define g_ScreenWidth   [[UIScreen mainScreen] bounds].size.width
#define g_ScreenHeight  [[UIScreen mainScreen] bounds].size.height

// Check View Size
#define g_ViewWidth(view)   view.frame.size.width
#define g_ViewHeight(view)  view.frame.size.height

// Check Version Compatibility
#define g_IS_IOS_6  ([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0)
#define g_IS_IOS_7  ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
#define g_IS_IOS_8  ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

// Check Device Compatibility
#define g_IS_IPAD   ([[[UIDevice currentDevice] model] isEqualToString:@"iPad"])
#define g_IS_IPHONE ([[[UIDevice currentDevice] model] isEqualToString:@"iPhone"])
#define g_IS_IPOD   ([[[UIDevice currentDevice] model] isEqualToString:@"iPod touch"])

#define g_IS_IPHONE_4_SCREEN        [[UIScreen mainScreen] bounds].size.height >= 480.0f && [[UIScreen mainScreen] bounds].size.height < 568.0f
#define g_IS_IPHONE_5_SCREEN        [[UIScreen mainScreen] bounds].size.height >= 568.0f && [[UIScreen mainScreen] bounds].size.height < 667.0f
#define g_IS_IPHONE_6_SCREEN        [[UIScreen mainScreen] bounds].size.height >= 667.0f && [[UIScreen mainScreen] bounds].size.height < 736.0f
#define g_IS_IPHONE_6Plus_SCREEN    [[UIScreen mainScreen] bounds].size.height >= 736.0f && [[UIScreen mainScreen] bounds].size.height < 1024.0f

// Slider Width
//#define g_SliderWidth_Left  200

// Other
#define g_StringFormate(fmt, args...)   [NSString stringWithFormat:fmt, ##args]
#define g_AppDelegate ((AppDelegate *)  [[UIApplication sharedApplication] delegate])


//FAQ Questions/Answers

//Car Spa FAQs
#define SERVICE_CarSpa @"Car Spa"
#define CarSpaQuest1 @"Is it a doorstep service?"
#define CarSpaAnswer1 @"Yes, we provide doorstep service. Our technicians will come at your given address and do the best job"

#define CarSpaQuest2 @"Do you guarantee removal of all stains from interior?"
#define CarSpaAnswer2 @"We will do our best to remove all the stains, even the stubborn one. But in case of risk of material damage, we might not be able to do so."

#define CarSpaQuest3 @"Do you clean engines?"
#define CarSpaAnswer3 @"No, we do not provide engine cleaning services"

#define CarSpaQuest4 @"Does your service include buffing, scratch removals, or polishing?"
#define CarSpaAnswer4 @"Yes we do Buffing, scratch removals &amp; polishing."

#define CarSpaQuest5 @"Does your service include detailing?"
#define CarSpaAnswer5 @"No, we not provide detailing"

#define CarSpaQuest6 @"Is there any pre-requisite for me?"
#define CarSpaAnswer6 @"Please make sure that water and electricity is available."

//Home Cleaning
#define SERVICE_HomeCleaning @"Home Cleaning"
#define HomeCleaningQuest1 @"How is deep cleaning different than regular cleaning?"
#define HomeCleaningAnswer1  @"Deep cleaning is an intensive cleaning which includes cleaning of every nook and cranny. Furniture and other appliances are moved whereas required and possible for effective cleaning. Regular cleaning is more of manual cleaning and is not so thorough."

#define HomeCleaningQuest2 @"How long will it take for deep cleaning of whole home?"
#define HomeCleaningAnswer2 @"Time taken depends on the area of the premise and occupancy of the rooms."

#define HomeCleaningQuest3 @"What is included in Home Deep Cleaning?"
#define HomeCleaningAnswer3 @"Mechanized scrubbing of the floors, vacuuming of the entire house &amp; upholstery, scrubbing of tiled walls, descaling of taps, dusting &amp; wiping of furniture and cleaning of lofts, fans &amp; fixtures"

#define HomeCleaningQuest4 @"Can I book home deep cleaning for a portion of my place?"
#define HomeCleaningAnswer4 @"Yes, you can book home deep cleaning for individual rooms or a portion of your place"

#define HomeCleaningQuest5 @"Do I need to provide my own vacuum or other cleaning supplies?"
#define HomeCleaningAnswer5 @"No, you are not required to supply us with any cleaning products, vacuum or any such thing. Unless, you would rather us use your cleaning supplies."

//Upholstery Cleaning
#define SERVICE_Upholstery @"Upholstery Cleaning"
#define UpholsteryCleaningQuest1 @"What all is included in Upholstery cleaning?"
#define UpholsteryCleaningAnswer1 @"Any type of fabric furniture, leather sofa, carpet, curtains or any other upholstery Item"

#define UpholsteryCleaningQuest2 @"How much time does it take for Sofa to be dry after cleaning?"
#define UpholsteryCleaningAnswer2 @"Sofa will be moist for 5 to 6 hours"

#define UpholsteryCleaningQuest3 @"How often should upholstery item be cleaned?"
#define UpholsteryCleaningAnswer3 @"Frequency will depend the area where you reside and usability of your place"

#define UpholsteryCleaningQuest4 @"Do you guarantee removal of all stains or spots?"
#define UpholsteryCleaningAnswer4 @"We cannot guarantee that all stains or spots will be removed. There are substances which discolor the fabric and age of the Item being cleaned also matters"

#define UpholsteryCleaningQuest5 @"Do you use any chemicals for upholstery cleaning?"
#define UpholsteryCleaningAnswer5 @"Yes, we use fabric friendly chemical for cleaning and they are customized as per item being cleaned"

//AC Services
#define SERVICE_AC @"AC"
#define ACQuest1 @"Do you provide AMC for AC service?"
#define ACAnswer1 @"Yes, we provide AMC for AC service"

#define ACQuest2 @"Do you deal with AC repair issues?"
#define ACAnswer2 @"Yes, we provide AC repair service also"

//Electrical Service
#define SERVICE_Electrical @"Electrical"
#define ElectricalServiceQuest1 @"Is cost of material required for repair included in rates charged by you?"
#define ElectricalServiceAnswer1 @"No, customer is responsible for cost of any material required."

#define ElectricalServiceQuest2 @"What all is included in repair work?"
#define ElectricalServiceAnswer2 @"It includes basic repairing works, it excludes motor rewinding, Fan rewinding, New Installation works. We do these works also but on extra cost."

//Plumbing Services
#define SERVICE_Plumbing @"Plumbing"
#define PlumbingQuest1 @"Are there any additional changes?"
#define PlumbingAnswer1 @"Yes, Customer is responsible for cost of any part which needs to be replaced. In case water pump is used, customer will be charged Rs 100"

//Carpentry Services
#define SERVICE_Carpentry @"Carpentry"
#define CarpentryQuest1 @"Is cost of material required for repair included in rates charged by you?"
#define CarpentryAns1 @"No, customer is responsible for cost of any material required."

#define CarpentryQuest2 @"Do you provide warranty for your work?"
#define CarpentryAns2 @"No, we do not provide any warranty for carpentry work except for any material which comes with warranty"

//Woodwork Services
#define SERVICE_Woodwork @"WOOD WORK"
#define WoodWorkQuest1 @"Do you create Furniture set only?"
#define WoodWorkAnswer1 @"Along with Furniture set, we deal with creative décor pieces also"

#define WoodWorkQuest2 @"Do you work with a specific type of wood?"
#define WoodWorkAnswer2 @"No, we work with any type of wood."

#define WoodWorkQuest3 @"Do you charge an hourly rate?"
#define WoodWorkAns3 @"Yes"

#define WoodWorkQuest4 @"Is cost of material used included?"
#define WoodWorkAns4 @"No, customer is responsible for cost of any material being used"

#define WoodWorkQuest5 @"Do you varnish the product?"
#define WoodWorkAns5 @"Yes, varnishing of built product is included"

//Pest Control Services
#define SERVICE_PestControl @"PEST CONTROL"
#define PestControlQuest1 @"Do we have to vacate the place for treatment?"
#define PestControlAns1 @"No, you do not need to vacate your place"

#define PestControlQuest2 @"Are your materials safe?"
#define PestControlAns2 @"We use eco-friendly chemical"

#define PestControlQuest3 @"Do I need regular pest control services?"
#define PestControlAns3 @"Need at least 3-4 treatments in a year to prevent further generation of larvae/new cockroaches and other pests."

#define PestControlQuest4 @"Do you provide AMC?"
#define PestControlAns4 @"Yes, we provide AMC for pest control services."

//Appliance Repair Services
#define SERVICE_Appliance @"Home Appliances"
#define ApplianceQuest1 @"Is it a doorstep service?"
#define ApplianceAns1 @"Yes, it’s a doorstep service but in case of some issues, we might take the appliance with us for repair"

#define ApplianceQuest2 @"Do you provide warranty for your work?"
#define ApplianceAns2 @"Yes, we provide warranty of 7 days."

#define ApplianceQuest3 @"Is cost of material required for repair included in rates charged by you?"
#define ApplianceAns3 @"No, customer is responsible for cost of any material required."

//Water Tank Cleaning Services
#define SERVICE_WaterTank @"WATER TANK CLEANING"
#define WaterTankQuest1 @"Is cost of material required for repair included in rates charged by you?"
#define WaterTankAns1 @"No, customer is responsible for cost of any material required."

#define WaterTankQuest2 @"How long does it take to clean the tank?"
#define WaterTankAns2 @"Time Taken- 2 to 3 hours for overhead tank cleaning"

#define WaterTankQuest3 @"Do you use any chemicals to clean and are they harmful?"
#define WaterTankAns3 @"We use anti- bacterial spray &amp; they are eco-friendly"

//Security Services

//CCTV Services
#define SERVICE_CCTV @"CCTV"
#define CCTVQuest1 @"Is cost of material required included in rates charged by you?"
#define CCTVAns1 @"No, customer is responsible for cost of any material required."

#define CCTVQuest2 @"Do you provide warranty for your work?"
#define CCTVAns2 @"Any material that comes with warranty will be included"

//Gardening Services
#define SERVICE_Gardening @"GARDENING"
#define GardeningQuest1 @"Do I need to provide anything likes bags, machine or something else?"
#define GardeningAns1 @"No, the team will bring everything"

#define GardeningQuest2 @"Do you plant flowers?"
#define GardeningAns2 @"Yes, we do. Either you can provide us the exact plants or give us a list of plants with all details. In case, we need to get the plants, send your requirement 2-3 days prior to our visit"

#define GardeningQuest3 @"Can you visit on regular basis?"
#define GardeningAns3 @"Yes, we can. You can arrange our visit weekly, fortnightly, monthly etc., as per your requirement."

//Paint Services
#define SERVICE_Paint @"PAINT SERVICE"
#define PaintQuest1 @"Is cost of material required included in rates charged by you?"
#define PaintAns1 @"No, customer is responsible for cost of any material required."

#define PaintQuest2 @"Do you provide warranty for your work?"
#define PaintAns2 @"NO, this is one time job only."

#define PaintQuest3 @"WHAT SHOULD I DO TO PREPARE MY HOUSE FOR PAINTING?"
#define PaintAns3 @"Cover all the Fixtures, Equipments etc. and provide workable space for workers."

#define PaintQuest4 @"WHAT TYPE OF PAINT DO YOU USE?"
#define PaintAns4 @"We provide all type of Paints as per customer requirements. Generally we use branded Paints like Asian, Burger etc."

#define PaintQuest5 @"HOW LONG DOES A JOB TAKE?"
#define PaintAns5 @"It depends of the area to be painted."

//RO Services
#define SERVICE_RO @"RO"
#define ROQuest1 @"Do you provide AMC for RO service?"
#define ROAns1 @"Yes, we provide AMC for RO service"

#define ROQuest2 @"Do you work on specific brands of RO?"
#define ROAns2 @"No, we work on any type of RO purifier"

#define ROQuest3 @"Is cost of material required included in rates charged by you?"
#define ROAns3 @"No, customer is responsible for cost of any material required."

#define SERVICE_InteriorDesign @"Interior Design"
//Interior Design Services
//#define InteriorDesignQuest1 @""
//#define InteriorDesignAns1 @""

//Fumigation Services
#define SERVICE_Fumigation @"Fumigation Services"
#define FumigationQuest1 @"How is fumigation different than Pest Control?"
#define FumigationAns1 @""

//Unlimited Plan

#define UnlimitedPlanQuest1 @"How Many services my Unlimited Plan covers?"
#define UnlimitedPlanQuest2 @"Is any inspection will be done before plan start at my Home?"
#define UnlimitedPlanQuest3 @"Is there any limitation of repairing?"
#define UnlimitedPlanQuest4 @"Is AC shifting will be covered?"
#define UnlimitedPlanQuest5 @"Is AC filter cleaning covered under plan?"
#define UnlimitedPlanQuest6 @"Is all repairs covered under plan?"
#define UnlimitedPlanQuest7 @"What will be scope of work for the plan?"
#define UnlimitedPlanQuest8 @"How many sqft are there in 1 YARD?"
#define UnlimitedPlanQuest9 @"Is there any limit of AC’s(Window & Split) under Unlimited plan?"
#define UnlimitedPlanQuest10 @"What is excluded in Unlimited plans?"
#define UnlimitedPlanQuest11 @"What is the duration of Unlimited Plan?"
#define UnlimitedPlanQuest12 @"Is there any Refund Policy?"

#define UnlimitedPlanAns1 @"Plan covers only these four basic services: Electrical, Plumbing, Carpentry and AC Servicing."
#define UnlimitedPlanAns2 @"Maintenance will be offered to the customer only after inspection of the AC set/electrical system/plumbing system/furniture, and ensuring that it is in the working/maintainable condition. Make n Make’s responsibility to maintain fixtures, fittings, equipment shall be limited to those in working condition at their maximum efficiency at the time of commencement of contract. In case the concerned fixture, fitting or equipment, though covered under the scope of service, is not in working condition, then it shall be first repaired on chargeable basis and then maintenance under contract shall start"
#define UnlimitedPlanAns3 @"The company shall be under no obligation to provide repair / service under this plan if the set/system is not working because of improper use, negligence, overuse, unauthorized alteration, modification or substitution of any part or serial number, defacement, abnormal voltage fluctuation, rat bite, negligence on part of customer, acts of God like floods, lightening, earthquakes etc., or through causes generated by non-ordinary use. If our services are required as a result of the causes stated above, such services shall be charged extra as per the company price list in effect at the time of the repair work."
#define UnlimitedPlanAns4 @"Un-installation & re-installation of AC Unit is not covered under the plan in case of change of location of product. Carpentry Service covers merely the maintenance of furniture and does not cover manufacturing of furniture. Electrical and Plumbing Services shall not cover installation of new equipment/fitting."
#define UnlimitedPlanAns5 @"Routine cleaning of the AC filter on a Monthly/fortnightly basis does not fall under the scope of this Unlimited Plan pack."
#define UnlimitedPlanAns6 @"Repairs that involve Civil work i.e. mason work, digging, paint, plaster, brick work, labour work etc. shall not be included under the scope of this service plan. MnM Services Pvt. Ltd. can provide you an estimate for such issues."
#define UnlimitedPlanAns7 @"The scope of all services offered under the service pack will be restricted to the inside area of the premises only. Outside and government liaisons area and other agency scope will not be included. Broadly, the valid scope is elaborated as follows: \n Electrical: Already installed DB/Switchboard /panels etc. are in scope provided that they are inside the premises only.\nPlumbing: Water line and Sewerage lines inside the premises only. \nAir conditioning: Includes Split, Window and Ductable ACs only (VRV AC units are excluded).\nCarpentry: Includes repairing of the furniture only. Manufacturing/fabrication/modification is not covered.The technician shall rectify the complaints for the existing set-up and will not carry out new installation work. Any new installation work will be intimated during call registration and we shall provide you a quote after the inspection. \nSome major repairs shall take more time and customer co-operation shall be appreciated.Each service call will be limited to a maximum duration of 90 minutes only. In case the work requires further time, the customer will need to raise a fresh service ticket/request for completion on a subsequent day. This does not cover the bulk jobs. In case of bulk jobs, which require over 90 minutes of service, the customers shall make a booking at least 48 hours in advance to ensure a smooth handling and completion of the job."

#define UnlimitedPlanAns8 @"1 Yard = 9 sq. ft."
#define UnlimitedPlanAns9 @"Maximum of 3, 5 and 7 ACs shall be covered under the unlimited plans for the Rs. 4999, Rs. 5999 and Rs. 6999 respectively. For additional ACs, separate charges as per the prevailing rate card of MnM Services Pvt. Ltd. shall be applicable."
#define UnlimitedPlanAns10 @"The following jobs stand strictly excluded from the scope of the Service Packs: Repair, installation, uninstallation of home appliances such as TV, washing machine, fridge, microwave, juicer-mixer-grinders, dishwashers, chimney etc. RO water system, DG sets, AC Installation & Uninstallation Works, Water Cooler Cleaning, Any uninstallation or installation, Wooden Flooring, Incidental civil work, special handling equipment for heavy and/or bulky products, The Unlimited Plan shall get activated within 24 hours of making the payment. In case of payments made through cheque, the Plan will get activated 24 hours after the credit of cheque in MnM’s bank account"
#define UnlimitedPlanAns11 @"Unlimited Plan pack will automatically expire after a tenure of 1 year."
#define UnlimitedPlanAns12 @"Yes, with the below mentioned conditions: A customer may request a refund within a period of 15 days. Refunds shall be made on a pro-rata basis considering the services already consumed. Customer will not be eligible for refunds if 3 calls have been consumed within 15 days of plan activation The Company reserves the right to cancel the subscription of a customer at any time during the subscription period, and in that case pro-data amount shall be refunded. "


//Fixed Plan

#define FixedPlanQuest1 @"What all services are included in Fixed Call Plan?"
#define FixedPlanQuest2 @"What is the duration of Fixed Call Plan?"
#define FixedPlanQuest3 @"What is the duration of per visit?"
#define FixedPlanQuest4 @"How any visit shall be taken in one go w.r.t service?"
#define FixedPlanQuest5 @"What is the scope of work?"
#define FixedPlanQuest6 @"Is there any cap/restriction on the number of visits that are bind in the plan?"
#define FixedPlanQuest7 @"Is there any Refund Policy?"

#define FixedPlanAns1 @"Under this plan, the four basic services are offered: Electrical, Plumbing, Carpentry and AC Servicing."
#define FixedPlanAns2 @"Duration of the Fixed Call Plan is 1 year."
#define FixedPlanAns3 @"Duration of per visit shall be 60 minutes and 90 minutes."
#define FixedPlanAns4 @"Visit will automatically calculated depending upon the time calculation by Check –in/Check Out process. Multiple tickets shall be consumed in one go."
#define FixedPlanAns5 @"Scope of work shall be inside the radius of the premises only and does not cover work that involves governmental or regulatory permissions."
#define FixedPlanAns6 @"No, all the visits can be consumed in a single service or multiple service."
#define FixedPlanAns7 @"Yes, with the below mentioned conditions: A customer may request a refund within a period of 15 days. Refunds shall be made on a pro-rata basis considering the services already consumed. Customer will not be eligible for refunds if 3 calls have been consumed within 15 days of plan activation The Company reserves the right to cancel the subscription of a customer at any time during the subscription period, and in that case pro-data amount shall be refunded."

//AddOn Plan

#define AddOnQuest1 @"Create the plan as per your need"
#define AddOnQuest2 @"Plan cannot include Basic services i.e. Electrical, Plumbing, Carpentry, AC service"

#endif /* Constant_h */
