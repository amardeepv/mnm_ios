//
//  AppDelegate.m
//  MakeNMake
//
//  Created by Nripendra on 29/01/17.
//  Copyright © 2017 Nripendra  singh. All rights reserved.
//

#import "AppDelegate.h"
#import "IQKeyboardManager.h"
#import "Constant.h"
//#import "FBSDKCoreKit"
//
//#import "FBSDKLoginKit"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

# define KFBLogin  @"FaceBook"
# define KGoogleLogin @"Google"
#import "SWRevealViewController.h"
#import "SideMenuVC.h"
#import "HomeViewController.h"
#import "WelcomePageMenuVC.h"
#import "ScheduleViewController.h"
#import "OfferViewController.h"
#import "PaymentVCViewController.h"

static NSString * const kClientId = @"1022406779750-4k4qknlv0hba0iquhvd74n9b8igmghsn.apps.googleusercontent.com";

@interface AppDelegate ()<GIDSignInUIDelegate,GIDSignInDelegate>


@end

@implementation AppDelegate

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [FBSDKAppEvents activateApp];
}


+(AppDelegate*)sharedInstance;
{
    return (AppDelegate *)[UIApplication sharedApplication].delegate;
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    locationManager = [[CLLocationManager alloc] init];
    geocoder = [[CLGeocoder alloc] init];
    
    [self initializeSelectionForServices];
    
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
        [locationManager requestWhenInUseAuthorization];
    [locationManager startUpdatingLocation];
    
    
    [application setStatusBarHidden:false];
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
   
    NSError* configureError;
    [[GGLContext sharedInstance] configureWithError: &configureError];
  //  NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    [GIDSignIn sharedInstance].delegate = self;
    [GIDSignIn sharedInstance].uiDelegate = self;

   [[NSUserDefaults standardUserDefaults]setInteger:111200 forKey:@"SocialID"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    [GIDSignIn sharedInstance].delegate = self;
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    //setting default value for showing coupon
    _showCouponField = false;
    
    self.window = [[UIWindow alloc]initWithFrame:[[UIScreen mainScreen] bounds]];
    
    if (![[NSUserDefaults standardUserDefaults] valueForKey:@"UserExist"]) {
    //PaymentVCViewController
    
        WelcomePageMenuVC *Obj = [[WelcomePageMenuVC alloc]initWithNibName:@"WelcomePageMenuVC" bundle: nil];
    
   // PaymentVCViewController *Obj = [[PaymentVCViewController alloc]initWithNibName:@"PaymentVCViewController" bundle: nil];

        [[NSUserDefaults standardUserDefaults] setValue:@"true" forKey:@"UserExist"];
    
        navCtrl = [[UINavigationController alloc]initWithRootViewController:Obj];
        

    }else{
        SideMenuVC *slideObj = [[SideMenuVC alloc]initWithNibName:@"SideMenuVC" bundle:nil];
        
        HomeViewController *homeObj = [[UIStoryboard storyboardWithName:@"Second" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
        SWRevealViewController *mainRevealController =
        [[SWRevealViewController alloc] initWithRearViewController:slideObj frontViewController:homeObj];
        
        mainRevealController.rearViewRevealWidth = 250;
        mainRevealController.rearViewRevealOverdraw = 120;
        mainRevealController.bounceBackOnOverdraw = NO;
        mainRevealController.stableDragOnOverdraw = YES;
        mainRevealController.delegate = self;

    //RegisterViewController
    
    //    RegisterViewController *mainRevealController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"RegisterViewController"];
////
      navCtrl = [[UINavigationController alloc]initWithRootViewController:mainRevealController];
//

    }

    [self.window setRootViewController:navCtrl];
    navCtrl.navigationBarHidden = true;
    
    [self.window makeKeyAndVisible];
    
    
       return YES;
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}


- (void)GetLocationMethod {
    locationManager = [[CLLocationManager alloc] init];
    geocoder = [[CLGeocoder alloc] init];
    
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
        [locationManager requestWhenInUseAuthorization];
    [locationManager startUpdatingLocation];

}
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    // Reverse Geocoding
    NSLog(@"Resolving the Address");
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
        if (error == nil && [placemarks count] > 0) {
            placemark = [placemarks lastObject];
            _addressLabel =  [NSString stringWithFormat:@"%@",placemark.locality];
            [locationManager stopUpdatingLocation];

        } else {
            NSLog(@"%@", error.debugDescription);
        }
        
    } ];
}
- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations on signed in user here.
    
    NSMutableDictionary *diction = [NSMutableDictionary dictionary];
    [diction setValue:user.profile.givenName forKey:@"firstName"];
    [diction setValue:user.profile.email forKey:@"email"];
    [diction setValue:user.profile.familyName forKey:@"LastName"];
    [diction setValue:user.userID forKey:@"SocialId"];
    [diction setValue:@"2" forKey:@"Provider"];
    [diction setValue:user.authentication.idToken forKey:@"AccessToken"];
    
if ([self.GoogleCustomDelegate respondsToSelector:@selector(googleSignInUserWithDict:)]) {
        [_GoogleCustomDelegate googleSignInUserWithDict:diction];
    }
    
}
- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    NSLog(@"user Disconnects from the app");
    
}
- (BOOL)application: (UIApplication *)application
            openURL: (NSURL *)url
  sourceApplication: (NSString *)sourceApplication
         annotation: (id)annotation
{
    if ([self.loginType isEqualToString:KFBLogin])
    {
        return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                              openURL:url
                                                    sourceApplication:sourceApplication
                                                           annotation:annotation];
    }else{
        
        return [[GIDSignIn sharedInstance] handleURL:url
                                   sourceApplication:sourceApplication
                                          annotation:annotation];
    }
    
    
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey, id> *)options NS_AVAILABLE_IOS(9_0);{
    
    if ([self.loginType isEqualToString:KFBLogin])
    {
    return [[FBSDKApplicationDelegate sharedInstance] application:app
                     openURL:url
           sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                  annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
    }else{
    return [[GIDSignIn sharedInstance] handleURL:url
                               sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                      annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
    
}

}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}



- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
-(void)setupSlideBar{
    SideMenuVC *slideObj = [[SideMenuVC alloc]initWithNibName:@"SideMenuVC" bundle:nil];
    
    HomeViewController *homeObj = [[UIStoryboard storyboardWithName:@"Second" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
    SWRevealViewController *mainRevealController =
    [[SWRevealViewController alloc] initWithRearViewController:slideObj frontViewController:homeObj];
    
    mainRevealController.rearViewRevealWidth = 250;
    mainRevealController.rearViewRevealOverdraw = 120;
    mainRevealController.bounceBackOnOverdraw = NO;
    mainRevealController.stableDragOnOverdraw = YES;
    mainRevealController.delegate = self;
    self.window = [UIApplication sharedApplication].keyWindow ;
    UINavigationController *navbar = [[UINavigationController alloc]initWithRootViewController:mainRevealController];
    navbar.navigationBarHidden = true;
    self.window.rootViewController = navbar;
}
-(NSString *)getUserID{
    
    return [[NSUserDefaults standardUserDefaults] valueForKey:UserId];
}
-(void)addServiceInfoWithServiceDict:(NSMutableDictionary *)dict{
    NSMutableDictionary  *selectedServiceInfo  = [[NSMutableDictionary alloc]init];
   
    [selectedServiceInfo setValue:[dict valueForKey:SERVICE_NAME] forKey:SERVICE_NAME];
    [selectedServiceInfo setValue:[dict valueForKey:SERVICE_PRICE] forKey:SERVICE_PRICE];
    [selectedServiceInfo setValue:[dict valueForKey:@"serviceID"] forKey:@"serviceID"];
    [selectedServiceInfo setValue:[dict valueForKey:@"parentServiceName"] forKey:@"parentServiceName"];
    [selectedServiceInfo setValue:[dict valueForKey:@"childServiceName"] forKey:@"childServiceName"];

    
    if(_serviceArray.count>0){
        
        
        NSMutableArray *tempArray = [[NSMutableArray alloc]initWithArray:_serviceArray];

        for (NSDictionary *dict in _serviceArray) {
            if ([[dict valueForKey:SERVICE_NAME] isEqualToString:[selectedServiceInfo valueForKey:SERVICE_NAME]]) {
                //                serviceExist
                [tempArray removeObject:dict];
            }
            [tempArray addObject:selectedServiceInfo];

        }
        
        _serviceArray = tempArray;
        
    }else{
        [_serviceArray addObject:selectedServiceInfo];

    }
    
    

    NSSet *set = [NSSet setWithArray:_serviceArray];
    _serviceArray = (NSMutableArray *)[set allObjects];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateBadgeCount" object:_serviceArray];

    NSLog(@"Selected services - %@",_serviceArray);
}

-(void)initializeSelectionForServices{
    //Initializing dictionary for use in Checkout flow
    
    //for telling that default flow is not Open ticket api flow
    [[NSUserDefaults standardUserDefaults] setValue:@"False" forKey:@"isOpenTicket"];

    _serviceArray = [[NSMutableArray alloc]init];
}
-(void)removeServiceInfoWithServiceDict:(NSMutableDictionary *)dict{
    NSMutableDictionary  *selectedServiceInfo  = [[NSMutableDictionary alloc]init];
    
    [selectedServiceInfo setValue:[dict valueForKey:SERVICE_NAME] forKey:SERVICE_NAME];
    [selectedServiceInfo setValue:[dict valueForKey:SERVICE_PRICE] forKey:SERVICE_PRICE];
    [selectedServiceInfo setValue:[dict valueForKey:@"serviceID"] forKey:@"serviceID"];
    [selectedServiceInfo setValue:[dict valueForKey:@"parentServiceName"] forKey:@"parentServiceName"];
    [selectedServiceInfo setValue:[dict valueForKey:@"childServiceName"] forKey:@"childServiceName"];
    
    if(_serviceArray.count>0){
        
        
        NSMutableArray *tempArray = [[NSMutableArray alloc]initWithArray:_serviceArray];
        
        for (NSDictionary *dict in _serviceArray) {
            if ([[dict valueForKey:SERVICE_NAME] isEqualToString:[selectedServiceInfo valueForKey:SERVICE_NAME]]) {
                //                serviceExist
                [tempArray removeObject:dict];
            }            
        }
        
        _serviceArray = tempArray;
        
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateBadgeCount" object:_serviceArray];
    
    NSLog(@"Selected services - %@",_serviceArray);

}

-(NSMutableArray *)getSelecteServices{
    return _serviceArray;
}

@end
