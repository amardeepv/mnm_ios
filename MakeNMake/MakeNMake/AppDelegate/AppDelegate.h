//
//  AppDelegate.h
//  MakeNMake
//
//  Created by Nripendra on 29/01/17.
//  Copyright © 2017 Nripendra  singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Google/SignIn.h>
#import "MBProgressHUD.h"
#import "AFHTTPRequestOperation.h"
#import <CoreLocation/CoreLocation.h>

@protocol GoogleSignInCustomProtocol <NSObject>

@optional
-(void)googleSignInUserWithDict:(NSMutableDictionary *)dict;

@end

@interface AppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate> {
    CLLocationManager *locationManager;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
    UINavigationController *navCtrl;
}
@property (weak, nonatomic) NSString*addressLabel;
@property (weak, nonatomic) MBProgressHUD *hud;
@property (nonatomic, weak)AFHTTPRequestOperation *op;
@property (nonatomic, assign) BOOL checkProgressCount, showCouponField, isOtpVerified;
@property (nonatomic, retain) NSMutableArray *arrHomeService;
@property (nonatomic, retain) NSMutableArray *strtBookingServiceDetail;

@property (strong, nonatomic) UIWindow *window;
@property(strong,nonatomic)NSString *loginType;

@property(strong,nonatomic)  id<GoogleSignInCustomProtocol>GoogleCustomDelegate;

@property(strong,nonatomic)NSString *userID;
@property(strong,nonatomic)NSString *openTicketPlanId;

@property(strong,nonatomic)NSMutableArray  *serviceArray;

@property(assign,nonatomic)int socialID;

-(NSString*)getUserID;
- (void)GetLocationMethod ;
+(AppDelegate*)sharedInstance;

-(void)setupSlideBar;
-(NSMutableArray *)getSelecteServices;
-(void)initializeSelectionForServices;

-(void)addServiceInfoWithServiceDict:(NSMutableDictionary *)dict;
-(void)removeServiceInfoWithServiceDict:(NSMutableDictionary *)dict;

@end

